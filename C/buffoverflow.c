#include <stdio.h>

target_func() {
	printf("Buffer Overflow Exploited.\n");
	exit(0);
}

getInput() {
	char buffer[8];
	gets(buffer);
	puts(buffer);	
}

main() {
	getInput();
}
