#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct tree {
    struct tree *parent;
    struct tree *left_child;
    struct tree *right_child;
} tree;

long count_leaves(tree *t)
{
    long total = 0;

    if (! t->left_child && ! t->right_child)
        return 1;

    total += count_leaves(t->left_child) + count_leaves(t->right_child);

    return total;
}

void make_tree(tree *t, long total_leaves)
{
    long next_leaves;

    if (total_leaves > 1) {

        next_leaves = ((total_leaves % 2) == 0) ?
            total_leaves / 2 : (total_leaves + 1) / 2;

        t->left_child = malloc(sizeof(tree));
        memset(t->left_child, 0, sizeof(tree));
        make_tree(t->left_child, next_leaves);

        t->right_child = malloc(sizeof(tree));
        memset(t->right_child, 0, sizeof(tree));
        make_tree(t->right_child, total_leaves - next_leaves);

    } else {
        return;
    }

}

int main(int argc, char **argv)
{
    tree *t;
    long total_leaves;

    if (argc < 2 || strcmp(argv[1], "--help") == 0) {
        printf("Usage: %s num-leaves\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    t = malloc(sizeof(tree));
    if (! t) {
        perror("malloc");
        return 1;
    }
    memset(t, 0, sizeof(tree));

    total_leaves = atol(argv[1]);

    make_tree(t, total_leaves);
    total_leaves = count_leaves(t);
    printf("total leaves: %ld\n", total_leaves);

    return 0;
}
