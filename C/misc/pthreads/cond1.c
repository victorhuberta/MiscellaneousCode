#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t count_mutex = PTHREAD_MUTEX_INITIALIZER;
int counter = 0;
#define COUNT_HALT1 3
#define COUNT_HALT2 6
#define COUNT_END 10

void *function1();
void *function2();
void _block_on_cond(int condition);
void _inc_and_print_counter(int t_num);

int main(void)
{
    pthread_t tid0, tid1;
    int res;

    res = pthread_create(&tid0, NULL, function1, NULL);
    res = pthread_create(&tid1, NULL, function2, NULL);

    if (res != 0) {
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }

    pthread_join(tid0, NULL);
    pthread_join(tid1, NULL);

    exit(EXIT_SUCCESS);
}

void *function1()
{
    while (1) {
        _block_on_cond(counter >= COUNT_HALT1 && counter <= COUNT_HALT2);
        _inc_and_print_counter(1);

        if (counter >= COUNT_END) {
            pthread_cond_signal(&condition_cond);
            return NULL;
        }
    }
}

void *function2()
{
    while (1) {
        _block_on_cond(counter < COUNT_HALT1 || counter > COUNT_HALT2);
        _inc_and_print_counter(2);

        if (counter >= COUNT_END) {
            pthread_cond_signal(&condition_cond);
            return NULL;
        }
    }
}

void _block_on_cond(int condition)
{
    pthread_mutex_lock(&condition_mutex);
    if (condition) {
        pthread_cond_signal(&condition_cond);
        pthread_cond_wait(&condition_cond, &condition_mutex);
    }
    pthread_mutex_unlock(&condition_mutex);
}

void _inc_and_print_counter(int t_num)
{
    pthread_mutex_lock(&count_mutex);
    ++counter;
    printf("Thread-%d counts: %d\n", t_num, counter);
    pthread_mutex_unlock(&count_mutex);
}
