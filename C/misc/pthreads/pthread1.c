#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>

void *print_stuff(void *str)
{
    int *retval = malloc(sizeof *retval);
    *retval = -1;

    if (retval == NULL) return retval;

    printf("I am %s\n", (char *) str);

    *retval = strlen(str);
    return retval;
}

int main(void)
{
    pthread_t tid0, tid1;
    char *f_name = "Thread-1",
        *s_name = "Thread-2";
    int res, *retval;

    res = pthread_create(&tid0, NULL, print_stuff, f_name);
    res = pthread_create(&tid1, NULL, print_stuff, s_name);

    if (res != 0) {
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }

    printf("Thread: %ld & Thread: %ld\n", tid0, tid1);

    pthread_join(tid0, (void **) &retval);
    printf("%d just finished its job\n", *retval);

    pthread_join(tid1, (void **) &retval);
    printf("%d just finished its job\n", *retval);

    exit(EXIT_SUCCESS);
}
