#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <alloca.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int counter = 0;

void *do_stuff();

int main(int argc, char **argv)
{
    pthread_t tid0, tid1;
    int res;

    res = pthread_create(&tid0, NULL, do_stuff, NULL);
    res = pthread_create(&tid1, NULL, do_stuff, NULL);

    if (res != 0) {
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }

    pthread_join(tid0, NULL);
    pthread_join(tid1, NULL);

    exit(EXIT_SUCCESS);
}

void *do_stuff()
{
    pthread_mutex_lock(&mutex);
    ++counter;
    printf("counter: %d\n", counter);
    pthread_mutex_unlock(&mutex);

    return NULL;
}
