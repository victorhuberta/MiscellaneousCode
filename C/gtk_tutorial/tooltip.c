#include <stdlib.h>
#include <gtk/gtk.h>

void print_something(GtkWidget *widget, gpointer window);

int main(int argc, char **argv)
{
    GtkWidget *window;
    GtkWidget *halign;
    GtkWidget *mnemonics_btn;

    gtk_init(&argc, &argv);
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Tooltip");
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(window), 5);

    mnemonics_btn = gtk_button_new_with_mnemonic("_Print something");
    gtk_widget_set_tooltip_text(mnemonics_btn, "Try me out!");
    g_signal_connect(mnemonics_btn, "clicked",
        G_CALLBACK(print_something), NULL);

    halign = gtk_alignment_new(0, 0, 0, 0);
    gtk_container_add(GTK_CONTAINER(halign), mnemonics_btn);

    gtk_container_add(GTK_CONTAINER(window), halign);
    gtk_widget_show_all(window);

    g_signal_connect(G_OBJECT(window), "destroy",
        G_CALLBACK(gtk_main_quit), NULL);

    gtk_main();

    exit(EXIT_SUCCESS);
}

void print_something(GtkWidget *widget, gpointer window)
{
    g_printf("Mnemonics button clicked\n");
}
