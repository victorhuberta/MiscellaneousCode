#include <stdlib.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

void toggle_status_bar(GtkWidget *view_statusbar_chk, gpointer status_bar);

int main(int argc, char **argv)
{
    GtkWidget *window;
    GtkWidget *vbox;

    GtkAccelGroup *accel_grp;

    GtkWidget *menu_bar;
    GtkWidget *file_mi;
    GtkWidget *file_menu;
    GtkWidget *new_mi;
    GtkWidget *open_mi;
    GtkWidget *sep1_mi, *sep2_mi;
    GtkWidget *view_statusbar_chk;
    GtkWidget *quit_mi;

    GtkWidget *status_bar;

    gtk_init(&argc, &argv);
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Accelerator");
    gtk_window_set_default_size(GTK_WINDOW(window), 300, 200);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(window), vbox);

    accel_grp = gtk_accel_group_new();
    gtk_window_add_accel_group(GTK_WINDOW(window), accel_grp);

    menu_bar = gtk_menu_bar_new();
    file_mi = gtk_menu_item_new_with_mnemonic("_File");
    file_menu = gtk_menu_new();
    new_mi = gtk_image_menu_item_new_from_stock(GTK_STOCK_NEW, NULL);
    open_mi = gtk_image_menu_item_new_from_stock(GTK_STOCK_OPEN, NULL);
    sep1_mi = gtk_separator_menu_item_new();

    view_statusbar_chk = gtk_check_menu_item_new_with_label("View Status Bar");
    gtk_check_menu_item_set_active
        (GTK_CHECK_MENU_ITEM(view_statusbar_chk), TRUE);

    sep2_mi = gtk_separator_menu_item_new();
    quit_mi = gtk_image_menu_item_new_from_stock(GTK_STOCK_QUIT, accel_grp);

    gtk_widget_add_accelerator(quit_mi, "activate", accel_grp,
        GDK_q, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(file_mi), file_menu);
    gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), new_mi);
    gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), open_mi);
    gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), sep1_mi);
    gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), view_statusbar_chk);
    gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), sep2_mi);
    gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), quit_mi);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), file_mi);

    gtk_box_pack_start(GTK_BOX(vbox), menu_bar, FALSE, FALSE, 0);

    status_bar = gtk_statusbar_new();
    gtk_box_pack_end(GTK_BOX(vbox), status_bar, FALSE, TRUE, 0);

    g_signal_connect(G_OBJECT(view_statusbar_chk), "activate",
        G_CALLBACK(toggle_status_bar), status_bar);
    g_signal_connect(G_OBJECT(quit_mi), "activate",
        G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(G_OBJECT(window), "destroy",
        G_CALLBACK(gtk_main_quit), NULL);

    gtk_widget_show_all(window);

    gtk_main();

    exit(EXIT_SUCCESS);
}

void toggle_status_bar(GtkWidget *view_statusbar_chk, gpointer status_bar)
{
    if (gtk_check_menu_item_get_active
            (GTK_CHECK_MENU_ITEM(view_statusbar_chk)))
        gtk_widget_show(status_bar);
    else
        gtk_widget_hide(status_bar);
}
