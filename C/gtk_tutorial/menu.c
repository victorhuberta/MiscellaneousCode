#include <stdlib.h>
#include <gtk/gtk.h>

int main(int argc, char **argv)
{
    GtkWidget *window;
    GtkWidget *vbox;

    GtkWidget *menu_bar;
    GtkWidget *file_mi;
    GtkWidget *file_menu;

    GtkWidget *import_mi;
    GtkWidget *import_menu;
    GtkWidget *import_news_mi;
    GtkWidget *import_bookmarks_mi;
    GtkWidget *import_mail_mi;

    GtkWidget *sep;

    GtkWidget *quit_mi;

    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Menu");
    gtk_window_set_default_size(GTK_WINDOW(window), 300, 200);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(window), vbox);

    menu_bar = gtk_menu_bar_new();

    file_mi = gtk_menu_item_new_with_label("File");
    file_menu = gtk_menu_new();

    import_mi = gtk_menu_item_new_with_label("Import");
    import_menu = gtk_menu_new();
    import_news_mi = gtk_menu_item_new_with_label("Import news feed...");
    import_bookmarks_mi = gtk_menu_item_new_with_label("Import bookmarks...");
    import_mail_mi = gtk_menu_item_new_with_label("Import mail...");

    sep = gtk_separator_menu_item_new();

    quit_mi = gtk_menu_item_new_with_label("Quit");

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(import_mi), import_menu);
    gtk_menu_shell_append(GTK_MENU_SHELL(import_menu), import_news_mi);
    gtk_menu_shell_append(GTK_MENU_SHELL(import_menu), import_bookmarks_mi);
    gtk_menu_shell_append(GTK_MENU_SHELL(import_menu), import_mail_mi);

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(file_mi), file_menu);
    gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), import_mi);
    gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), sep);
    gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), quit_mi);

    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), file_mi);
    gtk_box_pack_start(GTK_BOX(vbox), menu_bar, FALSE, FALSE, 0);

    g_signal_connect(G_OBJECT(quit_mi), "activate",
        G_CALLBACK(gtk_main_quit), NULL);

    g_signal_connect(G_OBJECT(window), "destroy",
        G_CALLBACK(gtk_main_quit), NULL);

    gtk_widget_show_all(window);
    gtk_main();
}
