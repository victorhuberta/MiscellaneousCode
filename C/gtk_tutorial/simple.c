#include <stdlib.h>
#include <gtk/gtk.h>

GdkPixbuf *create_pixbuf(const gchar *filename);

int main(int argc, char **argv)
{
    GtkWidget *window;
    GdkPixbuf *icon;

    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Simple App");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    icon = create_pixbuf("icon.png");
    gtk_window_set_icon(GTK_WINDOW(window), icon);

    gtk_widget_show(window);
    g_signal_connect(G_OBJECT(window), "destroy",
        G_CALLBACK(gtk_main_quit), NULL);

    g_object_unref(icon);

    gtk_main();

    exit(EXIT_SUCCESS);
}

GdkPixbuf *create_pixbuf(const gchar *filename)
{
    GdkPixbuf *pixbuf;
    GError *err = NULL;

    pixbuf = gdk_pixbuf_new_from_file(filename, &err);

    if (! pixbuf) {
        fprintf(stderr, "%s\n", err->message);
        g_error_free(err);
    }

    return pixbuf;
}
