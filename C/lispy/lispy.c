#include "mpc.h"

#ifdef _WIN32

static char buffer[2048];

char* readline(char* prompt) {
    fputs(prompt, stdout);
    fgets(buffer, 2048, stdin);
    char* cpy = malloc(strlen(buffer)+1);
    strcpy(cpy, buffer);
    cpy[strlen(cpy)-1] = '\0';
    return cpy;
}

void add_history(char* unused) {}

#else
#include <editline/readline.h>
#include <editline/history.h>
#endif

/* Create Enumeration of Possible Error Types */
enum { LERR_DIV_ZERO, LERR_BAD_OP, LERR_BAD_NUM };

/* Create Enumeration of Possible lval Types */
enum { LVAL_NUM, LVAL_ERR, LVAL_SEXPR, LVAL_SYM };

/* Declare New lval Struct */
typedef struct lval {
    int type;

    long num;
    char *err;
    char *sym;

    int count;
    struct lval **cell;
} lval;

/* Create a new number type lval */
lval *lval_num(long x)
{
    lval *v = malloc(sizeof *v);

    v->type = LVAL_NUM;
    v->num = x;

    return v;
}

/* Create a new error type lval */
lval *lval_err(char *err) {
    lval *v = malloc(sizeof *v);

    v->type = LVAL_ERR;
    v->err = malloc(strlen(err) + 1);
    strcpy(v->err, err);

    return v;
}

lval *lval_sym(char *sym)
{
    lval *v = malloc(sizeof *v);

    v->type = LVAL_SYM;
    v->sym = malloc(strlen(sym) + 1);
    strcpy(v->sym, sym);

    return v;
}

lval *lval_sexpr(void)
{
    lval *v = malloc(sizeof *v);

    v->type = LVAL_SEXPR;
    v->count = 0;
    v->cell = NULL;

    return v;
}

void lval_del(lval *v)
{
    int i;

    switch (v->type) {
        case LVAL_ERR: free(v->err); break;
        case LVAL_SYM: free(v->sym); break;
        case LVAL_SEXPR:
            for (i = 0; i < v->count; ++i)
                lval_del(v->cell[i]);
            free(v->cell);
            break;
        default:
            break;
    }

    free(v);
}

lval *lval_read_num(mpc_ast_t *ast)
{
    long num;

    errno = 0;
    num = strtol(ast->contents, NULL, 10);

    return (errno == ERANGE) ?
        lval_err("invalid number") : lval_num(num);
}

lval *lval_add(lval *v, lval *n)
{
    ++v->count;
    v->cell = realloc(v->cell, v->count * sizeof *v);
    v->cell[v->count - 1] = n;

    return v;
}

lval *lval_read(mpc_ast_t *ast)
{
    int i;
    lval *x = NULL;

    if (strstr(ast->tag, "number"))
        return lval_read_num(ast);

    if (strstr(ast->tag, "symbol"))
        return lval_sym(ast->contents);

    if (strcmp(ast->tag, ">") == 0 || strstr(ast->tag, "sexpr"))
        x = lval_sexpr();

    for (i = 0; i < ast->children_num; ++i) {
        if (strcmp(ast->children[i]->contents, "(")) continue;
        if (strcmp(ast->children[i]->contents, ")")) continue;
        if (strcmp(ast->children[i]->tag, "regex")) continue;

        x = lval_add(x, lval_read(ast->children[i]));
    }

    return x;
}

void lval_print(lval *v);
void lval_expr_print(lval *v, char open, char close)
{
    int i;

    putchar(open);
    for (i = 0; i < v->count; ++i) {
        lval_print(v->cell[i]);

        if (i < v->count - 1)
            putchar('\n');
    }
    putchar(close);
}

/* Print an "lval" */
void lval_print(lval *v) {
    switch (v->type) {
        case LVAL_NUM:
            printf("%li", v->num);
            break;
        case LVAL_ERR:
            printf("Error: %s", v->err);
            break;
        case LVAL_SYM:
            printf("%s", v->sym);
            break;
        case LVAL_SEXPR:
            lval_expr_print(v, '(', ')');
            break;
        default:
            break;
    }
}

/* Print an "lval" followed by a newline */
void lval_println(lval *v) { lval_print(v); putchar('\n'); }

lval *eval_op(lval *x, char* op, lval *y) {

    /* If either value is an error return it */
    if (x->type == LVAL_ERR) { return x; }
    if (y->type == LVAL_ERR) { return y; }

    /* Otherwise do maths on the number values */
    if (strcmp(op, "+") == 0) { return lval_num(x->num + y->num); }
    if (strcmp(op, "-") == 0) { return lval_num(x->num - y->num); }
    if (strcmp(op, "*") == 0) { return lval_num(x->num * y->num); }
    if (strcmp(op, "/") == 0) {
        /* If second operand is zero return error */
        return (y->num == 0) ?
            lval_err("divide by zero") :
            lval_num(x->num / y->num);
    }

    return lval_err("unknown operator");
}

lval *eval(mpc_ast_t* t) {

    if (strstr(t->tag, "number")) {
        /* Check if there is some error in conversion */
        errno = 0;
        long x = strtol(t->contents, NULL, 10);
        return errno != ERANGE ? lval_num(x) : lval_err("number out of range");
    }

    char* op = t->children[1]->contents;
    lval *x = eval(t->children[2]);

    int i = 3;
    while (strstr(t->children[i]->tag, "expr")) {
        x = eval_op(x, op, eval(t->children[i]));
        i++;
    }

    return x;
}

int main(int argc, char** argv) {

    mpc_parser_t* Number = mpc_new("number");
    mpc_parser_t* Symbol = mpc_new("symbol");
    mpc_parser_t* Sexpr = mpc_new("sexpr");
    mpc_parser_t* Operator = mpc_new("operator");
    mpc_parser_t* Expr = mpc_new("expr");
    mpc_parser_t* Lispy = mpc_new("lispy");

    mpca_lang(MPCA_LANG_DEFAULT,
          "number   : /-?[0-9]+/ ;"
          "symbol   : '+' | '-' | '*' | '/' ;"
          "sexpr    : '(' <expr>* ')' ;"
          "expr     : <number> | <symbol> | <sexpr> ;"
          "lispy    : /^/ <expr>* /$/ ;",
        Number, Symbol, Sexpr, Expr, Lispy);

    puts("Lispy Version 0.0.0.0.4");
    puts("Press Ctrl+c to Exit\n");

    while (1) {
        char* input = readline("lispy> ");
        add_history(input);

        mpc_result_t r;
        if (mpc_parse("<stdin>", input, Lispy, &r)) {
          lval *result = eval(r.output);
          lval_println(result);
          mpc_ast_delete(r.output);
        } else {
          mpc_err_print(r.error);
          mpc_err_delete(r.error);
        }

        free(input);
    }

    mpc_cleanup(5, Number, Operator, Expr, Lispy);

    return 0;
}
