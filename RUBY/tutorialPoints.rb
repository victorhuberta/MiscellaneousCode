$global_variable

class Customer
	attr_reader :instance_variable, :class_variable
	@@class_variable = 0
	def initialize(instance_variable)
		@instance_variable = instance_variable
	end
end

customer = Customer.new(10)
puts customer.instance_variable
puts <<EOF
	this is a text. 
	just trying to be cool.
EOF