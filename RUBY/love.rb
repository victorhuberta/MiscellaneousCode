class Girl
	attr_accessor :name, :boyfriend_count, :boyfriend_name
    def initialize(name)
        @name = name
        @boyfriend_count = 0
    end
    
    def loves(boy)
        @boyfriend_name = boy.name
    end

    def loves?(boy)
    	@boyfriend_name == boy.name
    end
    
    def answer_feelings_from(boy)
    	unless single? &&  loves?(boy)
    		puts "#{@name} says 'No, I am sorry. But I have a boyfriend.'"
	    else
	    	puts "#{@name} says 'Yes, I do. I love you too, #{boy.name}'"
	    	@boyfriend_count += 1
	    	boy.girlfriend_count += 1
	    end
    end

    def single?
        @boyfriend_count == 0
    end
end

class Boy
	attr_accessor :name, :girlfriend_count, :girlfriend_name
    def initialize(name)
        @name = name
        @girlfriend_count = 0
    end
    
    def loves(girl)
        @girlfriend_name = girl.name
    end

    def loves?(girl)
    	@girlfriend_name == girl.name
    end

    def confess_to(girl)
    	unless single? && loves?(girl)
    		puts "#{@name} thinks 'No, I can't confess. I have a girlfriend.'"
    	else
    		puts "#{@name} says '#{girl.name}, I love you. Do you want to be my girlfriend?'"
    	end
    end
    
    def single?
        @girlfriend_count == 0
    end
end

theresia = Girl.new("Theresia")
victor = Boy.new("Victor")
theresia.loves(victor)
victor.loves(theresia)
victor.confess_to(theresia)
theresia.answer_feelings_from(victor)
puts theresia.single?
puts victor.single?
another_girl = Girl.new("Sandra")
victor.loves(another_girl)
victor.confess_to(another_girl)
another_boy = Boy.new("Kevin")
another_boy.loves(theresia)
another_boy.confess_to(theresia)
theresia.answer_feelings_from(another_boy)