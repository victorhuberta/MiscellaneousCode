class Student
	attr_reader :first_name, :last_name, :grade
	def initialize(first_name, last_name, grade)
		@first_name = first_name
		@last_name = last_name
		@grade = grade
	end
	
	def junior?
		grade < 12
	end

	def senior?
		grade == 12
	end

	def to_s
		"#{last_name}, #{first_name}"
	end
end

def seniors(students)
	students.select { |student| student.senior? }
end

def juniors(students)
	students.select { |student| student.junior? }
end

Victor = Student.new("Victor", "Huberta", 12)
Jennifer = Student.new("Jennifer", "Kyle", 12)
Dominic = Student.new("Dominic", "Gengar", 10)

all_students = [Victor, Jennifer, Dominic]

seniors(all_students).each do |student|
	puts "Senior : #{student}"
end

juniors(all_students).each do |student|
	puts "Junior : #{student}"
end