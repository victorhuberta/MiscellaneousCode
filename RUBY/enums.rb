a = [2,4,3,1]
enum = a.to_enum
enum.each {|num| print num}
a.each {|num| print num}
sorted = a.sort_by {|num| num}
p sorted.class
smallest_two = sorted.first(2)
p smallest_two.class
sorted.each {|num| print num}
smallest_two.each {|num| print num}
a.each_slice(3) {|num| print num}
enum = (1..10).enum_for(:each_slice, 1)
enum.each {|a| print a}
p enum
enum = Enumerator.new do |yielder|
	number = 0
	count = 1
	loop do
		number += count
		count += 1
		yielder.yield number
	end
end
5.times {print enum.next, " "}
puts