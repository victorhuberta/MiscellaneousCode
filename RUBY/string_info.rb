# A class to store information about a string.
# The string's attributes such as language, 
# word count, line count, and char count 
# are stored.

class StringInformation
	attr_accessor :language, :word_count, :line_count, :char_count

	def to_s
		<<-INFO
Language: #{self.language}
Line Count: #{self.line_count}
Word Count: #{self.word_count}
Char Count: #{self.char_count}
		INFO
	end
end

# A class to process a string from a file
# and store the string's information in
# an object of StringInformation.

class StringProcessor
	attr_accessor :str, :s_info

	# Prepare the string object with
	# additional methods.
	def prep
		class << @str
			def words
				self.split
			end
		end
	end

	# Read a string from a file
	def take fname
		File.open(fname, "r") do |f| 
			self.str = f.read 
		end if File.file? fname

		self.prep
	end

	# Write string information into a file
	def give fname
		File.open(fname, "w") { |f| f.write self.s_info }
	end

	# Process the string and store information
	# into a StringInformation object
	def crunch
		s_info = StringInformation.new

		s_info.char_count = @str.length
		s_info.line_count = @str.lines.size
		s_info.word_count = @str.words.size

		self.s_info = s_info
	end

	protected :prep

end

sp = StringProcessor.new

sp.take ARGV[0]
sp.crunch
sp.give ARGV[1]
