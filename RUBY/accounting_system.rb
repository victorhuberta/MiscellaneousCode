class Account
	attr_accessor :balance
	attr_reader :cleared_balance

	def initialize(balance)
		@balance = balance
		@cleared_balance = 0.0
	end

	def greater_balance_than?(other)
		@cleared_balance > other.cleared_balance
	end

	def cleared_balance=(cleared_balance)
		@cleared_balance = cleared_balance
	end
end

class Transaction
	def initialize(account_a, account_b)
		@account_a = account_a
		@account_b = account_b
	end

private
	def debit(account, amount)
		account.balance -= amount
	end

	def credit(account, amount)
		account.balance += amount
	end

public
	def transfer(amount)
		debit(@account_a, amount)
		credit(@account_b, amount)
		@account_a.cleared_balance = amount
		@account_b.cleared_balance = amount
	end
end

savings = Account.new(100)
checking = Account.new(200)

trans = Transaction.new(checking, savings)
trans.transfer(50)
puts savings.greater_balance_than?(checking)