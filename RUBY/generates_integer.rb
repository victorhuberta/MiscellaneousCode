def Integer.all
	Enumerator.new do |yielder, n=0|
		loop {yielder.yield(n+=1)}
	end
end

p Integer.all.first(10)
p Integer.all.select {|i| (i % 3).zero?}
p Integer.first(10)
