Recipe = Struct.new(:ingredients, :method)

recipe = Recipe.new({avocados: 4, jalapenos: 2}, ["Peel / Slice Avocados", "Chop jalapenos into small dice"] )

puts "Ingredients"
recipe.ingredients.each do |key, value|
	puts "* #{key}: #{value}"
end
puts "Method"
recipe.method.each_with_index do |step, index|
	puts "#{index + 1}. #{step}"
end