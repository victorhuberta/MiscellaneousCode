class Instrument
	attr_reader :serial_number, :name, :price
	instrument_count = 0
	spoilt_count = 0
	def initialize(serial_number, name, price)
		@serial_number = serial_number
		@name = name
		@price = price
	end

	def with_serial_number?(serial_number)
		@serial_number == serial_number
	end

	def to_s
		"Serial number : #{serial_number}; Name : #{name}; Price : #{price}"
	end
end

class MusicStoreActivity
	def show_all(instrumentList)
		instrumentList.each do |instrument|
			puts instrument
		end
	end

	def display(instrumentList, serial_number)
		instrumentList.select { |instrument| instrument.with_serial_number?(serial_number)}
	end
end

instrumentList = []
Guitar = Instrument.new("GUI01", "Metador", 1501.50)
Piano = Instrument.new("PIA02", "Fufuraku", 4000)
Ukulele = Instrument.new("UKU07", "Lesuna", 1250)
instrumentList.push(Guitar, Piano, Ukulele)
puts instrumentList
display(instrumentList, "PIA02")