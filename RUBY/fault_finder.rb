#!/usr/bin/ruby

# Written by codehectic
# 
# C-written-program Fault Finder.
# 
# Test arguments with increasing length on a command line
# program. This program will notify user as soon as
# an argument causes segmentation fault.
#
# Example of usage:
# ./fault_finder.rb /vulnerable/program/path
# 
# Example of output:
# Fault occurred at length: 600!
# Adjusting argument length...
# Fault found! Overflow number: 519
#
require 'open3'

class FaultFinder
	attr_accessor :program, :arg_type, :other_args

	CHAR = "A"
	INT = "9"

	def exec
		b = ""
		if self.arg_type.to_sym == :CHAR then
			b = CHAR
		else
			b = INT
		end
		
		arg = ""

		loop do
			arg += (b * 100)
			
			o, e, s = Open3.capture3("./#{self.program} #{arg}")

			if /SIGSEGV/i =~ s.to_s then
				puts "Fault occurred at length: #{arg.length}!",
				     "Adjusting argument length..."
				loop do
					if arg.length <= 0 then
						puts "Failed to find fault. Exiting..."
						break "failed"
					end

					arg = b * (arg.length - 1)
					o, e, s = Open3.capture3("./#{self.program} #{arg}")
					unless /SIGSEGV/i =~ s.to_s then
						puts "Fault found! Overflow number: #{arg.length}"
						break "done"
					end
				end

				break "done"
			end

		end

	end

end

ff = FaultFinder.new
ff.program = ARGV[0]
ff.arg_type = ARGV[1]
ff.exec
