void main()
{
    import std.stdio;

    string text = "D is a lot of fun";
    auto wc = wordCount(text);

    writeln(wc);
    foreach (pair; wc.byKeyValue) {
        writeln(pair);
    }

    if (auto countptr = "d" in wc) {
        (*countptr)++;
        writeln(wc);
    }

    writeln("Checking if 'hell' exists...");
    auto hell = wc.get("hell", -1);
    writeln("'hell' = ", hell);

    writeln("Removing 'fun'...");
    wc.remove("fun");
    writeln(wc);
}

int[string] wordCount(string text)
{
    import std.algorithm.iteration: splitter;
    import std.string: toLower;

    int[string] words;

    foreach (word; splitter(text.toLower(), " ")) {
        words[word]++;
    }

    return words;
}
