import std.stdio;

void main()
{
    auto arr = [1, 2, 3, 4, 5];
    auto arr2 = [5, 4, 3, 2, 1];
    int[25] arr3;

    foreach (e; arr) {
        foreach_reverse (a; arr2) {
            foreach (ref b; arr3) {
                if (b == 0) {
                    b = e + a;    
                    break;
                }
            }
        }
    }

    writeln(arr3);
}
