import std.stdio;

struct Person {
    string name;
    int age, height;
    static int totalPeople;

    this() @disable;

    this(string name, int age, int height) {
        this.name = name;
        this.age = age;
        this.height = height + 20;

        ++totalPeople;
    }

    // By default the function is public.
    void eat(string food) {
        writeln(name, " is eating ", food);
    }

    // It can't modify any of the other struct members.
    void birthday() const {
        // error: ++age;
        writeln(name, " is having their birthday!");
    }

    // Only callable by immutable objects.
    void do_nothing() immutable {
        writeln(name, " is doing nothing");
    }

    public void finish_eating() {
        writeln(name, " has just finished eaten");
        poop();
    }

    // Callable by other member functions of this struct
    // and other code in this module.
    private void poop() {
        writeln(name, " is secretly pooping...");
    }

    static int getTotalPeople() {
        return totalPeople;
    }
}

void main()
{
    Person p1 = Person("Jackson", 20, 180);
    writeln(p1);

    p1 = Person("Kitty", 17, 165);
    writeln(p1);

    Person p2 = Person("Coconut", 21, 163);
    writeln(p2);

    // Allocate in heap.
    Person* p3 = new Person("Brew", 15, 120);
    writeln(p3);
    writeln(*p3);

    // Re-assign p3 to another heap-allocated struct.
    // Previous struct has been de-allocated by GC.
    p3 = new Person("Korosensei", 60, 220);
    writeln(p3);
    writeln(*p3);

    p3.eat("fried rice");
    p3.finish_eating();
    p3.poop();
    p3.birthday();

    // A const struct can only call its const functions.
    const Person* p4 = new Person("Victor", 20, 180);
    writeln(p4);
    writeln(*p4);
    p4.birthday();

    immutable Person* statue = cast(immutable(Person*)) new Person("Bahamas", 1000, 1800);
    writeln(statue);
    writeln(*statue);
    statue.do_nothing();
    statue.birthday();

    Person* unstatue = cast(Person*) statue;
    unstatue.eat("Pizza");
    unstatue.birthday();

    int totalPeople = Person.getTotalPeople();
    writeln("Total People = ", totalPeople);
}
