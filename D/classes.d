class Any {
    protected {
        string type;
    }

    this(string type)
    {
        this.type = type;
    }

    final string getType()
    {
        return type;
    }

    abstract void increment();
}

class Integer: Any {
    private {
        int number;
    }

    this(int number)
    {
        super("integer");
        this.number = number;
    }

    override string toString()
    {
        import std.conv: to;

        return to!string(number);
    }

    override void increment()
    {
        number += 10;
    }
}

class Float: Any {
    private {
        float number;
    }

    this(float number)
    {
        super("float");
        this.number = number;
    }

    override string toString()
    {
        import std.string: format;

        return format("%.2f", number);
    }

    override void increment()
    {
        number += 4.6;
    }
}

void main()
{
    import std.stdio: writefln;

    Any[] numbers = [new Integer(4), new Float(9.98)];

    foreach (number; numbers) {
        number.increment();
        writefln("Number of type %s is %s", number.getType(),
                number.toString());
    }
}
