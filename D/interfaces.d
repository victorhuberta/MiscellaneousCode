interface Animal {
    void makeNoise();

    // NVI Pattern.
    final void makeMultipleNoises(int n)
    {
        for (auto i = 0; i < n; ++i) {
            makeNoise();
        }
    }
}

class Dog: Animal {
    protected {
        string noise;
    }

    this()
    {
        this.noise = "Bark";
    }

    override void makeNoise()
    {
        import std.stdio: writeln;

        writeln(noise, "!!");
    }
}

class Cat: Animal {
    protected {
        string noise;
    }

    this()
    {
        this.noise = "Meow";
    }

    override void makeNoise()
    {
        import std.stdio: writeln;

        writeln(noise, "~~");
    }
}

void main()
{
    Dog dog = new Dog;
    Cat cat = new Cat;
    Animal unknown = dog;

    dog.makeMultipleNoises(4);
    cat.makeMultipleNoises(4);
    unknown.makeNoise();
}
