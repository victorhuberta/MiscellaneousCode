import std.stdio;

void safeFun() @safe
{
    writeln("This is safe.");
    char* c = new char;
    writeln(c);
    *c = 'X';
    writeln(*c);

    trustedFun();
}

void trustedFun() @trusted
{
    writeln("This is trusted.");
    double* d = new double;
    writeln(d);
    writeln(d + 1);
    *d = 12345.6789;
    writeln(*d);
    writeln(*d + 10);

    unsafeFun();
}

void unsafeFun()
{
    writeln("This is unsafe.");

    int* i = new int;
    writeln(i);
    writeln(i + 0x5);
    *i = 10;
    writeln(*i);
    writeln(*i + 5);
}

void main()
{
    safeFun();
}
