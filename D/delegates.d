enum IntOps {
    add = 0,
    sub = 1,
    mul = 2,
    div = 3
}

auto getMathOperation(T)(IntOps op)
{
    auto add = (T lhs, T rhs) => lhs + rhs;
    auto sub = (T lhs, T rhs) => lhs - rhs;
    auto mul = (T lhs, T rhs) => lhs * rhs;
    auto div = (T lhs, T rhs) => lhs / rhs;

    switch (op) {
        case IntOps.add: return add;
        case IntOps.sub: return sub;
        case IntOps.mul: return mul;
        case IntOps.div: return div;
        default: assert(0);
    }
}

R runMathOperation(A1, A2, R)(R function(A1, A2) func, A1 a, A2 b)
{
    return func(a, b);
}

void main()
{
    import std.stdio: writeln;

    auto func = getMathOperation!int(IntOps.add);
    writeln("The type of func is ", typeof(func).stringof);
    writeln("Result = ", runMathOperation(func, 10, 20));
}
