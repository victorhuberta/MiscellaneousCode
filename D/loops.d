import std.stdio;

double average(int[] array)
{
    auto total = 0.0;
    auto length = array.length;

    import std.array: empty, front;

    while (! array.empty) {
        total += array.front;
        array = array[1 .. $];
    }

    return total / length;
}

void main()
{
    auto testers = [ [5, 15], // 20
        [2, 3, 2, 3], // 10
        [3, 6, 2, 9] ]; // 20

    for (int i = 0; i < testers.length; ++i) {
        writefln("The average of %s is %.2f",
            testers[i], average(testers[i]));
    }
}
