import std.stdio;

import Dgame.Window;
import Dgame.Window.Event;

import Dgame.System.Font;
import Dgame.System.StopWatch;

import Dgame.Graphic.Text;
import Dgame.Graphic.Color;

immutable ubyte FPS = 30;
immutable ubyte TICKS_PER_FRAME = 1000 / FPS;

void main()
{
    StopWatch sw;
    bool running = true;
    Event event;

    Window window = Window(640, 480, "Example 2");
    Font font = Font("/usr/share/fonts/truetype/droid/" ~
        "DroidSansMono.ttf", 20);
    Text curFps = new Text(font);

    window.setVerticalSync(Window.VerticalSync.Enable);

    while (running) {
        // if (sw.getElapsedTicks() < TICKS_PER_FRAME) continue;
        // sw.reset();

        window.clear();

        while (window.poll(&event)) {
            switch (event.type) {
                case Event.Type.Quit:
                    writeln("Quitting...");

                    Time time = sw.getTime();

                    writefln("Game has run for %d ms - %.2f secs - %.2f min",
                        time.msecs, time.seconds, time.minutes);

                    running = false;
                    break;
                default:
                    break;
            }
        }

        curFps.format("Vsync: %s; Current FPS: %d",
            window.getVerticalSync(), sw.getCurrentFPS());
        window.draw(curFps);

        window.display();
    }
}
