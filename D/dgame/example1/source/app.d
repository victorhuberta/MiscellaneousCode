import std.stdio;

import Dgame.Window;
import Dgame.Window.Event;

import Dgame.Graphic.Surface;
import Dgame.Graphic.Color;

import Dgame.System.StopWatch;
import Dgame.System.Keyboard;

void main()
{
    Window window = Window(640, 480, "Example Yo!");
    Surface icon = Surface("res/images/icon.jpg");

    window.setIcon(icon);
    window.setClearColor(Color4b.Blue);

    bool running = true;
    Event event;

    while (running) {
        window.clear();
        
        while (window.poll(&event)) {
            switch (event.type) {
                case Event.Type.Quit:
                    writeln("Quitting...");
                    running = false;
                    break;
                case Event.Type.KeyDown:
                    writeln("Pressed key: ", event.keyboard.key);

                    if (event.keyboard.key == Keyboard.Key.Esc) {
                        window.push(Event.Type.Quit);
                    }
                    break;
                case Event.Type.MouseButtonDown:
                    writefln("Mouse clicked on: %d, %d", event.mouse.button.x,
                        event.mouse.button.y);
                    break;
                case Event.Type.MouseMotion:
                    writefln("Mouse moves to: %d, %d in relative to: %d, %d",
                        event.mouse.motion.x, event.mouse.motion.y,
                        event.mouse.motion.rel_x, event.mouse.motion.rel_y);
                    break;
                case Event.Type.MouseWheel:
                    writefln("Mouse scrolls to: %d, %d", event.mouse.wheel.x,
                        event.mouse.wheel.y);
                    break;
                default:
                    break;
            }
        }

        window.display();
    }

    StopWatch.wait(2000); // Pause 2 seconds for no reason.
}
