import std.stdio;

import Dgame.Window;
import Dgame.Window.Event;
import Dgame.Graphic;
import Dgame.System;

import Dgame.Math.Vector2;
import Dgame.Math.Rect: Size;

immutable string DROID_SANS_MONO = "/usr/share/fonts/truetype/" ~
    "droid/DroidSansMono.ttf";
immutable string DUCK_IMG = "res/images/duck.png";

void main()
{
    Window window = Window(800, 600, "DUCK DUCK DUCK DUCK");
    window.setClearColor(Color4b.Green);
    // window.setVerticalSync(Window.VerticalSync.Enable);
    const Size wndSize = window.getSize();

    StopWatch sw;
    Font fpsFont = Font(DROID_SANS_MONO, 18);
    Text fps = new Text(fpsFont);

    Texture duckTex = Texture(Surface(DUCK_IMG));
    Sprite duck = new Sprite(duckTex);
    duck.setPosition(50, 100);
    int move_x = 4, move_y = 4;

    bool running = true;
    Event event;

    while (running) {
        window.clear();

        while (window.poll(&event)) {
            switch (event.type) {
                case Event.Type.Quit:
                    writeln("Quitting...");
                    running = false;
                    break;
                default:
                    break;
            }
        }

        fps.format("FPS: %d", sw.getCurrentFPS());
        window.draw(fps);

        const Vector2f duckPos = duck.getPosition();

        if (duckPos.x + duckTex.width >= wndSize.width || duckPos.x < 0) {
            move_x *= -1;
        }
        if (duckPos.y + duckTex.width >= wndSize.width || duckPos.y < 0) {
            move_y *= -1;
        }
        duck.move(move_x, move_y);
        window.draw(duck);

        window.display();
    }
}
