import std.stdio;

void main()
{
    int i = 10;
    i = multTwo(&i);
    writeln(i);

    const int* p1 = &i;
    // error: p1 = new int;
    // error: *p1 = 5;

    // error: immutable int* p2 = &i;
    // error: p2 = new int;
    // error: *p2 = 4;

    immutable int j = i;
    writeln(j);

    i = 3;
    writeln(i);
}

int multTwo(int* i)
{
    *i *= 2;
    return *i;
}
