import std.stdio;

void main()
{
    int[] test = [3, 9, 11, 7, 2, 76, 90, 6];
    auto min = minimum(test);
    writefln("The minimum of %s is %d", test, min);
    assert(min == 2);
}

/**
 * Recursively find the minimum integer.
 */
int minimum(int[] a)
{
    assert(a.length > 0);
    if (a.length == 1)
        return a[0];

    auto another = minimum(a[1 .. $]);
    return (a[0] < another) ? a[0] : another;
}
