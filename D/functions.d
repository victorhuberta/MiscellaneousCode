import std.stdio;
import std.random;

void main()
{
    randomCalculator();
    
    // Test if add() function is in scope.
    static assert(!__traits(compiles, add(1, 2)));
}

void randomCalculator()
{
    auto add(int a = 0, int b = 0) {
        return a + b;
    }

    auto sub(int a = 0, int b = 0) {
        return a - b;
    }

    auto mul(int a = 0, int b = 0) {
        return a * b;
    }

    auto div(int a = 0, int b = 1) {
        return a / b;
    }

    int a = 10;
    int b = 3;

    switch (uniform(0, 4)) {
        case 0:
            writeln(add(a, b));
            break;
        case 1:
            writeln(sub(a, b));
            break;
        case 2:
            writeln(mul(a, b));
            break;
        case 3:
            writeln(div(a, b));
            break;
        default:
            // UNREACHABLE
            assert(0);
    }
}
