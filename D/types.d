import std.stdio: writeln;

void main()
{
    bool what = true;
    assert(what.max == 1);
    assert(what.min == 0);
    writeln("what = ", what);
    what = false;
    writeln("what = ", what);

    byte b;
    assert(b == 0);
    writeln("byte.min = ", byte.min);
    writeln("byte.max = ", byte.max);

    int i = 255;
    ushort s = cast(ushort) i;
    writeln("i = ", i);
    writeln("s = ", s);

    writeln("dchar.min = ", dchar.min, "; dchar.max = ", dchar.max);
    
    double d = 6.25567891;
    float f = d; // demoted
    writeln("d = ", d);
    writeln("f = ", f);
    writeln("f.init = ", f.init);
    writeln("f.nan = ", f.nan);
    writeln("f.infinity = ", f.infinity);
    writeln("f.dig = ", f.dig);
    writeln("f.mant_dig = ", f.mant_dig);

    long l = 1_000_000_000;

    writeln("real.min = ", real.min_normal);
    writeln("real.max = ", real.max);
    writeln("real.init = ", real.init);
    writeln("real.stringof = ", real.stringof);

    writeln("size_t.sizeof = ", size_t.sizeof);

    auto text = "lorem ipsum";
    writeln("text = ", text);
    writeln("text.stringof = ", text.stringof);
    writeln("typeof(text).stringof = ", typeof(text).stringof);
}
