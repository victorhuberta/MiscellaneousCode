import std.stdio;

void main()
{
    char[] input = ['w', 'e', 'l', 'c', 'o', 'm', 'e',
        't', 'o', 'd'];

    writeln("Before: ", input);
    auto encrypted = encrypt(input, 16);
    writeln("After: ", encrypted);

    assert(encrypted == ['m', 'u', 'b', 's', 'e', 'c',
        'u', 'j', 'e', 't']);
}

char[] encrypt(char[] input, char shift)
{
    auto result = input.dup;

    for (int i = 0; i < input.length; ++i) {
        result[i] += shift;
        if (result[i] > 'z')
            result[i] -= 26;
    }

    return result;
}
