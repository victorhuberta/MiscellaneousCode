class DangerousException: Exception {
    this(string msg, string file = __FILE__, size_t line = __LINE__)
    {
        super(msg, file, line);
    }
}

class HarambeException: DangerousException {
    this(string msg, string file = __FILE__, size_t line = __LINE__)
    {
        super(msg, file, line);
    }
}

void dangerousOperation()
{
    throw new DangerousException("Something dangerous just happened!");

    // Secret exceptions; will never be thrown.
    throw new HarambeException("Was he just a gorilla?");
    throw new HarambeException("#dicksoutforharambe");
}

int safeOperation() nothrow
{
    // dangerousOperation(); // Sneaky bastard

    int a = 1 + 1; // Doing something safe.
    return a;
}

void main()
{
    import std.stdio: writeln;
    import std.exception: enforce, collectException;

    try {
        dangerousOperation();
    } catch (DangerousException e) {
        writeln(typeof(e).stringof);
        writeln(e.msg);
        writeln(e.file);
        writeln(e.line);
        writeln(e.info);
    } catch (Exception e) {
        writeln("You know, just in case.");
    } finally {
        writeln("Nice guys finish last.");
    }

    int a = safeOperation();

    // Like his older cousin assert(), but he throws exceptions instead.
    enforce(a == 2, "Illuminati confirmed. 1 + 1 /= 2");
    enforce!HarambeException(a == 2, "WTF...");

    auto e = collectException(dangerousOperation);
    if (e) {
        writeln("Collected exception: ", e);
    }
}
