import std.stdio;

void main()
{
    int i = 5;

    string classification = classifyNumber(i);
    writeln(classification);
}

string classifyNumber(int i)
{
    if (i < 0)
        return "BelowZero";
    
    switch (i) {
        case 0: .. case 9:
            return "Within0to9";
        case 10:
            return "IsATen";
        default:
            return "Unclassified";
    }
}
