class Shelter(T) {
    string shelterName;
    T[] animals;

    this(string shelterName, T[] animals = [])
    {
        this.shelterName = shelterName;
        this.animals = animals;
    }

    void mate(T male, T female)
    {
        import std.stdio: writefln;

        if (male.getName() == "") return;
        if (female.getName() == "") return;

        writefln("Mating %s with %s...", male.getName(),
            female.getName());

        writefln("We have a %s%s", male.getName()[0 .. ($ / 2)],
            female.getName()[($ / 2) .. $]);
    }

    void giveBirths()
    {
        foreach (animal; animals) {
            animal.reproduce();
        }
    }
}

interface Animal {
    void reproduce();
}

class FarmAnimal: Animal {
    protected {
        string name;
        FarmAnimal child;
    }

    this()
    {
        name = "";
        child = null;
    }

    this(string name, FarmAnimal child)
    {
        this.name = name;
        this.child = child;
    }

    string getName()
    {
        return name;
    }

    override void reproduce()
    {
        import std.stdio: writefln;

        if (child is null) return;

        writefln("%s gives birth to %s", name, child.toString());
    }
}

class Cow: FarmAnimal {
    this()
    {
        super();
    }

    this(string name, FarmAnimal child)
    {
        super(name, child);
    }
}

class Calf: Cow {
    override string toString()
    {
        return "Calf";
    }
}

class Chicken: FarmAnimal {
    this()
    {
        super();
    }

    this(string name, FarmAnimal child)
    {
        super(name, child);
    }

    override string toString()
    {
        return "Chicken";
    }
}

class Chick: Chicken {
    override string toString()
    {
        return "Chick";
    }
}

class Horse: FarmAnimal {
    this()
    {
        super();
    }

    this(string name, FarmAnimal child)
    {
        super(name, child);
    }

    override string toString()
    {
        return "Horse";
    }
}

class Colt: Horse {
    override string toString()
    {
        return "Colt";
    }
}

void main()
{
    auto larry = new Cow("Larry", new Calf);
    auto numi = new Chicken("Numi", new Chick);
    auto pegasus = new Horse("Pegasus", new Colt);

    FarmAnimal[] farmAnimals = [larry, numi, pegasus];

    auto shelter = new Shelter!FarmAnimal("The Joy", farmAnimals);
    shelter.mate(larry, numi);
    shelter.mate(numi, pegasus);
    shelter.mate(pegasus, larry);
    shelter.mate(numi, larry);
    shelter.mate(larry, pegasus);
    shelter.mate(pegasus, numi);
    shelter.giveBirths();
}
