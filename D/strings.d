import std.stdio;
import std.range: walkLength;
import std.uni: byGrapheme;
import std.string: format;

void main()
{
    string str = format("%s %s", "Hellö", "Wörld");
    writeln("My string: ", str);
    writeln("Array length (code units) of string: ", str.length);
    writeln("Range length (code points) of string: ", str.walkLength);
    writeln("Character length (graphemes) of string: ",
        str.byGrapheme.walkLength);

    import std.array: replace;
    writeln(replace(str, "lö", "lo"));
    
    import std.algorithm: endsWith;
    writefln("Does %s end with 'rld'? %s", str, endsWith(str, "rld"));

    import std.conv: to;
    dstring dstr = to!dstring(str);
    writeln("My dstring: ", dstr);

    alias statue = immutable(int)[];
    statue a = [10, 20, 3, 4, 5];
    writeln(a);
    // a[1] = 99; // error!

    string lstr = q{
        You know how long it took for that guy to recognize me!?
        Over 9999!!!
    };
    writeln(lstr);

    string rstr = `\n\n\nraw string\t, yo "man"!\n\n`;
    string rstr2 = r"this is also raw\n!\t, damn";
    string nrstr = "this isn't raw\n\n\t";
    writeln(rstr);
    writeln(rstr2);
    writeln(nrstr);
}
