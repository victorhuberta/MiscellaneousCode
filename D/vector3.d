struct Vector3 {
    double x;
    double y;
    double z;

    double length() const {
        import std.math: sqrt;
        return sqrt((x * x) + (y * y) + (z * z));
    }

    double dot(Vector3 rhs) const {
        return (x * rhs.x) + (y * rhs.y) + (z * rhs.z);
    }

    string toString() const {
        import std.string: format;
        return format("x: %.1f y: %.1f z: %.1f", x, y, z);
    }
}

void main() {
    auto vec1 = Vector3(10, 0, 0);
    Vector3 vec2;
    vec2.x = 0;
    vec2.y = 20;
    vec2.z = 0;

    assert(vec1.length == 10);
    assert(vec2.length == 20);

    assert(vec1.dot(vec2) == 0);

    auto vec3 = Vector3(1, 2, 3);
    assert(vec3.dot(Vector3(1, 1, 1)) == 6);

    assert(vec3.dot(Vector3(3, 2, 1)) == 10);

    import std.stdio: writeln, writefln;
    writeln("vec1 = ", vec1);
    writefln("vec2 = %s", vec2);
    
    assert(vec1.toString() == "x: 10.0 y: 0.0 z: 0.0");
    assert(vec2.toString() == "x: 0.0 y: 20.0 z: 0.0");
}
