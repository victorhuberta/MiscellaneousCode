struct FiboRange {
    int[] fiboarr;
    int last; // fiboarr's end of array index.
    immutable int blockSz = 50; // fiboarr's allocation block size.

    int[] fiboslice;
    int start, end;

    this() @disable;

    this(int start, int end)
    {
        this.start = start;
        this.end = end;
        fiboarr = new int[50];
        generateFibo();
        fiboslice = getFiboSlice();
    }

    bool empty() const @property
    {
        return fiboslice.length == 0;
    }

    void popFront()
    {
        fiboslice = fiboslice[1 .. $];
    }

    int front() const @property
    {
        return fiboslice[0];
    }

    private void generateFibo()
    {
        auto a = 1, b = 1;

        void add(int[] fiboarr, int item)
        {
            fiboarr[last++] = item;
            if (last >= blockSz)
                fiboarr.length += blockSz;
        }

        for (int i = 0; i < fiboarr.length; ++i) {
            add(fiboarr, a);
            auto tmp = b;
            b = a + b;
            a = tmp;
        }
    }

    private int[] getFiboSlice() const
    {
        int start_idx, end_idx;

        for (int i = 0; i < last; ++i) {
            if (start <= fiboarr[i]) {
                start_idx = i;
                break;
            }
        }

        for (int i = last - 1; i >= 0; --i) {
            if (end <= fiboarr[i]) {
                end_idx = i;
                break;
            }
        }

        return cast(int[]) fiboarr[start_idx .. end_idx];
    }
}

void main()
{
    import std.stdio;
    import std.range: take;
    import std.array: array;

    FiboRange* fib = new FiboRange(0, 60);
    auto fib10 = take(fib, 10);

    int[] the10Fibs = array(fib10);
    writeln("The 10 first Fibonacci numbers: ", the10Fibs);
    assert(the10Fibs == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]);
}
