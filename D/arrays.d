import std.stdio;

void main() @safe
{
    int[] a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    int[] b = new int[10];
    b[0] = 2, b[1] = 3, b[2] = 4;

    int[] c = a[] ~ b[];

    int[10] d;
    d[] = a[] + b[];

    int[] e = a.dup;
    
    writeln(a);
    writeln(b);
    writeln(c);
    writeln(d);

    writeln("a.ptr = ", a.ptr);
    writeln("b.ptr = ", b.ptr);
    writeln("c.ptr = ", c.ptr);
    writeln("d.ptr = ", d.ptr);
    writeln("e.ptr = ", e.ptr);
}
