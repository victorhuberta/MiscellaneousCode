#!/bin/bash
# Get all links in a HTML document.

url="$@"
tmphtml=$(mktemp -q -t "XXXXXX.html")
linksf=$(mktemp -q -t "XXXXXX.links")

if ! [[ -r "$tmphtml" ]] || ! [[ -r "$linksf" ]]; then
    echo "$0: failed to create html file. Aborting..."
    exit 1
fi

wget "$url" -O "$tmphtml" &>/dev/null
if [[ $? -ne 0 ]]; then
    echo "$0: download failed. Aborting..."
    rm -rf "$tmphtml" "$linksf"
    exit 1
fi

links=$(sed -n '
s/.*href="\([^"]*\)".*/\1/p
s/.*src="\([^"]*\)".*/\1/p' < "$tmphtml" | sort -u)

echo "$links" | tee "$linksf"
echo
total_links=$(wc -l "$linksf" | sed -n 's/^\([0-9]\+\) .*/\1/p')
echo "Total links: $total_links"

rm -rf "$tmphtml" "$linksf"
