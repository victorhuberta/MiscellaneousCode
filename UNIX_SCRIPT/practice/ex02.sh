#!/bin/bash
# Organize files into folders based on file size.
# 0k contains files that are less than 1K,
# 1k contains files between 1k and 2k in size,
# 2k contains files between 2k and 3k in size,
# etc.

if [ $# -ne 1 ]; then
    echo "Usage: $0 dirpath"
    exit 2
fi

dirpath="$1"
if ! [ -d "$dirpath" ]; then
    echo "$0: $dirpath is invalid."
    exit 1
fi

cd "$dirpath"

fszlist=$(du *)

echo "$fszlist" | while read -r line
do
    fsize=$(echo "$line" | sed 's/\(.*\)\t\(.*\)/\1/')
    dirname="${fsize}k"
    if ! [ -d "$dirname" ]; then
        mkdir -p "$dirname"
    fi

    fname=$(echo "$line" | sed 's/\(.*\)\t\(.*\)/\2/')
    if [ "$dirname" != "$fname" ]; then
        mv "$fname" "${dirname}/" &> /dev/null
    fi
done
