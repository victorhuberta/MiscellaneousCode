#!/bin/bash
# Find the user with the highest average score
# based on a file with the following format:
#
# Guna:23:45:56
# Andy:46:67:78
# Naomi:90:56:67
#
# Output the user ID and average score to STDOUT.

if [ $# != 1 ]; then
    echo "Usage: $0 score_file"
    exit 2
fi

score_file="$1"
top_user=""
top_avg=0

while read -r line
do
    username=$(echo "$line" | gawk -F: '{ print $1 }')
    average=$(echo "$line" | gawk -F: '{ print ($2 + $3 + $4) / 3 }')

    if [ 1 -eq "$(echo "$average > $top_avg" | bc)" ]; then
        top_user=$username
        top_avg=$average
    fi
done < "$score_file"

echo "User with highest average score is $top_user with $top_avg."
