const int LED = 9;
const int BUTTON = 7;
int old = 0;
int state = 0;
int toggle = 0;
int brightness = 0;
unsigned long start_time = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT);
}

void loop()
{
  state = digitalRead(BUTTON);
  
  if ((state == HIGH) && (old != state)) {
    toggle = ! toggle;
    start_time = millis();
  } else if ((state == HIGH) && (old == state)) {
    if (millis() - start_time > 500) {
      ++brightness;
      
      Serial.println(brightness);
      
      if (brightness > 255) brightness = 0;
      analogWrite(LED, brightness);
      delay(20);
    }
  }
  
  analogWrite(LED, (toggle) ? brightness : 0);
  old = state;
  delay(10);
}
