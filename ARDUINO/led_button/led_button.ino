const int LED = 12;
const int BUTTON = 7;
int state, old_state, toggle = 0;

void setup()
{
  pinMode(BUTTON, INPUT);
  pinMode(LED, OUTPUT);
}

void loop()
{
  state = digitalRead(BUTTON);

  if ((state == HIGH) && (old_state != state)) {
    toggle = ! toggle;
    delay(10); // de-bouncing
  }
  
  old_state = state;
  
  digitalWrite(LED, (toggle) ? HIGH : LOW);
}
