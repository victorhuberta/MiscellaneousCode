class Static {
    double x, z;

    this(double x, double z)
    {
        this.x = x;
        this.z = z;
    }

    void update(Static s)
    {
        x += s.x;
        z += s.z;
    }

    Static opBinary(string op)(Static rhs)
    {
        double newX = mixin("x" ~ op ~ "rhs.x");
        double newZ = mixin("z" ~ op ~ "rhs.z");
        return Static(newX, newZ);
    }

    Static opBinary(string op)(int rhs)
    {
        return Static(mixin("x" ~ op ~ "rhs"),
            mixin("z" ~ op ~ "rhs"));
    }
}

struct SteeringOutput {
    Static linear;
    float angular;

    this() {
        linear = new Static(0.0, 0.0);
    }

    SteeringOutput opBinary(string op)(SteeringOutput rhs)
    {
        Static newLinear = mixin("linear" ~ op ~ "rhs.linear");
        float newAngular = mixin("angular" ~ op ~ "rhs.angular");
        return SteeringOutput(newLinear, newAngular);
    }
}

class Kinematic {
    Static position;
    float orientation;
    Static velocity;
    float rotation;

    this() {
        position = new Static(0.0, 0.0);
        velocity = new Static(0.0, 0.0);
    }

    double findDistance(double change, double accel, int time)
    {
        return change * time + 0.5 * accel * time * time;
    }

    Static findLinearDistance(Static velocity, Static linear, int time)
    {
        double xChange = findDistance(velocity.x, linear.x, time); 
        double zChange = findDistance(velocity.z, linear.z, time); 
        return Static(xChange, zChange);
    }

    void update(SteeringOutput steering, int time)
    {
        position.update(findStaticDistance(velocity, steering.linear, time));
        orientation += findDistance(rotation, steering.angular, time);

        velocity += steering.linear * time;
        rotation += steering.angular * time;
    }
}

float getNewOrientation(float currentOrientation, Static velocity)
{
    import std.math: atan2;

    if (velocity != null) {
        return atan2(-velocity.z, velocity.x);
    }

    return currentOrientation;
}

class Character {
    Static position;
    float orientation;

    this() {
        position = new Static();
    }
}

class KinematicSeek {
    Character character;
    Character target;

    Static maxSpeed;

    this(Character character, Character target) {
        this.character = character;
        this.target = target;
        maxSpeed = new Static(100.0, 100.0);
    }

    SteeringOutput getSteering() {
        auto steering = SteeringOutput();
        steering.linear = target.position - character.position;
        steering.linear *= maxSpeed;
        steering.angular = 0.0;

        character.orientation = getNewOrientation(character.orientation,
            steering.linear);

        return steering;
    }
}

void main(string[] args)
{
    // TODO: Test Kinematic class.
}
