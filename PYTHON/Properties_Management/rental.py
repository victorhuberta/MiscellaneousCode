from property import validate_input
from house import House
from apartment import Apartment

class Rental:
	valid_furnished = ("yes", "no")

	def __init__(self, furnished='', utilities='',
			rent='', **kwargs):
		super().__init__(**kwargs)
		self.furnished = furnished
		self.utilities = utilities
		self.rent = rent

	def display(self):
		super().display()
		print("=======================")
		print("   RENTAL DETAILS")
		print("=======================")
		print("Rent : {}".format(self.rent))
		print("Utilities : {}".format(self.utilities))
		print("Furnished : {}".format(self.furnished))

	def prompt_init():
		rent = input("what is the monthly rent ? : ")
		utilities = input("what are the estimated utilities ? : ")
		furnished = validate_input(
			"Is the property furnished ?",
			Rental.valid_furnished)
		return dict(rent=rent, utilities=utilities,
				furnished=furnished)
	prompt_init = staticmethod(prompt_init)

class HouseRental(Rental, House):
	def prompt_init():
		house_rental_dict = House.prompt_init()
		house_rental_dict.update(Rental.prompt_init())
		return house_rental_dict
	prompt_init = staticmethod(prompt_init)

class ApartmentRental(Rental, Apartment):
	def prompt_init():
		apartment_rental_dict = Apartment.prompt_init()
		apartment_rental_dict.update(Rental.prompt_init())
		return apartment_rental_dict

	prompt_init = staticmethod(prompt_init)


