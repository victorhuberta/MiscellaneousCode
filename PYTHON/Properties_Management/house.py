from property import Property, validate_input

class House(Property):
	valid_garage = ("attached", "detached", "none")
	valid_fenced_yard = ("yes", "no")

	def __init__(self, num_stories=0, garage='',
		fenced_yard=False, **kwargs):

		super().__init__(**kwargs)
		self.num_stories = num_stories
		self.garage = garage
		self.fenced_yard = fenced_yard

	def display(self):
		super().display()
		print("=======================")
		print("   HOUSE DETAILS")
		print("=======================")
		print("Number of stories : {}".format(self.num_stories))
		print("Garage : {}".format(self.garage))
		print("Fenced yard : {}".format(self.fenced_yard))
		print()

	def prompt_init():
		parent_dict = Property.prompt_init()

		num_stories = input("How many stories? :")
		garage = validate_input(
			"Is there a garage ?",
			House.valid_garage)
		fenced_yard = validate_input(
			"Is the yard fenced ?",
			House.valid_fenced_yard)

		parent_dict.update(
			{
				"num_stories" : num_stories,
				"garage" : garage,
				"fenced_yard" : fenced_yard
			})

		return parent_dict
	prompt_init = staticmethod(prompt_init)