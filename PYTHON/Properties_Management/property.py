class Property:

	def __init__(self, square_feet='', num_bedrooms='',
				num_bathrooms='', **kwargs):
		super().__init__(**kwargs)
		self.square_feet = square_feet
		self.num_bedrooms = num_bedrooms
		self.num_bathrooms = num_bathrooms

	def display(self):
		print("=======================")
		print("   PROPERTY DETAILS")
		print("=======================")
		print("Square footage : {}".format(self.square_feet))
		print("Number of bedrooms : {}".format(self.num_bedrooms))
		print("Number of bathrooms : {}".format(self.num_bathrooms))
		print()

	def prompt_init():
		square_feet = input("Enter the square footage : ")
		num_bedrooms = input("Enter the number of bedrooms : ")
		num_bathrooms = input("Enter the number of bathrooms : ")
		return dict(square_feet=square_feet, num_bedrooms=num_bedrooms,
					num_bathrooms=num_bathrooms)
	prompt_init = staticmethod(prompt_init)

def validate_input(input_str, options):
	input_str += " ({}) : ".format(', '.join(options))
	response = input(input_str)
	while response.lower() not in options:
		response = input(input_str)
	return response.lower()