from house import House
from apartment import Apartment

class Purchase:
	def __init__(self, price='', taxes='', **kwargs):
		super().__init__(**kwargs)
		self.price = price
		self.taxes = taxes

	def display(self):
		super().display()
		print("=======================")
		print("   PURCHASE DETAILS")
		print("=======================")
		print("Price : {}".format(self.price))
		print("Taxes : {}".format(self.taxes))
		print()

	def prompt_init():
		price = input("What is the selling price ? : ")
		taxes = input("What are the estimated taxes ? : ")
		return dict(price=price, taxes=taxes)

	prompt_init = staticmethod(prompt_init)

class HousePurchase(Purchase, House):
	def prompt_init():
		house_purchase_dict = House.prompt_init()
		house_purchase_dict.update(Purchase.prompt_init())
		return house_purchase_dict
	prompt_init = staticmethod(prompt_init)

class ApartmentPurchase(Purchase, Apartment):
	def prompt_init():
		apartment_purchase_dict = Apartment.prompt_init()
		apartment_purchase_dict.update(Purchase.prompt_init())
		return apartment_purchase_dict

	prompt_init = staticmethod(prompt_init)
