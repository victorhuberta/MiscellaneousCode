from property import Property, validate_input

class Apartment(Property):
	valid_laundries = ("coin", "ensuite", "none")
	valid_balconies = ("yes", "no", "solarium")

	def __init__(self, balcony='', laundry='', **kwargs):
		super().__init__(**kwargs)
		self.balcony = balcony
		self.laundry = laundry

	def display(self):
		super().display()
		print("=======================")
		print("   APARTMENT DETAILS")
		print("=======================")
		print("Balcony : {}".format(self.balcony))
		print("Laundry facility : {}".format(self.laundry))
		print()

	def prompt_init():
		parent_dict = Property.prompt_init()

		laundry = validate_input(
			"What laundry facilities does the "
			"property have?",
			Apartment.valid_laundries)
		balcony = validate_input(
			"Does the property have a balcony?",
			Apartment.valid_balconies)

		parent_dict.update(
			{
				"laundry" : laundry,
				"balcony" : balcony
			})
		return parent_dict
	prompt_init = staticmethod(prompt_init)