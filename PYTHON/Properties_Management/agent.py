from purchase import HousePurchase, ApartmentPurchase
from rental import HouseRental, ApartmentRental
from property import validate_input

class Agent:
	valid_property_types = ("house", "apartment")
	valid_payment_types = ("purchase", "rental")

	def __init__(self):
		self.property_list = []
		self.type_map = {
			("house", "rental"): HouseRental,
			("house", "purchase"): HousePurchase,
			("apartment", "rental"): ApartmentRental,
			("apartment", "purchase"): ApartmentPurchase
		}

	def list_properties(self):
		for property in self.property_list:
			property.display()

	def add_property(self):
		property_type = validate_input(
			"What type of property ?",
			Agent.valid_property_types)
		payment_type = validate_input(
			"What payment type ?",
			Agent.valid_payment_types)

		PropertyClass = self.type_map[
			(property_type, payment_type)]

		init_args = PropertyClass.prompt_init()
		self.property_list.append(PropertyClass(**init_args))

