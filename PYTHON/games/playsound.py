import pygame, sys, time
from pygame.locals import *

def main():
	pygame.init()
	DISPLAYSURF = pygame.display.set_mode((400,300))
	pygame.display.set_caption("Play Wolf Sound")
	soundObj = pygame.mixer.Sound("wolves.wav")
	pygame.mixer.music.load("wolves.wav")
	pygame.mixer.music.play(-1, 0.0)	
	while True:
		for event in pygame.event.get():
			if event.type == QUIT:
				pygame.quit()
				sys.exit()
			time.sleep(1)
			soundObj.play()
			time.sleep(3)
			soundObj.stop()
		pygame.display.update()

if __name__ == "__main__":
	main()
