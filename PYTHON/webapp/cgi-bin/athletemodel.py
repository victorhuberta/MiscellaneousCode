#! /usr/bin/python3

import pickle
from athletelist import AthleteList
from athletelist import get_coach_data

def put_to_store(files_list):
	athletes = {}
	for each_file in files_list:
		athlete = AthleteList(get_coach_data(each_file))
		athletes[athlete.name] = athlete

	try:
		with open("athletes.pickle", "wb") as athletes_data:
			pickle.dump(athletes, athletes_data)
	except IOError as ioerr:
		print("IOError (put_to_store): " + str(ioerr))
	return athletes

def get_from_store():
	athletes = {}
	try:
		with open("athletes.pickle", "rb") as athletes_data:
			athletes = pickle.load(athletes_data)
	except IOError as ioerr:
		print("IOError (get_from_store): " + str(ioerr))

	return athletes

def get_names_from_store():
	athletes = get_from_store()
	response = [athletes[each_athlete].name for each_athlete in athletes]
	return(response)