def get_coach_data(filename=""):
	try:
		with open(filename, "r") as record:
					data = record.readline().split(",")
	except IOError as ioerr:
		print("IOError : ", str(ioerr))
		exit()
	athlete = {'name':data.pop(0),
			 'DOB':data.pop(0),
			  'times':data}
	return athlete

class AthleteList(list):
	def __init__(self, data={}):
		list.__init__([])
		self.name = data['name']
		self.dob = data['DOB']
		self.extend(data['times'])

	@property
	def to_dict(self):
		dict = {'name' : self.name, 'DOB' : self.dob, 'top3' : self.top3}
		return dict

	@property
	def top3(self):
		times = sorted(set(self.sanitize_times()))
		return times[0:3]

	def sanitize_times(self):
		cleaned_records = []
		for item in self:
			if '-' in item:
				splitter = '-'
			elif ':' in item:
				splitter = ':'
			else:
				splitter = '.'
			(mins, secs) = item.split(splitter)
			cleaned_records.append(mins + "." + secs)
		return cleaned_records
		
"""
filenames = ["james.txt", "julie.txt", "mikey.txt", "sarah.txt"]
athletes = []
for filename in filenames:
	athlete = AthleteList(get_coach_data(filename))
	athletes.append(athlete)

for athlete in athletes:
	print("%s's best times are : %s" % (athlete.name, athlete.top3()))
"""