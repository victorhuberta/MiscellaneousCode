#!/usr/bin/python3

import yate
import athletemodel
import cgi
import json

form_data = cgi.FieldStorage()

athletes = athletemodel.get_from_store()
athlete_name = form_data['which_athlete'].value
print(yate.start_response('application/json'))
print(json.dumps(athletes[athlete_name].to_dict))