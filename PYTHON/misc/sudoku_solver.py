def reset_sudoku_box(sudoku_box, boxes):
	del sudoku_box[0:len(sudoku_box)]
	del boxes[0:len(boxes)]
	for x in range(9):
		boxes.append([])

	for x in range(9):
		sudoku_box.append([])

	for x in sudoku_box:
		for y in range(9):
			x.append(None)

def add_to_box(x, y, num):
	if 1 <= x <= 3:
		if 1 <= y <= 3:
			boxes[0].append(int(num))
		elif 4 <= y <= 6:
			boxes[3].append(int(num))
		elif 7 <= y <= 9:
			boxes[6].append(int(num))
	elif 4 <= x <= 6:
		if 1 <= y <= 3:
			boxes[1].append(int(num))
		elif 4 <= y <= 6:
			boxes[4].append(int(num))
		elif 7 <= y <= 9:
			boxes[7].append(int(num))
	elif 7 <= x <= 9:
		if 1 <= y <= 3:
			boxes[2].append(int(num))
		elif 4 <= y <= 6:
			boxes[5].append(int(num))
		elif 7 <= y <= 9:
			boxes[8].append(int(num))

def check_from_box(x, y, num):
	if 1 <= x <= 3:
		if 1 <= y <= 3:
			if int(num) in boxes[0]:
				return 1
		elif 4 <= y <= 6:
			if int(num) in boxes[3]:
				return 1
		elif 7 <= y <= 9:
			if int(num) in boxes[6]:
				return 1
	elif 4 <= x <= 6:
		if 1 <= y <= 3:
			if int(num) in boxes[1]:
				return 1
		elif 4 <= y <= 6:
			if int(num) in boxes[4]:
				return 1
		elif 7 <= y <= 9:
			if int(num) in boxes[7]:
				return 1
	elif 7 <= x <= 9:
		if 1 <= y <= 3:
			if int(num) in boxes[2]:
				return 1
		elif 4 <= y <= 6:
			if int(num) in boxes[5]:
				return 1
		elif 7 <= y <= 9:
			if int(num) in boxes[8]:
				return 1
	return 0

sudoku_box = []
boxes = []
reset_sudoku_box(sudoku_box, boxes)
f = open("sudoku_problem.txt", "r")
problem = [item for item in f.read().split(";")]
f.close()

for item in problem:
	str_pos, num = item.split("=")
	pos = tuple(str_pos.split(",")) 
	x,y = int(pos[0]), int(pos[1])
	sudoku_box[x - 1][y - 1] = int(num)
	add_to_box(x, y, num)

exists = 0
num_list = list(range(1,10))
status = "UNSOLVED"
from random import shuffle

while status == "UNSOLVED":
	reset_sudoku_box(sudoku_box, boxes)
	for x in range(len(sudoku_box)):
		for y in range(len(sudoku_box[x])):
			if sudoku_box[x][y] is None:
				for num in num_list:
					exists = check_from_box(x, y, num)
					if exists == 1:
						exists = 0
						continue
					for col_index in range(9):
						if sudoku_box[col_index][y] == num:
							exists = 1
							break
					if exists == 1:
						exists = 0
						continue
					for row_index in range(9):
						if sudoku_box[x][row_index] == num:
							exists = 1
							break
					if exists == 1:
						exists = 0
						continue
					sudoku_box[x][y] = num
					add_to_box(x, y, num)
					break
	status = "SOLVED"
	for x in range(len(sudoku_box)):
		for y in range(len(sudoku_box[x])):
			if sudoku_box[x][y] is None:
				status = "UNSOLVED"
				break
		if status == "UNSOLVED":
			print("Problem hasn't yet been solved. Trying other combinations...")
			shuffle(num_list)
			break
	
print(sudoku_box)



