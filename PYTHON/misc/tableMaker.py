def isPrime(n):
  if (n > 1):
    for x in range(2, n):
      if (n % x == 0):
        return False;
    return True;
  else:
    return False;

def makeTable(formula, max, increment):
  separator = "  |  ";
  print(separator.join(("n", formula, "Is Prime?")));
  for n in range(max + 1):
    result = eval(formula.replace("n", str(n)));
    isPrimeText = "";
    if (isPrime(result)):
      isPrimeText = "Yes";
    else:
      isPrimeText = "No";

    rowText = separator.join((str(n), str(result), isPrimeText));
    print(rowText);

def main():
  makeTable("3 ** n - 2 ** n", 5, 1);

if __name__ == "__main__":
  main();
