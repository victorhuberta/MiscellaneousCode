import matplotlib.pyplot as plt

plt.figure()

x_series = [item for item in range(10000, 56000, 5000)]

sdata_file = open("selectionSort_records.txt", "r")
idata_file = open("insertionSort_records.txt", "r")

y_series = [sdata for sdata in sdata_file.read().split('\n') if sdata is not '']
y_series2 = [idata for idata in idata_file.read().split('\n') if idata is not '']

plt.plot(x_series, y_series, label='Selection Sort')
plt.plot(x_series, y_series2, label='Insertion Sort')
plt.legend()

plt.xlabel("Data Size")
plt.ylabel("Sorting Time")
plt.title("Selection & Insertion Sort Graph")

plt.savefig("sort_graph.png")