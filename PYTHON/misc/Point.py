import math

class Point:
	'Represents a point in two-dimensional geometric coordinates'

	def __init__(self, x=0, y=0):
		'''Initialize the position of a new point. The x and y
			coordinates can be specified. If they are not, the point
			defaults to the origin.'''

		self.move(x, y)

	def reset(self):
		"Move the point to a new location in two-dimensional space."

		self.x = 0
		self.y = 0

	def move(self, x, y):
		'Reset the point back to the geometric origin: 0, 0'

		self.x = x
		self.y = y

	def calculate_distance(self, p):
		"""Calculate the distance from this point to a second point
			passed as a parameter.
			This function uses the Pythagorean Theorem to calculate
			the distance between the two points. The distance is returned
			as a float."""

		return math.sqrt (
			(self.x - p.x) ** 2 +
			(self.y - p.y) ** 2)

p1 = Point()
p2 = Point()

p1.reset()
p2.move(5,0)

p1p2distance = p1.calculate_distance(p2)
p2p1distance = p2.calculate_distance(p1)

assert (p1p2distance == p2p1distance)

print(p1p2distance)
print(p2p1distance)