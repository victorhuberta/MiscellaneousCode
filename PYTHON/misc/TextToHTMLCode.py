def textToHTML(text):
	char_list = ['a','b','c','d','e','f','g','h','i','j',
				'k','l','m','n','o','p','q','r','s','t',
				'u','v','w','x','y','z','0','1','2','3',
				'4','5','6','7','8','9','10']
	special_chars = {
		'&':'&#38;',
		'#':'&#35;',
		' ':'&#32;',
		'/':'&#47;',
		'\\':'&#92',
		';':'&#59;',
		':':'&#58;',
		'?':'&#63;',
		'@':'&#64;',
		'(':'&#40;',
		')':'&#41;',
		'[':'&#91;',
		']':'&#93;',
		"'":'&#39;',
		'"':'&#34;',
		'`':'&#96;',
		'<':'&#60;',
		'>':'&#62;',
		'=':'&#61;',
		'+':'&#43;',
		'%':'&#37;'
	}
	html_dict = {}
	start_value = 97
	for char in char_list:
		if char == '0':
			start_value = 48
		html_code = '&#' + str(start_value) + ';'
		html_dict[char] = html_code
		start_value += 1

	html_dict.update(special_chars)

	result = ""
	for char in text:
		try:
			result += html_dict[char]
		except KeyError:
			print('The character %s is not in list.' % char)

	return result

text = input('Input your string : ')
print(textToHTML(text))