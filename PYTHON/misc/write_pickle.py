import pickle

words = ["hello", "this is cool", "please stop", "oh no", "oh my god"]
words2 = ["Okay,", "this is very exciting", "please don't stop", "oh yes", "oh my devil"]
try:
	with open('man_data.pickle', 'wb') as man_file, open('other_data.pickle', 'wb') as other_file:
		pickle.dump(words, man_file)
		pickle.dump(words2, other_file)
except IOError as err:
	print("IOError : ", str(err))
except pickle.PickleError as perr:
	print("PickleError : ", str(perr))