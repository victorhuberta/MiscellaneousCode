from random import shuffle

class PlayingCard:
	def __init__(self, card_type, value):
		self.card_type = card_type
		if value > 10:
			self.value = 10
		else:
			self.value = value
		if value == 11:
			self.name = "JACK"
		elif value == 12:
			self.name = "QUEEN"
		elif value == 13:
			self.name = "KING"
		elif value == 1:
			self.name = "ACE"
		else:
			self.name = str(self.value)

	def getType(self):
		return self.card_type

	def getValue(self):
		return self.value

	def __add__(self, card):
		total = self.value + card.value
		return total

	def transforms(self):
		if self.name == "ACE":
			self.value = 11

	def __repr__(self):
		return "%s OF %s" % (self.name, self.card_type)

	def __str__(self):
		return self.__repr__()

class BlackJack:
	def __init__(self, players=[]):
		self.players = players
		self.turn = 0
		self.deck = []
		self.winner = None
		ctype_list = ["DIAMONDS", "CLUBS", "HEARTS", "SPADES"]

		for ctype in ctype_list:
			for value in range(1,14):
				self.deck.append(PlayingCard(ctype, value))

		shuffle(self.deck)
		shuffle(self.players)

	def starts(self):
		for player in self.players:
			player.draws(self.deck, 2)

		while (self.winner == None):
			self.players[self.turn].thinks(self.deck)
			playing_num = 0
			for player in self.players:
				if player.status == "PLAYING":
					playing_num += 1
			if playing_num == 0:
				self.stops()
			if (self.turn == (len(self.players) - 1)):
				self.turn = 0
			else:
				self.turn += 1

	def stops(self):
		raw_cards_value = {player.getCardsValue() : player.name for player in self.players}
		cards_value = {raw_card_value : raw_cards_value[raw_card_value] for raw_card_value in raw_cards_value if raw_card_value <= 21}
		max_value = max(cards_value.keys())
		self.winner = cards_value[max_value]

	def getResult(self):
		return "The WINNER of the Black Jack Game is ... " + self.winner

	def getDeck(self):
		return deck
	def getPlayers(self):
		return players
	def getTurn(self):
		return turn


class Player:
	def __init__(self, name, status="PLAYING"):
		self.name = name
		self.status = status
		self.cards = []

	def draws(self, deck, amount=1):
		for x in range(amount):
			self.cards.append(deck.pop())
		print(self.name, "drew", amount, "card(s).")

	def stops(self):
		self.status = "STOPPED"

	def loses(self):
		self.status = "LOST"

	def thinks(self, deck):
		cards_value = self.getCardsValue()
		if  cards_value <= 16:
			self.draws(deck, 1)
		elif 17 <= cards_value <= 21:
			self.stops()
		else:
			self.loses()

	def getCardsValue(self):
		cards_value = 0
		for card in self.cards:
			cards_value += card.getValue()
		return cards_value

player_names = ["Victor", "Kevin", "Miko", "Janice"]
players = []
for player_name in player_names:
	players.append(Player(player_name))

game = BlackJack(players)
game.starts()
print(game.getResult())


