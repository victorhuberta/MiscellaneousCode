import sys;

def isPrime(n):
  if (n > 1):
    for x in range(2, n):
      if (n % x == 0):
        return False;
    return True;
  else:
    return False;

def findTripletPrimes(limit):
  if (limit < 2):
  	return;
  for i in range(2, limit):
      if (isPrime(i)):
      	first = i;
      	second = i + 2;
      	third = i + 4;
        if (isPrime(second) and isPrime(third)):
          threePrimes = (str(first), str(second), str(third));
          print(', '.join(threePrimes));

def main():
  findTripletPrimes(int(sys.argv[1]));

if (__name__ == '__main__'):
  main();
