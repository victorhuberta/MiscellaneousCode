"""
Authored by : Victor Huberta.
A program to draw Vertex graphs using Prim's
and Kruskal's algorithms based on the specification
in 'graph.txt' and save them in '.png' image file.
"""

import networkx as nx
import matplotlib.pyplot as plt

def drawGraph(g, filename):
	#pos = nx.spring_layout(g)
	pos = nx.shell_layout(g)
	edge_labels = nx.get_edge_attributes(g, 'weight')
	nx.draw_networkx_nodes(g, pos, node_size=6000)
	nx.draw_networkx_edges(g, pos, width=2)
	nx.draw_networkx_labels(g, pos, font_size=10, font_family='sans-serif')
	nx.draw_networkx_edge_labels(g, pos, edge_labels=edge_labels, label_pos=0.58)
	#nx.draw(g, pos, edge_labels=edge_labels, node_size=6000, width=2, font_size=10, font_family='sans-serif')
	plt.savefig("%s.png" % (filename))
	plt.show()

def createInitialGraph(nodes, edges, weights):
	# Draw vertex graph
	if nodes and edges and weights:
		g = nx.Graph()
		edges_weights = {}

		# Add nodes to graph
		for node in nodes:
			g.add_node(node)

		# Add edges to graph
		w = 0
		for edge in edges:
			# Special argument 'CONNECTALL'
			# causes the program to connect all the nodes with edges
			if 'CONNECTALL' in edge:
				for x in xrange(len(nodes)):
						for i in xrange(x, len(nodes)):
							if nodes[i] != nodes[x]:
								g.add_edge(nodes[x], nodes[i], weight=weights[w])
								edges_weights[(nodes[x], nodes[i])] = int(weights[w])
								w += 1
				break
			# Add edges to graph according to specification
			else:
				edge_data = edge.split(',')
				from_, to_ = edge_data[0], edge_data[1]
				g.add_edge(from_, to_, weight=weights[w])
				edges_weights[(from_, to_)] = int(weights[w])
				w += 1

		drawGraph(g, "initial_graph")
		return edges_weights

def createPrimMST(nodes, edges_weights):
	g = nx.Graph()

	# Add nodes to graph
	for node in nodes:
		g.add_node(node)

	sorted_edges = sorted(edges_weights, key=edges_weights.get)
	
	visited_nodes = []
	visited_nodes.append(nodes[0])
	graph_count = 0

	while len(visited_nodes) < len(nodes):
		for visited_node in visited_nodes:
			for edge in sorted_edges:
				from_ = None
				to_ = None

				if (edge[0] == visited_node) and (edge[1] not in visited_nodes):
					from_, to_ = edge[0], edge[1]
				elif (edge[1] == visited_node) and (edge[0] not in visited_nodes):
					from_, to_ = edge[1], edge[0]

				if from_ and to_:
					g.add_edge(from_, to_, weight=edges_weights[edge])
					graph_count += 1
					drawGraph(g, "prim_mst%d" % (graph_count))
					visited_nodes.append(to_)
					break

			break

def addToPaths(paths, from_, to_):
	updated = False
	for path in paths:
		if from_ in path or to_ in path:
			path.update([from_, to_])
			updated = True
	if not updated: paths.append(set([from_, to_]))

	try:
		for i in xrange(len(paths)):
			if paths[i + 1]:
				if paths[i].intersection(paths[i + 1]):
					paths[i].update(paths[i + 1])
					paths.remove(paths[i + 1])
	except IndexError, ie:
		pass

def isAllConnected(paths):
	for path in paths:
		if len(path) >= len(nodes):
			return True
	return False

def isCircuit(paths, from_, to_):
	for path in paths:
		if from_ in path and to_ in path:
			return True
	return False


def createKruskalMST(nodes, edges_weights):
	g = nx.Graph()

	# Add nodes to graph
	for node in nodes:
		g.add_node(node)

	sorted_edges = sorted(edges_weights, key=edges_weights.get)
	paths = []
	visited_nodes = set()
	graph_count = 0

	for edge in sorted_edges:
		from_, to_ = edge[0], edge[1]
		circuit = isCircuit(paths, from_, to_)
		if not circuit:
			addToPaths(paths, from_, to_)
			visited_nodes.update([from_, to_])
			g.add_edge(from_, to_, weight=edges_weights[edge])
			graph_count += 1
			drawGraph(g, "kruskal_mst%d" % (graph_count))

		if isAllConnected(paths):
			break

def cleanList(items):
	for item in items:
		if '' in item: item.remove('')
		if '\n' in item: item.remove('\n')

try:
	graph_data = open("graph.txt", "r")

	# Extract nodes, edges, and weights from file
	for line in graph_data.readlines():
		if line.startswith('NODES'):
			nodes = line[6:].split(',')
		elif line.startswith('EDGES'):
			edges = line[6:].split(';')
		elif line.startswith('WEIGHTS'):
			weights = line[8:].split(';')

except IOError, ioe:
	print("IOError : %s" % (ioe))
	exit()

cleanList([nodes, edges, weights])

edges_weights = createInitialGraph(nodes, edges, weights)
createPrimMST(nodes, edges_weights)
createKruskalMST(nodes, edges_weights)
