import matplotlib.pyplot as plt

plt.figure()

x_series = [item for item in range(50000, 460000, 10000)]

qdata_file = open("quickSort_records.txt", "r")

y_series = [qdata for qdata in qdata_file.read().split('\n') if qdata is not '']

plt.plot(x_series, y_series)

plt.xlabel("List Maximum Size")
plt.ylabel("Sorting Time")
plt.title("Quick Sort Graph")

plt.savefig("quickSort_graph.png")