import matplotlib.pyplot as plt

plt.figure()

x_series = [item for item in range(50000, 460000, 10000)]

sdata_file = open("selectionSort_records.txt", "r")

y_series = [sdata for sdata in sdata_file.read().split('\n') if sdata is not '']

plt.plot(x_series, y_series)

plt.xlabel("List Maximum Size")
plt.ylabel("Sorting Time")
plt.title("Selection Sort Graph")

plt.savefig("selectionSort_graph.png")