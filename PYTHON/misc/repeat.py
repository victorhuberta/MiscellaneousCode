#!/usr/bin/env python
import sys

def repeat(s, exclaim):
	result = s * 3;
	if exclaim:
		result = result + '!!!'
	return result

def main():
	name = sys.argv[1]
	if name == 'Victor':
		print repeat(name, False)
	print repeat('Cool ', True)

if __name__ == '__main__':
	main()


