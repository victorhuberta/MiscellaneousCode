import pickle

try:
	with open("man_data.pickle", "rb") as man_file, open("other_data.pickle", "rb") as other_file:
		words = pickle.load(man_file)
		words2 = pickle.load(other_file)
		print(words)
		print(words2)
except IOError as ioerr:
	print("IOError : ", str(ioerr))
except pickle.PickleError as perr:
	print("PickleError : ", str(perr))