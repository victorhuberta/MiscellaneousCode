import sys
import datetime
import time

time_tuple = ( 2012, # Year
                  9, # Month
                  6, # Day
                  0, # Hour
                 38, # Minute
                  0, # Second
                  0, # Millisecond
              )

def _win_set_time(time_tuple):
  import win32api
  # http://timgolden.me.uk/pywin32-docs/win32api__SetSystemTime_meth.html
  # pywin32.SetSystemTime(year, month , dayOfWeek , day , hour , minute , second , millseconds )
  dayOfWeek = datetime.datetime(*time_tuple).isocalendar()[2]
  win32api.SetSystemTime( *(time_tuple[:2] + (dayOfWeek,) + time_tuple[2:]))

if __name__ == '__main__':
  if sys.platform == 'win32':
    while True:
      _win_set_time(time_tuple)
      time.sleep(300)