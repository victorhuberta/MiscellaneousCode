#!/usr/bin/env python
import sys

def main():
	raw = r'this\t\n and that'
	print raw
	multi = "It was the best of times.\n\
It was the worst of times."
	print multi
	pi = 314
	x = 45
	y = x // pi
	print y

if __name__ == '__main__':
	main()
