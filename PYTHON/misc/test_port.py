import socket
import sys

def test_port(target_host, target_port):
	client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	client.connect((target_host, target_port))
	client.send("cat /etc/passwd".encode('ascii'))
	response = b''
	while True:
		more = client.recv(4096)
		response += more
		if len(more) < 4096:
			break
	print(response.decode('utf-8'))
	client.close()

if __name__ == '__main__':
	test_port(sys.argv[1], int(sys.argv[2]))
