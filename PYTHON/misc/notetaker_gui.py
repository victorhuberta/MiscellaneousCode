#!/usr/bin/env python
from Tkinter import *
import pickle

def generate_gui():
	root = Tk()
	root.title("Note Taker")
	root.geometry("600x400")

	def add_text():
		content = textbox.get()
		listbox.insert(END, content)
		textbox.delete(0,END)
	def return_insert(event):
		add_text()
	def rightClick_delete(event):
		listbox.delete(ANCHOR)
	def leftClick_edit(event):
		textbox.delete(0, END)
		content = listbox.get(ANCHOR)
		listbox.delete(ANCHOR)
		textbox.insert(0, content)
	def save():
		f = file("content.db", "wb")
		content = listbox.get(0, END)
		pickle.dump(content, f)
		f.close()
	def load():
		f = file("content.db", "rb")
		content = pickle.load(f)
		for item in content:
			listbox.insert(END, item)
		f.close()

	textframe = Frame(root)
	listframe = Frame(root)
	textbox = Entry(textframe)
	add_button = Button(textframe, text="ADD", command=add_text)
	save_button = Button(textframe, text="SAVE", command=save)
	scrollbar = Scrollbar(listframe, orient=VERTICAL)
	listbox = Listbox(listframe, yscrollcommand=scrollbar.set)
	scrollbar.configure(command = listbox.yview)

	textbox.bind("<Return>", return_insert)
	listbox.bind("<Double-Button-3>", rightClick_delete)
	listbox.bind("<Button-1>", leftClick_edit)

	textbox.pack(side=LEFT, fill=X, expand=1)
	add_button.pack(side=LEFT)
	save_button.pack(side=LEFT)
	listbox.pack(side=LEFT, fill=BOTH, expand=1)
	scrollbar.pack(side=RIGHT, fill=Y)
	textframe.pack(fill=X)
	listframe.pack(fill=BOTH, expand=1)
	try:
		load()
	except:
		pass
	root.mainloop()

if __name__ == "__main__":
	generate_gui()
