class Mother:
	def __init__(self, name, personality):
		self.name = name
		self.personality = personality

	def getPersonality(self):
		if self.seeCircumstances() == "NOT GOOD":
			return "Psychotic personality"
		else:
			return self.personality

class Child(Mother):
	def __init__(self, name, personality):
		Mother.__init__(self, name, personality)

	def seeCircumstances(self):
		return "NOT GOOD"

mother = Mother("Maria", "Kind-hearted personality")
child = Child("Jackson", "Good Looking personality")
print(child.getPersonality())
