import sys;

def isPerfect(n):
  divisibles = [];
  for i in range(1, n):
    if (n % i == 0):
      divisibles.append(i);
  sum = 0;
  for divisible in divisibles:
    sum += divisible;
  if (sum == n):
    return True;
  else:
    return False;

def main():
  if (isPerfect(int(sys.argv[1]))):
    print("Yes");
  else:
    print("No");

if (__name__ == '__main__'):
  main();
