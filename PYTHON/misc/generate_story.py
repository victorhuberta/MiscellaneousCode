html = open("main.html", "w")
try:
	basic = open("basic.html", "r")
	for line in basic:
		if line.find("<start></start>") != -1:
			story = open("story.txt", "r")
			for sentence in story:
				if sentence.find("\n") != -1:
					sentence += "<br/>"
				print(sentence, file=html)
		print(line, file=html)
except IOError:
	print("Can't find file.")
story.close()
basic.close()
html.close()