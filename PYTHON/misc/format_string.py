def format_string(string, formatter=None):
	class DefaultFormatter:
		def format(self, string):
			return str(string).title()

	if not formatter:
		formatter = DefaultFormatter()

	return formatter.format(string)

s1 = "hello, world. how are you today?"

print(s1)
print(format_string(s1))