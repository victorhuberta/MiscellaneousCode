import sys

def full_iterate(items, indent=False, level=0, output=sys.stdout):
	for item in items:
		if isinstance(item, list):
			full_iterate(item, indent, level+1, output)
		else:
			if indent:
				print("\t" * level, end='', file=output)
			print(item, file=output)
