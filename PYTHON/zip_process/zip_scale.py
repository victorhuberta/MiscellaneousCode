import os
import sys
from zip_processor import ZipProcessor
from pygame import image
from pygame.transform import scale

class ZipScale(ZipProcessor):
	def __init__(self, zip_filename, width=640, height=480):
		ZipProcessor.__init__(self, zip_filename)
		self.size = (int(width), int(height))

	def process_files(self):
		for filename in os.listdir(self.temp_dir):
			img = image.load(self._full_filename(filename))
			scaled = scale(img, self.size)
			image.save(scaled, self._full_filename(filename))

if __name__ == '__main__':
	ZipScale(*sys.argv[1:4]).process_zip()
