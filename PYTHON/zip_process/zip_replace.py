import os
import sys
from zip_processor import ZipProcessor

class ZipReplace(ZipProcessor):
	def __init__(self, zip_filename, search_str, replace_str):
		super().__init__(zip_filename)
		self.search_str = search_str
		self.replace_str = replace_str

	def process_files(self):
		for filename in os.listdir(self.temp_dir):
			with open(self._full_filename(filename), "r") as _file:
				content = _file.read()
			content = content.replace(self.search_str, self.replace_str)
			with open(self._full_filename(filename), "w") as _file:
				_file.write(content)

if __name__ == '__main__':
	ZipReplace(*sys.argv[1:4]).process_zip()