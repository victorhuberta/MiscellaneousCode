import os
import zipfile
import sys
import shutil

class ZipProcessor:
	def __init__(self, zip_filename):
		self.zip_filename = zip_filename
		self.temp_dir = "unzipped-{}/".format(zip_filename[:-4])

	def _full_filename(self, filename):
		return os.path.join(self.temp_dir + filename)

	def process_zip(self):
		self.unzip_file()
		self.process_files()
		self.zip_files()

	def unzip_file(self):
		os.mkdir(self.temp_dir)
		zip_file = zipfile.ZipFile(self.zip_filename)
		try:
			zip_file.extractall(self.temp_dir)
		finally:
			zip_file.close()

	def process_files(self):
		'Overriden method to process files extracted in temp_dir'
		pass

	def zip_files(self):
		zip_file = zipfile.ZipFile(self.zip_filename, "w")
		try:
			for filename in os.listdir(self.temp_dir):
				zip_file.write(self._full_filename(filename), filename)
		finally:
			zip_file.close()
		shutil.rmtree(self.temp_dir)


