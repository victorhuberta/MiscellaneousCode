import hashlib

class User:
	def __init__(self, username, password):
		self.username = username
		self.enc_password = self._encrypt_pw(password)
		self.is_logged_in = False

	def _encrypt_pw(self, password):
		hash_string = self.username + password
		hash_string = hash_string.encode("utf8")
		return hashlib.sha256(hash_string).hexdigest()

	def check_password(self, password):
		return self.enc_password == self._encrypt_pw(password)

class Authenticator:
	def __init__(self):
		self.users = {}

	def add_user(self, username, password):
		if username in self.users:
			raise UsernameAlreadyExists(username)
		if len(password) < 6:
			raise PasswordTooShort(username)

		self.users[username] = User(username, password)

	def log_in(self, username, password):
		try:
			user = self.users[username]
		except KeyError as ke:
			raise InvalidUsername(username)
		else:
			if not user.check_password(password):
				raise InvalidPassword(username, user)

		user.is_logged_in = True

	def is_logged_in(self, username):
		if username in self.users:
			return self.users[username].is_logged_in
		return False

class Authorizor:
	def __init__(self, authenticator):
		self.authenticator = authenticator
		self.permissions = {}

	def add_permission(self, perm_name):
		try:
			perm_set = self.permissions[perm_name]
		except KeyError as ke:
			self.permissions[perm_name] = set()
		else:
			raise PermissionError("Permission Exists")

	def permit_user(self, perm_name, username):
		try:
			perm_set = self.permissions[perm_name]
		except KeyError as ke:
			raise PermissionError("Permission Doesn't Exist")
		else:
			if username not in self.authenticator.users:
				raise InvalidUsername(username)

			perm_set.add(username)

	def check_permission(self, perm_name, username):
		if not self.authenticator.is_logged_in(username):
			raise NotLoggedInError(username)

		try:
			perm_set = self.permissions[perm_name]
		except KeyError as ke:
			raise PermissionError("Permission Doesn't Exist")
		else:
			if username not in perm_set:
				raise NotPermittedError(username)
			else:
				return True

class AuthException(Exception):
	def __init__(self, username, user=None):
		super().__init__(username, user)
		self.username = username
		self.user = user

class UsernameAlreadyExists(AuthException):
	pass

class PasswordTooShort(AuthException):
	pass

class InvalidUsername(AuthException):
	pass

class InvalidPassword(AuthException):
	pass

class PermissionError(AuthException):
	pass

class NotLoggedInError(AuthException):
	pass

class NotPermittedError(AuthException):
	pass

authenticator = Authenticator()
authorizor = Authorizor(authenticator)

