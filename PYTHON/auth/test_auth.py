import auth

auth.authenticator.add_user("vehazet", "vehazetpass")
auth.authorizor.add_permission("test program")
auth.authorizor.add_permission("change program")
auth.authorizor.permit_user("test program", "vehazet")

class Editor:
	def __init__(self):
		self.username = None
		self.menu_map = {
			"login" : self.login,
			"test" : self.test,
			"change" : self.change,
			"quit" : self.quit
		}

	def login(self):
		logged_in = False
		while not logged_in:
			try:
				username = input("Enter username : ")
				password = input("Enter password : ")
				auth.authenticator.log_in(username, password)
			except (auth.InvalidUsername, auth.InvalidPassword):
				print("Invalid Username or Password.")
			else:
				self.username = username
				logged_in = True

	def is_permitted(self, perm_name):
		try:
			permitted = auth.authorizor.check_permission(perm_name, self.username)
		except auth.NotLoggedInError as nle:
			print("Please log in first.")
		except auth.NotPermittedError as npe:
			print("%s is not permitted to perform the action." % npe.username)
		else:
			return permitted

	def test(self):
		if self.is_permitted("test program"):
			print("Testing the program now...")

	def change(self):
		if self.is_permitted("change program"):
			print("Changing the program now...")

	def quit(self):
		raise SystemExit()

	def menu(self):
		try:
			while True:
				print("""
Please enter a command:

\tlogin\t- Login
\ttest\t- Test the application
\tchange\t- Change the application
\tquit\t- Quit

""")
				answer = input("--> ").lower()
				try:
					func = self.menu_map[answer]
				except KeyError as ke:
					print("Invalid option : %s" % answer)
				else:
					func()
		finally:
			print("Thank you for testing the auth module :)")

Editor().menu()
