import datetime

last_id = 0

class Notebook:
	def __init__(self, title):
		self.title = title
		self.notes = []

	def search(self, filter_str):
		found_notes = []
		for note in self.notes:
			if note.match(filter_str): 
				found_notes.append(note)
		return found_notes

	def new_note(self, content="", tags=""):
		note = Note(content, tags)
		self.notes.append(note)

	def _find_by_id(self, note_id):
		for note in self.notes:
			if str(note.id) == str(note_id): return note

	def modify_content(self, note_id, new_content):
		note = self._find_by_id(note_id)
		if note:
			note.content = new_content

	def modify_tags(self, note_id, new_tags):
		note = self._find_by_id(note_id)
		if note:
			note.tags = new_tags

class Note:
	def __init__(self, content, tags):
		global last_id
		last_id += 1
		self.id = last_id
		self.content = content
		self.tags = tags
		self.creation_date = datetime.date.today()

	def match(self, filter_str):
		if filter_str in self.content or \
			filter_str in self.tags:
			return True
		else:
			return False
