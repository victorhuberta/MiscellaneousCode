import sys
from notebook import Note, Notebook

class Menu:
	def __init__(self):
		self.notebook = Notebook("Victor's")
		self.choices = {
			"1": self.show_notes,
			"2": self.search_notes,
			"3": self.add_note,
			"4": self.modify_note,
			"5": self.quit,
		}

		self.menu = \
"""
Notebook Menu

1. Display all notes
2. Search for notes
3. Add a new note
4. Edit a note
5. Quit
"""

	def run(self):
		while True:
			print(self.menu)
			choice = input("Enter your choice : ")
			action = self.choices.get(choice)
			if action:
				action()
			else:
				print("%s is not a valid choice. Please try again." % choice)

	def show_notes(self, notes=None):
		if not notes:
			notes = self.notebook.notes

		for note in notes:
			print("\nID : %d\n%s\nTags : %s\nCreated on %s" % (
					note.id, note.content,
					note.tags, note.creation_date
				)
			)

	def search_notes(self):
		filter_str = input("Enter filter : ")
		found_notes = self.notebook.search(filter_str)
		if found_notes:
			print("Notes found!")
			self.show_notes(found_notes)
		else:
			print("No notes found!")

	def add_note(self):
		content = input("Enter content of note : ")
		tags = input("Enter tags for note (separated by comma) : ")
		self.notebook.new_note(content, tags)
		print("Note sucessfully saved!")

	def modify_note(self):
		note_id = input("Enter the note id : ")
		note = self.notebook._find_by_id(note_id)
		if note:
			new_content = input("Enter the new content : ")
			new_tags = input("Enter the new tags : ")
			if new_content:
				self.notebook.modify_content(note_id, new_content)
			if new_tags:
				self.notebook.modify_tags(note_id, new_tags)
		else:
			print("Note not found. Please try again!")

	def quit(self):
		print("Thank you for using the notebook. See you soon :)")
		sys.exit()

if __name__ == '__main__':
	Menu().run()
