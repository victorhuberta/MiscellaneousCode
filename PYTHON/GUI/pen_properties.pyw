import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class EditorForm(QDialog):
	width = 0
	bevelled = True
	style = "DashDotted"
	def __init__(self, parent=None):
		super(EditorForm, self).__init__(parent)

	def setPenProperties(self):
		dialog = PenPropertiesDlg(self)
		dialog.widthSpinBox.setValue(self.width)
		dialog.bevelledCheckBox.setChecked(self.bevelled)
		dialog.styleComboBox.setCurrentIndex(dialog.styleComboBox.findText(self.style))

		if dialog.exec_():
			self.width = dialog.widthSpinBox.value()
			self.bevelled = dialog.bevelledCheckBox.isChecked()
			self.style = unicode(dialog.styleComboBox.currentText())
			self.update()

	def update(self):
		pass


class PenPropertiesDlg(QDialog):
	def __init__(self, parent=None):
		super(PenPropertiesDlg, self).__init__(parent)

		widthLabel = QLabel("&Width")
		styleLabel = QLabel("&Style")
		self.widthSpinBox = QSpinBox()
		self.widthSpinBox.setAlignment(Qt.AlignRight)
		self.widthSpinBox.setRange(0, 24)
		widthLabel.setBuddy(self.widthSpinBox)
		self.bevelledCheckBox = QCheckBox("&Bevelled edges?")
		self.styleComboBox = QComboBox()
		self.styleComboBox.addItems(['Solid', 'Dashed', 'Dotted', 'DashDotted', 'DashDotDotted'])
		styleLabel.setBuddy(self.styleComboBox)
		self.okButton = QPushButton("&OK")
		self.cancelButton = QPushButton("Cancel")

		buttonLayout = QHBoxLayout()
		buttonLayout.addStretch()
		buttonLayout.addWidget(self.okButton)
		buttonLayout.addWidget(self.cancelButton)

		layout = QGridLayout()
		layout.addWidget(widthLabel, 0, 0)
		layout.addWidget(self.widthSpinBox, 0, 1)
		layout.addWidget(self.bevelledCheckBox, 0, 2)
		layout.addWidget(styleLabel, 1, 0)
		layout.addWidget(self.styleComboBox, 1, 1, 1, 2)
		layout.addLayout(buttonLayout, 2, 0, 1, 3)

		self.setLayout(layout)

		self.connect(self.okButton, SIGNAL("clicked()"), self, SLOT("accept()"))
		self.connect(self.cancelButton, SIGNAL("clicked()"), self, SLOT("reject()"))

		self.setWindowTitle("Pen Properties")

app = QApplication(sys.argv)
editorForm = EditorForm()
editorForm.show()
editorForm.setPenProperties()
app.exec_()
