import sys
import urllib2
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class Form(QDialog):
	def __init__(self, parent=None):	
		super(Form, self).__init__(parent)
		date = self.getData()
		rates = sorted(self.rates.keys())
		dateLabel = QLabel(date)
		self.fromComboBox = QComboBox()
		self.fromComboBox.addItems(rates)
		self.fromDoubleSpinBox = QDoubleSpinBox()
		self.fromDoubleSpinBox.setRange(0.01, 10000000.00)
		self.fromDoubleSpinBox.setValue(1.00)
		self.toComboBox = QComboBox()
		self.toComboBox.addItems(rates)
		self.toLabel = QLabel("1.00")

		grid = QGridLayout()
		grid.addWidget(dateLabel, 0, 0)
		grid.addWidget(self.fromComboBox, 1, 0)
		grid.addWidget(self.fromDoubleSpinBox, 1, 1)
		grid.addWidget(self.toComboBox, 2, 0)
		grid.addWidget(self.toLabel, 2, 1)

		self.setLayout(grid)

		self.connect(self.fromComboBox, SIGNAL("currentIndexChanged(int)"), self.updateUi)
		self.connect(self.fromDoubleSpinBox, SIGNAL("valueChanged(double)"), self.updateUi)
		self.connect(self.toComboBox, SIGNAL("currentIndexChanged(int)"), self.updateUi)
		self.setWindowTitle("Currency Conversion Tool")

	def updateUi(self):
		to = unicode(self.toComboBox.currentText())
		from_ = unicode(self.fromComboBox.currentText())
		result = (self.rates[from_] / self.rates[to]) * self.fromDoubleSpinBox.value()
		self.toLabel.setText("%0.2f" % (result))

	def getData(self):
		self.rates = {}
		try:
			f = urllib2.urlopen("http://www.bankofcanada.ca/en/markets/csv/exchange_eng.csv")
			date = "Unknown"
			for line in f:
				if line is None or line.startswith("#") or line.startswith("Closing"):
					continue
				fields = line.split(", ")
				if line.startswith("Date"):
					date = fields[-1]
				else:
					try:
						self.rates[unicode(fields[0])] = float(fields[-1])
					except ValueError, verr:
						pass
			return "Exchange Rates Date: " + date
		except Exception, e:
			return  "Failed to download : %s" % (e)

app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()
