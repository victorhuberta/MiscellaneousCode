import kivy
kivy.require("1.9.1")

from kivy.lang import Builder
from kivy.app import App
from kivy.uix.screenmanager import Screen, ScreenManager, FadeTransition
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.widget import Widget
from kivy.graphics import Line
from kivy.uix.label import Label

Builder.load_file("kvs/global.kv")

navigator = ScreenManager(transition=FadeTransition())

Builder.load_file("kvs/register_screen.kv")
class RegisterScreen(Screen, FloatLayout):
    pass


Builder.load_file("kvs/login_screen.kv")
class LoginScreen(Screen, FloatLayout):

    def login(self, username, password):
        if username == "victor" and password == "huberta":
            navigator.current = "paint_screen"
        else:
            pass
            # TODO: change the err_msg text to "Wrong password!"


Builder.load_file("kvs/paint_screen.kv")
class PaintScreen(Screen, FloatLayout):
    pass


class Painter(Widget):

    def on_touch_down(self, touch):
        super().on_touch_down(touch)
        with self.canvas:
            touch.ud["line"] = Line(points=(touch.x, touch.y))

    def on_touch_move(self, touch):
        super().on_touch_move(touch)
        touch.ud["line"].points += (touch.x, touch.y)


navigator.add_widget(LoginScreen(name="login_screen"))
navigator.add_widget(RegisterScreen(name="register_screen"))
navigator.add_widget(PaintScreen(name="paint_screen"))

class FirstApp(App):

    def build(self):
        return navigator


if __name__ == "__main__":
    FirstApp().run()
