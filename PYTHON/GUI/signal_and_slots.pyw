import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class Form(QDialog):
	def __init__(self, parent=None):
		super(Form, self).__init__(parent)

		dial = QDial()
		dial.setNotchesVisible(True)
		spinbox = ZeroQSpinBox()

		layout = QHBoxLayout()
		layout.addWidget(dial)
		layout.addWidget(spinbox)

		self.setLayout(layout)
		self.connect(dial, SIGNAL("valueChanged(int)"), spinbox, SLOT("setValue(int)"))
		self.connect(spinbox, SIGNAL("valueChanged(int)"), dial, SLOT("setValue(int)"))
		self.connect(spinbox, SIGNAL("atzero"), self.announce)
		self.setWindowTitle("Signals and Slots")

	def announce(self, zeros):
		print("ZeroQSpinBox has been at zero for %d times" % (zeros))

class ZeroQSpinBox(QSpinBox):
	zeros = 0

	def __init__(self, parent=None):
		super(ZeroQSpinBox, self).__init__(parent)
		self.connect(self, SIGNAL("valueChanged(int)"), self.checkZero)

	def checkZero(self):
		if self.value() is 0:
			self.zeros += 1
			self.emit(SIGNAL("atzero"), self.zeros)


app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()
