from PyQt4.QtCore import *
from PyQt4.QtGui import *

class StdNumberFormatDlg(QDialog):

	def __init__(self, format, parent=None):
		super(StdNumberFormatDlg, self).__init__(parent)
		thousandsLabel = QLabel("&Thousands separator")
		decimalMarkerLabel = QLabel("Decimal &marker")
		decimalPlacesLabel = QLabel("&Decimal places")
		self.thousandsEdit = QLineEdit(format['thousandsseparator'])
		thousandsLabel.setBuddy(self.thousandsEdit)
		self.decimalMarkerEdit = QLineEdit(format['decimalmarker'])
		decimalMarkerLabel.setBuddy(self.decimalMarkerEdit)
		self.decimalPlacesSBox = QSpinBox()
		self.decimalPlacesSBox.setRange(0,6)
		self.decimalPlacesSBox.setValue(format['decimalplaces'])
		decimalPlacesLabel.setBuddy(self.decimalPlacesSBox)
		self.redNegativeCheckBox = QCheckBox("&Red negative numbers")
		self.redNegativeCheckBox.setChecked(format['rednegativenumbers'])

		buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

		self.format = format.copy()

		layout = QGridLayout()
		layout.addWidget(thousandsLabel, 0, 0)
		layout.addWidget(self.thousandsEdit, 0, 1)
		layout.addWidget(decimalMarkerLabel, 1, 0)
		layout.addWidget(self.decimalMarkerEdit, 1, 1)
		layout.addWidget(decimalPlacesLabel, 2, 0)
		layout.addWidget(self.decimalPlacesSBox, 2, 1)
		layout.addWidget(self.redNegativeCheckBox, 3, 0, 1, 2)
		layout.addWidget(buttonBox, 4, 0, 1, 2)

		self.setLayout(layout)

		self.connect(buttonBox, SIGNAL("accepted()"), self, SLOT("accept()"))
		self.connect(buttonBox, SIGNAL("rejected()"), self, SLOT("reject()"))

		self.setWindowTitle("Set Number Format (Modal)")

	def accept(self):
		class ThousandsError(Exception): pass
		class DecimalError(Exception): pass

		thousands = unicode(self.thousandsEdit.text())
		decimal = unicode(self.decimalMarkerEdit.text())
		punctuation = frozenset(",;:.")

		try:
			if len(thousands) > 1:
				raise ThousandsError("Thousands separator needs to be zero or one character.")
			elif len(decimal) != 1:
				raise DecimalError("Decimals marker needs to be one character.")
			elif thousands and thousands not in punctuation:
				raise ThousandsError("Thousands separator needs to be in a form of punctuation.")
			elif decimal not in punctuation:
				raise DecimalError("Decimal marker needs to be in a form of punctuation.")
			elif thousands == decimal:
				raise ThousandsError("Thousands separator needs to be different from decimal marker.")
		except ThousandsError, te:
			QMessageBox.warning(self, "Thousands Separator Error", unicode(te))
			self.thousandsEdit.selectAll()
			self.thousandsEdit.setFocus()
			return
		except DecimalError, de:
			QMessageBox.warning(self, "Decimal Marker Error", unicode(de))
			self.decimalMarkerEdit.selectAll()
			self.decimalMarkerEdit.setFocus()
			return

		self.format['thousandsseparator'] = thousands
		self.format['decimalmarker'] = decimal
		self.format['decimalplaces'] = self.decimalPlacesSBox.value()
		self.format['rednegativenumbers'] = self.redNegativeCheckBox.isChecked()
		QDialog.accept(self)

	def numberFormat(self):
		return self.format



