import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import setNumberFormatDlg

class NumbersForm(QDialog):
	format = {'thousandsseparator' : ',', 'decimalmarker' : '.', \
			 'decimalplaces' : 2, 'rednegativenumbers' : True}

	def __init__(self, parent=None):
		super(NumbersForm, self).__init__(parent)

		self.setNumberFormat()

	def setNumberFormat(self):
		dialog = setNumberFormatDlg.StdNumberFormatDlg(self.format, self)

		if dialog.exec_():
			self.format = dialog.numberFormat()
			self.updateTables()

	def updateTables(self):
		QMessageBox.warning(self, "Update tables", "Tables have been successfully updated with %s." % (self.format))

app = QApplication(sys.argv)
form = NumbersForm()
form.show()
app.exec_()