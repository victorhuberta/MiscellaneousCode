import sys
import time
from PyQt4.QtCore import *
from PyQt4.QtGui import *

app = QApplication(sys.argv)

try:
	if len(sys.argv) < 2:
		raise ValueError
	hours, mins = sys.argv[1].split(":")
	due = QTime(int(hours), int(mins))
	if not due.isValid():
		raise ValueError
	message = "Alert : "
	if len(sys.argv) > 2:
		message = " ".join(sys.argv[2:])
	label = QLabel("<font color=red size=72><b>%s</b></font>" % (message))
	#label.setWindowFlags(Qt.SplashScreen)
	while QTime.currentTime() < due:
		time.sleep(20)
	label.show()
	QTimer.singleShot(60000, app.quit)
	app.exec_()
except ValueError:
	print("Please provide a time and/or a message to the program.")
finally:
	app.quit()


