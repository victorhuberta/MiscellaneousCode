from __future__ import division
import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class Form(QDialog):
	def __init__(self, parent=None):
		super(Form, self).__init__(parent)
		principalLabel = QLabel("Principal: ")
		rateLabel = QLabel("Rate: ")
		yearsLabel = QLabel("Years: ")
		amountLabel = QLabel("Amount: ")
		self.principalDSBox = QDoubleSpinBox()
		self.principalDSBox.setRange(0.00, 1000000000.00)
#		self.principalDSBox.setValue(0.00)
		self.principalDSBox.setPrefix("$ ")
		self.rateDSBox = QDoubleSpinBox()
		self.rateDSBox.setRange(0.00, 100.00)
		self.rateDSBox.setValue(0.00)
		self.rateDSBox.setSuffix(" %")
		self.yearsCBox = QComboBox()
		years = ["%d years" % (item) for item in xrange(1, 13)]
		self.yearsCBox.addItems(years)
		self.resultLabel = QLabel("$ 0.00")

		layout = QGridLayout()
		layout.addWidget(principalLabel, 0, 0)
		layout.addWidget(rateLabel, 1, 0)
		layout.addWidget(yearsLabel, 2, 0)
		layout.addWidget(amountLabel, 3, 0)
		layout.addWidget(self.principalDSBox, 0, 1)
		layout.addWidget(self.rateDSBox, 1, 1)
		layout.addWidget(self.yearsCBox, 2, 1)
		layout.addWidget(self.resultLabel, 3, 1)

		self.setLayout(layout)
		self.principalDSBox.setFocus()
		self.connect(self.principalDSBox, SIGNAL("valueChanged(double)"), self.updateUi)
		self.connect(self.rateDSBox, SIGNAL("valueChanged(double)"), self.updateUi)
		self.connect(self.yearsCBox, SIGNAL("currentIndexChanged(int)"), self.updateUi)

		self.setWindowTitle("Interest")

	def updateUi(self):
		result = self.principalDSBox.value() * ((1 + (self.rateDSBox.value() / 100.0)) ** (self.yearsCBox.currentIndex() + 1))
		self.resultLabel.setText("$ %.2f" % result)

app = QApplication(sys.argv)
form = Form()
form.show()
app.exec_()