import android
import json
import time

from urllib import parse
from urllib.request import urlopen

hello_msg = "Welcome to Coach Kelly's Timing App"
list_title = "Here is your list of athletes: "
quit_msg = "Quitting Coach Kelly's App."
web_server = "http://192.168.1.133:8080"
get_names_cgi = "/cgi-bin/generate_names.py"
get_data_cgi = "/cgi-bin/generate_data.py"

def send_to_server(url, post_data=None):
	if post_data:
		page = urlopen(url, parse.urlencode(post_data).encode("ascii"))
	else:
		page = urlopen(url)
	return page.read().decode("utf-8")

app = android.Android()
def status_update(msg, how_long=2):
	app.makeToast(msg)
	time.sleep(how_long)

status_update(hello_msg)
athlete_names = sorted(json.loads(send_to_server(web_server + get_names_cgi)))
app.dialogCreateAlert(list_title)
app.dialogSetSingleChoiceItems(athlete_names)
app.dialogSetPositiveButtonText('Select')
app.dialogSetNegativeButtonText('Quit')
app.dialogShow()
response = app.dialogGetResponse().result
if response['which'] in ('positive'):
	selected_athlete = app.dialogGetSelectedItems().result[0]
	which_athlete = athlete_names[selected_athlete]
	athlete = simplejson.loads(send_to_server(web_server + get_data_cgi, {'which_athlete': which_athlete}))
	app.dialogCreateAlert(athlete)
	athlete_title = athlete['name'] + "'s top 3 times: "
	app.dialogCreateAlert(athlete_title)
	app.dialogSetItems(athlete['top3'])
	app.dialogSetPositiveButtonText('OK')
	app.dialogShow()

	response = app.dialogGetResponse().result
	
status_update(quit_msg)