import sys

class ArtificialConversation:

	def __init__(self, sub_filename, reset=False):
		if reset:
			self.cleanse_subtitle(sub_filename)
		self.subtitle = self.read_file(sub_filename)

	def read_file(self, sub_filename):
		with open(sub_filename, "r") as subfile:
			subtitle = subfile.readlines()
		return subtitle

	def write_file(self, sub_filename, subtitle):
		with open(sub_filename, "w") as subfile:
			for line in subtitle:
				subfile.write(line)

	def cleanse_subtitle(self, sub_filename):
		requirements = ["0", "1", "2", "3", "4", \
						"5", "6", "7", "8", "9"]

		subtitle = self.read_file(sub_filename)
		cleansed_subtitle = []
		prev_line = ""
		for next_line in subtitle:
			if len(next_line.strip()) != 0:
				if next_line.strip()[0] not in requirements:
					next_line = prev_line + next_line
					prev_line = ""

					if next_line.strip()[-1] == ',':
						prev_line = next_line
					
					next_line = self.filter_line(next_line, special=True)

					if next_line.strip()[0] != '<':
						cleansed_subtitle.append(next_line)

		self.write_file(sub_filename, cleansed_subtitle)


	def filter_line(self, line, special=False):
		if special:
			return line.lower().replace('<i>',
				'').replace('</i>','').replace('<b>',
				'').replace('</b>','').replace('<u>',
				'').replace('</u>','').replace('-','')

		return line.lower().replace(',','').replace(':',
				'').replace(';','').replace('?',
				'').replace('!','').replace('-', '')

	def search_and_compare(self, request):
		lines_dict = {} # {match_points : line_pos}
		match_points_list = []

		for line_pos in range(0, len(self.subtitle)):
			match_points = 0
			line = self.subtitle[line_pos]
			target_words = self.filter_line(line).split()
			source_words = self.filter_line(request).split()
			for source_word in source_words:
				for target_word in target_words:
					if source_word in target_word:
						match_points += 1
			if match_points > 0:
				match_points_list.append(match_points)
				lines_dict[match_points] = line_pos

		if len(match_points_list) == 0:
			return "I don't get what you mean.\n"

		match_points_list.sort()
		try:
			return self.subtitle[lines_dict[match_points_list[-1]] + 1]
		except IndexError:
			return "I don't get what you mean.\n"

	def prompt(self):
		request = "start"
		response = "Hello, I am Machiko\n"
		while request.lower() not in "bye bye":
			print(response, end="")
			request = input(">> ")
			response = self.search_and_compare(request)

if __name__ == "__main__":
	ArtificialConversation(*sys.argv[1:3]).prompt()