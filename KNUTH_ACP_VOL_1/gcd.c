#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int m, n, r;

    if (argc != 3) {
        fprintf(stderr, "Usage: %s m n\n", argv[0]);
        return 1;
    }

    m = atoi(argv[1]);
    n = atoi(argv[2]);

    if (m < n)
        r = m, m = n, n = r;

    while ((r = m % n) != 0)
        m = n, n = r;
    printf("GCD = %d\n", n);

    return 0;
}
