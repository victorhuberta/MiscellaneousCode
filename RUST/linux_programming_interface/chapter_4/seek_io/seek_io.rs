use std::env;
use std::process;
use std::fs::OpenOptions;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Error as IoError;
use std::io::ErrorKind;
use std::error::Error;

fn main() {
    if env::args().count() < 3 {
        println!("{}", "Usage: ./seek_io file {s<length>|r<length>|R<length>|w<text>}");
        process::exit(0);
    }

    let mut args = Vec::new();
    for arg in env::args() {
        args.push(arg);
    }

    let filename = &args[1];

    match OpenOptions::new().read(true).write(true).create(true).open(filename) {
        Err(e) => println!("{}", e.description()),
        Ok(mut f) => {
            for i in 2..args.len() {
                if let Err(e) = handle_options(&mut f, &args[i]) {
                    println!("{}", "Error here");
                    println!("{}", e.description());
                }
            }
        }
    }
}

fn handle_options(f: &mut File, option: &str) -> Result<(), IoError> {
    match &option[0..1] {
        "s" => seek_offset(f, option),
        "r" => read_text(f, option, true),
        "R" => read_text(f, option, false),
        "w" => write_text(f, option),
        _ => {
            println!("{}", "Arguments should start with {srRw}!");
            Ok(())
        }
    }
}

fn seek_offset(f: &mut File, arg: &str) -> Result<(), IoError> {
    let ofst = arg[1..arg.len()].parse::<u64>();

    match ofst {
        Ok(n) => {
            let new_ofst = try!(f.seek(SeekFrom::Start(n)));
            println!("{}: seek succeed. New offset: {}.", arg, new_ofst);
            Ok(())
        },
        Err(e) => Err(IoError::new(ErrorKind::Other, e.description()))
    }

}

fn read_text(f: &mut File, arg: &str, as_hex: bool) -> Result<(), IoError> {
    let bytes = (&arg[1..arg.len()]).parse::<u64>();

    match bytes {
        Ok(n) => {
            let mut buf: Vec<u8> = vec![0; n as usize];
            try!(f.read_exact(&mut buf));

            let output = String::from_utf8(buf).unwrap();
            print!("{}: ", arg);

            if as_hex {
                println!("{}", output);
            } else {
                for c in output.chars() {
                    print!("{:x} ", c as u32);
                }
                println!("{}", "");
            }

            Ok(())
        },
        Err(e) => Err(IoError::new(ErrorKind::Other, e.description()))
    }
}

fn write_text(f: &mut File, arg: &str) -> Result<(), IoError> {
    let text = (&arg[1..arg.len()]).to_string();
    let bytes = text.into_bytes();

    try!(f.write_all(&bytes));
    println!("{}: wrote {} bytes.", arg, bytes.len());
    Ok(())
}
