use std::io;
use std::io::Write;
use std::env;
use std::error::Error;
use std::fs::*;

fn main() {
    if env::args().count() > 1 {
        let is_append = env::args().any(|arg| arg == "-a");

        let filename = env::args().nth(1).unwrap();

        if is_append {
            OpenOptions::new().append(true)
                .create(true).open(&filename).unwrap();
        } else {
            OpenOptions::new().write(true)
                .create(true).truncate(true).open(&filename).unwrap();
        }
    }

    loop {
        let mut input = String::new();

        match io::stdin().read_line(&mut input) {
            Ok(_) => {
                print!("{}", input);
                if env::args().count() > 1 {
                    let filename = env::args().nth(1).unwrap();

                    let mut f = OpenOptions::new().append(true)
                        .create(true).open(&filename).unwrap();

                    if let Err(e) = f.write_all(&input.clone().into_bytes()) {
                        println!("{}", e.description());
                    }
                }
            }
            Err(e) => println!("error: {}", e.description())
        }
    }
}
