use std::process::exit;
use std::env;
use std::io;
use std::io::prelude::*;
use std::fs::File;

fn main() {
    if  env::args().count() != 3 {
        println!("Usage: ./copy oldfile newfile");
        exit(0);
    }

    let mut args = Vec::new();

    for arg in env::args() {
        args.push(arg);
    }

    let res = copy_old_into_new_file(&args[1], &args[2]);
    match res {
        Ok(_) => println!("File copied."),
        Err(e) => println!("{}", e)
    }
}

fn copy_old_into_new_file(old_filename: &String, new_filename: &String) -> Result<(), io::Error> {
    let mut old_file = try!(File::open(old_filename));
    let mut new_file = try!(File::create(new_filename));

    let mut buf = Vec::new();
    loop {
        let num_read = try!(old_file.read_to_end(&mut buf));
        if num_read > 0 {
            try!(new_file.write_all(&buf));
            break;
        } else {
            println!("Error in reading file.");
        }
    }
    Ok(())
}
