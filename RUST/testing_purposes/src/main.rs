//! # Rust Program For Testing Purposes
//! This program is only created for testing
//! purposes. None of it makes any sense and
//! it's okay.

extern crate rand;

use std::io;
use std::iter::Iterator;
use rand::Rng;

fn main() {
    simple_tests1();
    simple_tests2();
    simple_tests3();
}

struct Point {
    x: i32,
    y: i32
}

struct PointRef<'a> {
    x: &'a mut i32,
    y: &'a mut i32
}

struct YearsOld(i32);

struct Nothing;

enum Victor<'a> {
    Name { full: &'a str }
}

fn simple_tests3() {
    let mut p: Point = Point { x: 10, y: 20 };
    modify_point(&mut p.x, &mut p.y);
    println!("p.x = {}; p.y = {}", p.x, p.y);

    let mut p1: Point = Point { x: 20, .. p};
    modify_point(&mut p1.x, &mut p1.y);
    println!("p1.x = {}; p1.y = {}", p1.x, p1.y);

    let YearsOld(age) = YearsOld(19);
    println!("I am {} years old.", age);

    let you = Nothing;
    match you {
        Nothing => println!("You are NOTHING.")
    }

    let full_name = Victor::Name { full: "Victor Huberta" };
    match full_name {
        Victor::Name { full: "Victor Huberta" } => {
            println!("Victor Huberta is his name.");
        },
        _ => println!("I don't get the full name.")
    }
}

fn modify_point(x: &mut i32, y: &mut i32) {
    let p_ref: PointRef = PointRef { x: x, y: y };
    *p_ref.x += 99;
    *p_ref.y += 101;
}

struct Foo<'a> {
    x: &'a mut i32
}

impl<'a> Foo<'a> {
    fn x(&self) -> &i32 { self.x }
}

fn simple_tests2() {
    let i = &mut 10;
    let mut foo = Foo { x: i };
    {
        let i = play_with_lifetimes(&foo);
        println!("i = {}", i);
    }
    {
        let i = add_to_foo(&mut foo, 50);
        println!("i = {}", i);
    }
    println!("i = {}", foo.x());
}

/// This is to practice myself with lifetimes.
fn play_with_lifetimes<'a>(f: &'a Foo) -> &'a i32 {
    println!("Foo.x = {}", f.x);
    f.x
}

/// Add any integer to struct Foo.
fn add_to_foo<'a>(f: &'a mut Foo, n: i32) -> &'a i32 {
    *f.x += n;
    f.x
}

/// Do nonsense to practice Rust.
fn simple_tests1() {
    let numbers: [i32; 5] = init_numbers();
    print_favorites(numbers);
    print_slice(numbers);
    print_str();

    let t: (i32, i32, i32) = (10, 20, 30);
    let sum: i32 = sum_tuple(t);
    println!("Sum of (10, 20, 30) is {}", sum);

    println!("What is your name, traveller?");
    let mut name: String = String::new();
    io::stdin().read_line(&mut name)
        .expect("Couldn't read line.");

    let name: &str = name.trim();
    let is_owner: bool = check_owner(name);
    if is_owner {
        println!("Hi, master Victor. How are you today?");
    } else {
        println!("You are not him. Where is he!?");
    }
    print_all_lines("everything\nis\nawesome");
    print_odd_numbers(1, 21);

    let mut v = vec![1, 2, 3];
    push_rnd_to_vector(&mut v);
    for i in &v {
        println!("{}", i);
    }

    let mut n = 0;
    {
        let n_ref = &mut n;
        *n_ref += 1;
    }
    println!("n = {}", n);
}

/// Initialize an array of 32-bit numbers.
fn init_numbers() -> [i32; 5] {
    let mut numbers: [i32; 5] = [0; 5];
    numbers[0] = 99;
    numbers[2] = 20;
    numbers
}

/// Print my favorite numbers from an array of numbers.
fn print_favorites(numbers: [i32; 5]) {
    println!("My favorite two numbers are {} and {}",
        numbers[0], numbers[2]);
}

/// Print a slice of the array of numbers.
fn print_slice(numbers: [i32; 5]) {
    let numbers_slice: &[i32] = &numbers[0..3];
    println!("Here are the numbers in its slice...");
    for e in numbers_slice {
        println!("{}", e);
    }
}

/// Print a hello world with the type of &str.
fn print_str() {
     let text: &str = "Hello, world!";
     println!("{}", text);
}

/// Sum all integers in a tuple.
fn sum_tuple(t: (i32, i32, i32)) -> i32 {
    let (x, y, z) = t;
    x + y + z
}

/// Check if user is owner.
fn check_owner(name: &str) -> bool {
    if name == "Victor" { true } else { false }
}

/// Print all the lines from a given string.
fn print_all_lines(text: &str) {
    let lines: std::str::Lines = text.lines();
    for (line_no, line) in lines.enumerate() {
        println!("{}: {}", line_no, line);
    }
}

/// Print all odd numbers from a given range.
fn print_odd_numbers(min: i32, max: i32) {
    'odd_loop: for num in min..max {
        if num % 2 == 0 { continue 'odd_loop; }
        println!("{}", num);
    }
}

/// Push a random number to vector.
fn push_rnd_to_vector(v: &mut Vec<i32>) {
    let mut rng = rand::thread_rng();
    let rand_num = rng.gen_range(1, 101);
    v.push(rand_num);
}
