extern crate basic_game;

use basic_game::Player;
use basic_game::PlayerBuilder;

fn main() {
    let mut player: Player = Player::new("Victor Huberta", 100, 4);
    println!("Current {}'s HP is {}.", player.name, player.hp);
    println!("{}'s resistance is {}.", player.name, player.resistance());
    println!("Damaging {}...", player.name);
    player.damage(10);
    println!("{}'s HP is now {}.", player.name, player.hp);

    let mut builder: PlayerBuilder = PlayerBuilder::new();
    let player2: Player = builder.name("Pokemon").hp(150).defense(1).finalize();
    println!("Player 2's name is {}.", player2.name);
}
