//! The `basic_game` crate provides necessary player
//! structs and their implementations.
//!
//! # Examples
//!
//! ```
//! assert_eq!(4, 4);
//! ```

pub struct Player<'a> {
    pub name: &'a str,
    pub hp: u64,
    pub defense: u64
}

impl<'a> Player<'a> {
    pub fn new(name: &str, hp: u64, defense: u64) -> Player {
        Player { name: name, hp: hp, defense: defense }
    }

    pub fn damage(&mut self, amount: u64) {
        self.hp = self.hp + self.defense - amount;
    }

    pub fn resistance(&self) -> u64 {
        self.hp / self.defense
    }
}

pub struct PlayerBuilder<'a> {
    pub name: &'a str,
    pub hp: u64,
    pub defense: u64
}

impl<'a> PlayerBuilder<'a> {
    pub fn new() -> PlayerBuilder<'a> {
        PlayerBuilder { name: "Player", hp: 100, defense: 5 }
    }

    pub fn name(&'a mut self, name: &'a str) -> &'a mut PlayerBuilder {
        self.name = name;
        self
    }

    pub fn hp(&'a mut self, hp: u64) -> &'a mut PlayerBuilder {
        self.hp = hp;
        self
    }

    pub fn defense(&'a mut self, defense: u64) -> &'a mut PlayerBuilder {
        self.defense = defense;
        self
    }

    pub fn finalize(&self) -> Player {
        Player { name: self.name, hp: self.hp, defense: self.defense }
    }
}
