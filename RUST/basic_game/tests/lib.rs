//! The module tests for several player definitions
//! and implementations.

extern crate basic_game;

use basic_game::Player;

#[test]
fn test_player_struct() {
    let player = Player { name: "Player 1", hp: 100, defense: 4 };
    assert_eq!("Player 1", player.name);
    assert_eq!(100, player.hp);
    assert_eq!(4, player.defense);
}
