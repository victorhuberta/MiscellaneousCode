extern crate phrases;

use phrases::english::{greetings as en_greetings, farewells as en_farewells};
use phrases::japanese::{greetings as jp_greetings, farewells as jp_farewells};

fn main() {
    println!("Hello in English: {}.", en_greetings::hello());
    println!("Goodbye in English: {}.", en_farewells::goodbye());

    println!("Hello in Japanese: {}.", jp_greetings::hello());
    println!("Goodbye in Japanese: {}.", jp_farewells::goodbye());
}
