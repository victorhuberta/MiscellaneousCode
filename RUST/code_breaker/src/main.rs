extern crate code_breaker;

use code_breaker::ciphers::substitution::caesar_cipher;
use code_breaker::ciphers::transposition::rail_fence;
use code_breaker::ciphers::digraph::play_fair;

fn main() {
    caesar_cipher::decipher("RDBEJIXCV IWTDGN XH WDL TUUXRXTCIAN EGDQATBH RPC QT HDAKTS \
        JHXCV PAVDGXIWBH DC PQHIGPRI BDSTAH");

    rail_fence::decipher("ATOETEOUNEAWRAOT UANDHLFTACLWEBMI TACWELCANILEAOPO \
        OREIMIOTDFYCSUUN MERTONMISIHAOTTA CNHDGPOPCONNCA");

    play_fair::decipher("UG ST OL CP UE AK NW WP AF EU US OL FP GD PK UL FR LS RM AK EK AF \
        ZG DA LG WD IT CM AF YT QO LS RA DX TO GP UL SE PE SW");
}
