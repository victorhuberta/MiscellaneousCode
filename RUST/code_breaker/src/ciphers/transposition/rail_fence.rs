//! Module: rail_fence; @Author: Victor Huberta
//! ```
//! The module handles the encipherment and
//! decipherment of plain text and Rail fence
//! ciphers, respectively.
//! ```
//!

/// Function: decipher
/// # Args
/// cipher -> the cipher needs to be deciphered.
/// ```
/// Decipher a Rail fence cipher into plain text by
/// using brute-force i.e. test the first 20
/// possible transpositions.
/// ```
///
pub fn decipher(cipher: &str) {
    println!("====================================================");
    println!("Deciphering rail fence cipher text...");
    println!("====================================================");

    let mut cipher = cipher.to_string().to_lowercase();
    remove_all_whitespaces(&mut cipher);

    let len = cipher.chars().count() as f64;
    const MAX_SHIFT: usize = 20;

    for shift in 2..MAX_SHIFT {
        let shift = shift as f64;
        let chars_per_line = (len / shift).ceil(); // round it up!
        let mut lines = [chars_per_line; MAX_SHIFT];
        let remainder_chars = len % chars_per_line;

        if remainder_chars != 0f64 {
            lines[(shift - 1f64) as usize] = remainder_chars;
        }

        for i in (0..shift as usize).rev() {
            if (lines[i - 1] - lines[i]) >= 2f64 {
                let new_value = (lines[i] + lines[i - 1]) / 2f64;
                lines[i - 1] = new_value.ceil(); // round it up!
                lines[i] = new_value.floor(); // round it down!
            } else {
                break;
            }
        }

        let mut plain = String::new();
        for i in 0..chars_per_line as usize {
            plain.push_str(&cipher[i..i+1]);
            let mut next_index = i;
            for j in 0..shift as usize {
                next_index += lines[j] as usize;
                if next_index as f64 >= len { break; }
                plain.push_str(&cipher[next_index..(next_index + 1)]);
            }
        }
        plain.truncate(len as usize);
        println!("{} shifts -> {}", shift, plain);
    }
    println!("====================================================");
}

/// Function: remove_all_whitespaces
/// # Args
/// text -> the text which its whitespaces need
/// to be removed from.
/// ```
/// Remove all whitespaces from a given text.
/// ```
///
fn remove_all_whitespaces(text: &mut String) {
    let mut new_text = String::new();
    {
        let mut iter = text.split_whitespace();

        loop {
            match iter.next() {
                Some(s) => new_text.push_str(s),
                None => break
            }
        }
    }
    *text = new_text.clone();
}
