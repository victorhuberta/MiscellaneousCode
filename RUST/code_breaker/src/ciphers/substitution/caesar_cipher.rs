//! Module: caesar_cipher; @Author: Victor Huberta
//! ```
//! The module handles the encipherment and
//! decipherment of plain text and Caesar
//! ciphers, respectively.
//! ```
//!

/// Function: decipher
/// # Args
/// cipher -> the cipher needs to be deciphered.
/// ```
/// Decipher a Caesar cipher into plain text by
/// using brute-force i.e. test all 26 possible
/// keys.
/// ```
///
pub fn decipher(cipher: &str) {
    println!("====================================================");
    println!("Running Caesar cipher on the text...");
    println!("====================================================");
    let cipher = cipher.to_string().to_lowercase();

    for i in 1..26 {
        let mut result = String::new();
        let mut chars = cipher.chars();

        loop {
            match chars.next() {
                Some(c) => {
                    match c {
                        'a' ... 'z' => transform_and_append(&mut result, c, i),
                        _ => result.push(c)
                    }
                },
                None => {
                    println!("Key #{}: {}", i, result);
                    break;
                }
            }
        }
    }
    println!("====================================================");
}

/// Function: transform_and_append
/// # Args
/// result -> an output variable of type String.
/// c -> the character needs to be transformed.
/// i -> the number of shifts.
/// ```
/// Transform a character into another by shifting
/// the character `i` times to the right.
/// ```
///
fn transform_and_append(result: &mut String, c: char, i: u8) {
    let mut n = c as u8 + i;

    if n - ('a' as u8) >= 26 { n -= 26; }
    result.push(n as char);
}
