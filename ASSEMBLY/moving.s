.data

Byte1:
    .byte 10

Byte2:
    .byte 20

Int32:
    .int 45

Int16:
    .short 12

Float:
    .float 23.45

FloatArray:
    .float 23.45,44.1,12.24

.text

.globl _start

_start:
    movl $Int32, %eax
    movl (%eax), %ebx
    
    movw Int16, %cx
    movw %cx, Int16

    movb Byte1, %al
    movb Byte2, %bl
    movb %al, Byte2
    movb %bl, Byte1

    movl $1, %eax
    movl $0, %ebx
    int $0x80
