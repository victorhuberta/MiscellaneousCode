section .data
	data db 'Welcome to the assembly world!'
	lenData equ $ - data
	msgDone db 'Data written to file.', 0xA
	lenDone equ $ - msgDone
	filename db 'myfile.txt'

section .bss
	fd_out resb 1
	fd_in resb 1
	info resb 30

section .text
	global _start

	_start:
		;create file
		mov eax, 8
		mov ebx, filename
		mov ecx, 0777
		int 80h
		mov [fd_out], byte eax

		;write file
		mov eax, 4
		mov ebx, [fd_out]
		mov ecx, data
		mov edx, lenData
		int 80h

		;close file
		mov eax, 6
		mov ebx, [fd_out]

		;show if done
		mov eax, 4
		mov ebx, 1
		mov ecx, msgDone
		mov edx, lenDone
		int 80h

		;open file
		mov eax, 5
		mov ebx, filename
		mov ecx, 0
		mov edx, 0777
		int 80h
		mov [fd_in], byte eax

		;read file
		mov eax, 3
		mov ebx, [fd_in]
		mov ecx, info
		mov edx, 30
		int 80h

		;close file
		mov eax, 6
		mov ebx, [fd_in]
		int 80h

		;show info
		mov eax, 4
		mov ebx, 1
		mov ecx, info
		mov edx, 30
		int 80h

		mov eax, 1
		int 80h
