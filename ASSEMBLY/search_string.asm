section .data
	s1 db 'Hello, world!', 0
	lenS1 equ $ - s1
	msgFound db 'Found!', 0
	lenFound equ $ - msgFound
	msgNotFound db 'Not Found!', 0
	lenNotFound equ $ - msgNotFound

section .text
	global _start

	_start:
		mov ecx, lenS1
		mov edi, s1
		mov al, 'X'
		cld
		repne scasb
		je found

		mov eax, 4
		mov ebx, 1
		mov ecx, msgNotFound
		mov edx, lenNotFound
		int 80h
		jmp exit

	found:
		mov eax, 4
		mov ebx, 1
		mov ecx, msgFound
		mov edx, lenFound
		int 80h

	exit:
		mov eax, 1
		int 80h