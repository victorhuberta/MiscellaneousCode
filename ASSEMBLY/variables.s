.data

HelloWorldString:
    .ascii "Hello, world!\n"

ByteLocation:
    .byte 15

Int32:
    .int 40

Int16:
    .short 20

Float:
    .float 12.345

IntegerArray:
    .int 10,20,30,40,50

FloatArray:
    .float 10.01,34.56,21.1,0.567

.bss
    .comm LargeBuffer, 10000

.text

.globl _start

_start:
    nop
    movl $1, %eax
    movl $0, %ebx
    int $0x80
