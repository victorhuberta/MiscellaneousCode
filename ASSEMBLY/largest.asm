section .data
	msg db 'The largest among 34, 47, and 16 is '
	lenMsg equ $-msg
	num1 dd '34'
	num2 dd '47'
	num3 dd '16'

section .bss
	largest resb 2

section .text
	global _start

	_start:
		mov ecx, [num1]
		cmp ecx, [num2]
		jg check_third
		mov ecx, [num2]

	check_third:
		cmp ecx, [num3]
		jg _exit
		mov ecx, [num3]

	_exit:
		mov [largest], ecx
		mov eax, 4
		mov ebx, 1
		mov ecx, msg
		mov edx, lenMsg
		int 80h

		mov eax, 4
		mov ebx, 1
		mov ecx, largest
		mov edx, 2
		int 80h

		mov eax, 1
		int 80h