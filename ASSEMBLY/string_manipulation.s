.data

HelloWorldString:
    .asciz "Hello world"

H3ll0:
    .asciz "H3ll0"

.bss
    .lcomm DestinationString, 100

.text

.globl _start

_start:
    #moving strings around
    movl $HelloWorldString, %esi
    movl $DestinationString, %edi
    movsb
    movsw
    movsl
    
    #set the direction flag
    std
    cld
    
    #repeat movsb until the end of string
    subl $7, %esi
    subl $7, %edi
    movl $11, %ecx
    cld
    rep movsb

    #load string from memory into eax register
    leal HelloWorldString, %esi
    lodsb
    lodsw
    lodsl
    
    #store string from eax register to memory
    leal DestinationString, %edi
    stosb
    stosw
    stosl

    #comparing strings
    leal HelloWorldString, %esi
    leal DestinationString, %edi
    repz cmpsb

    #exit
    movl $1, %eax
    movl $0, %ebx
    int $0x80
    
    
