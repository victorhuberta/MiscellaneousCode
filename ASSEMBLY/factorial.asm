section .data
	msg db 'The factorial of 3 is : '
	lenMsg equ $ - msg
	fact db '0'

section .text
	global _start

	_start:
		mov bx, 4
		call proc_fact
		aam
		add ah, 30h
		add al, 30h
		xchg ah, al
		mov [fact], ax

		mov eax, 4
		mov ebx, 1
		mov ecx, msg
		mov edx, lenMsg
		int 80h

		mov eax, 4
		mov ebx, 1
		mov ecx, fact
		mov edx, 2
		int 80h

		mov eax, 1
		int 80h

	proc_fact:
		cmp bl, 1
		jg do_calculation
		mov ax, 1
		ret

	do_calculation:
		dec bl
		call proc_fact
		inc bl
		mul bl ;ax = al * bl
		ret