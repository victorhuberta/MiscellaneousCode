section .data
	msg db 'The sum is : '
	len equ $-msg
	num1 db '12345'
	num2 db '23456', 0xA
	sum db '     ', 0xA, 0xD
	addition db ' + '

section .text
	global _start:

	_start:
		mov esi, 4
		mov ecx, 5
		clc

	_repeat:
		mov al, [num1 + esi]
		adc al, [num2 + esi]
		aaa
		pushf
		or al, 30h ;this line could be replaced by 'add al, 30h'
		popf
		mov [sum + esi], al
		dec esi
		loop _repeat

	_display:
		mov eax, 4
		mov ebx, 1
		mov ecx, num1
		mov edx, 5
		int 80h

		mov eax, 4
		mov ebx, 1
		mov ecx, addition
		mov edx, 3
		int 80h

		mov eax, 4
		mov ebx, 1
		mov ecx, num2
		mov edx, 6
		int 80h

		mov eax, 4
		mov ebx, 1
		mov ecx, msg
		mov edx, len
		int 80h

		mov eax, 4
		mov ebx, 1
		mov ecx, sum
		mov edx, 7
		int 80h

	_exit:
		mov eax, 1
		int 80h
