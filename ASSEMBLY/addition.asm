SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDIN equ 0
STDOUT equ 1

section .data
	promptMsg1 db 'Enter first value : '
	lenPromptMsg1 equ $-promptMsg1
	promptMsg2 db 'Enter second value : '
	lenPromptMsg2 equ $-promptMsg2
	operator db ' + '
	equal db ' = '

section .bss
	num1 resb 3
	num2 resb 3
	res resb 3

section .text
	global _start:
	_start:
	mov edx, lenPromptMsg1
	mov ecx, promptMsg1
	mov ebx, STDOUT
	mov eax, SYS_WRITE
	int 80h

	mov edx, 3
	mov ecx, num1
	mov ebx, STDIN
	mov eax, SYS_READ
	int 80h

	mov edx, lenPromptMsg2
	mov ecx, promptMsg2
	mov ebx, STDOUT
	mov eax, SYS_WRITE
	int 80h

	mov edx, 3
	mov ecx, num2
	mov ebx, STDIN
	mov eax, SYS_READ
	int 80h

	mov edx, 2
	mov ecx, num1
	mov ebx, STDOUT
	mov eax, SYS_WRITE
	int 80h

	mov edx, 3
	mov ecx, operator
	mov ebx, STDOUT
	mov eax, SYS_WRITE
	int 80h

	mov edx, 2
	mov ecx, num2
	mov ebx, STDOUT
	mov eax, SYS_WRITE
	int 80h

	mov edx, 3
	mov ecx, equal
	mov ebx, STDOUT
	mov eax, SYS_WRITE
	int 80h

	mov ax, [num1]
	sub ah, '0'
	sub al, '0'
	mov bx, [num2]
	sub bh, '0'
	sub bl, '0'
	add ax, bx
	aaa
	add ah, 30h
	add al, 30h
	mov [res], ax

	mov edx, 3
	mov ecx, res
	mov ebx, STDOUT
	mov eax, SYS_WRITE
	int 80h

	mov eax, SYS_EXIT
	int 80h
