section .data
	promptMsg db 'Please enter a number : '
	lenPromptMsg equ $-promptMsg
	dispMsg db 'The number you entered : '
	lenDispMsg equ $-dispMsg

section .bss
	num resb 5

section .text
	global _start
	_start:

	;User prompt
	mov eax, 4
	mov ebx, 1	;stdout
	mov ecx, promptMsg
	mov edx, lenPromptMsg
	int 80h

	;Read input
	mov eax, 3
	mov ebx, 2	;stdin
	mov ecx, num
	mov edx, 5
	int 80h

	;Display 'The number you entered : '
	mov eax, 4
	mov ebx, 1	;stdout
	mov ecx, dispMsg
	mov edx, lenDispMsg
	int 80h

	;Display the number
	mov eax, 4
	mov ebx, 1	;stdout
	mov ecx, num
	mov edx, 5
	int 80h

	;Exit code (call to sys_exit)
	mov eax, 1
	mov ebx, 0
	int 80h
