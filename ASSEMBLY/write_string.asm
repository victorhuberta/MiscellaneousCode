section .data
	s1 db 'Hello, programmers!', 0xA
	lenS1 equ $ - s1
	s2 db 'Welcome to the world of '
	lenS2 equ $ - s2
	s3 db 'Linux Assembly Programming!', 0xA
	lenS3 equ $ - s3

%macro write_string 2
	push eax
	push ebx
	push ecx
	push edx
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80h
	pop edx
	pop ecx
	pop ebx
	pop eax
%endmacro

section .text
	global _start

	_start:
		write_string s1, lenS1
		write_string s2, lenS2
		write_string s3, lenS3
		mov eax, 1
		int 80h
