section .data
	msg db 'Allocated 16kb(s) !', 0xA
	lenMsg equ $ - msg

section .text
	global _start

	_start:

		;to get the current highest available address
		mov eax, 45 ;sys_brk
		xor ebx, ebx
		int 80h

		add eax, 16384 ; highest address + number of bytes to be reserved
		mov ebx, eax
		mov eax, 45
		int 80h

		cmp eax, 0 ;if sys_brk return -1 (error)
		jl exit
		mov edi, eax ;highest available address
		sub edi, 4 ;pointing to the last DWORD
		mov ecx, 4096 ;number of DWORDs allocated
		xor eax, eax
		std
		rep stosd ;store 4 (dword) * 4096 = 16384 bytes
		cld

		mov eax, 4
		mov ebx, 1
		mov ecx, msg
		mov edx, lenMsg
		int 80h
	exit:
		mov eax, 1
		xor ebx, ebx
		int 80h