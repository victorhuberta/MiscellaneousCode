SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDIN equ 0
STDOUT equ 1

section .data
	msg db 'Input your number : '
	lenMsg equ $-msg
	addition db ' + '
	substraction db ' - '
	multiplication db ' * '
	division db ' / '
	equal db ' = '
	num1 db '10'
	num2 db '20'

section .bss
	res resb 3

section .text
	global _start

	_start:

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, num1
	mov edx, 2
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, addition
	mov edx, 3
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, num2
	mov edx, 2
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, equal
	mov edx, 3
	int 80h

	mov ax, [num1]
	sub ah, 30h
	sub al, 30h
	mov bx, [num2]
	sub bh, 30h
	sub bl, 30h
	add ax, bx
	aaa
	add ah, 30h
	add al, 30h
	mov [res], ax

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, res
	mov edx, 3
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, num2
	mov edx, 2
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, substraction
	mov edx, 3
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, num1
	mov edx, 2
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, equal
	mov edx, 3
	int 80h

	mov ax, [num1]
	sub ah, '0'
	sub al, '0'
	mov bx, [num2]
	sub bh, '0'
	sub bl, '0'
	xchg ax, bx
	sub ax, bx
	aas
	add ah, '0'
	add al, '0'
	mov [res], ax

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, res
	mov edx, 3
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, num1
	mov edx, 2
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, multiplication
	mov edx, 3
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, num2
	mov edx, 2
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, equal
	mov edx, 3
	int 80h

	mov ax, [num1]
	sub ah, '0'
	sub al, '0'
	mov dx, [num2]
	sub dh, '0'
	sub dl, '0'
	mul dx
	aam
	add ah, '0'
	add al, '0'
	mov [res], ax

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, res
	mov edx, 2
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, num2
	mov edx, 2
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, division
	mov edx, 3
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, num1
	mov edx, 2
	int 80h

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, equal
	mov edx, 3
	int 80h

	mov ax, [num1]
	sub ah, '0'
	sub al, '0'
	mov dx, [num2]
	sub dh, '0'
	sub dl, '0'
	aad
	xchg ax, dx
	div dx
	add ah, '0'
	add al, '0'
	mov [res], ax

	mov eax, SYS_WRITE
	mov ebx, STDOUT
	mov ecx, res
	mov edx, 2
	int 80h

	mov eax, SYS_EXIT
	int 80h

