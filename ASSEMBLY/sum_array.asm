section .data
	global x
	x:
		db 2
		db 3
		db 4
	; x db 2, 3, 4
	sum:
		db 0

section .text
	global _start

	_start:
		mov eax, 3
		mov ebx, 0
		mov ecx, x

	top:
		add ebx, [ecx]
		inc ecx
		dec eax
		jnz top

	done:
		add ebx, '0'
		mov [sum], byte ebx

		mov eax, 4
		mov ebx, 1
		mov ecx, sum
		mov edx, 1
		int 80h

	exit:
		mov eax, 1
		int 80h