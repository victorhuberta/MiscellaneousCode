.data
    HelloWorld:
	.asciz "Hello world!\n"

.text

.globl _start

.type PrintFunction, @function

PrintFunction:
    push %rbp
    mov %rsp, %rbp
    
    movl $4, %eax
    movl $1, %ebx
    movl 16(%rbp), %ecx
    movl 24(%rbp), %edx
    int $0x80

    mov %rbp, %rsp
    pop %rbp
    ret

_start:
    push $13
    push $HelloWorld
    call PrintFunction
    add $16, %rsp

    Exit:
	movl $1, %eax
	movl $0, %ebx
	int $0x80
