.data
    HelloWorld:
	.asciz "Hello World!\n"
    HelloFunction:
	.asciz "Hello Function!\n"

.bss
    .lcomm StringPointer, 4
    .lcomm StringLength, 4

.text
.globl _start

.type Println, @function

Println:
    movl $4, %eax
    movl $1, %ebx
    movl StringPointer, %ecx
    movl StringLength, %edx
    int $0x80
    ret

_start:
    
    movl $HelloWorld, StringPointer
    movl $13, StringLength
    call Println

    movl $HelloFunction, StringPointer
    movl $16, StringLength
    call Println

    Exit:
	movl $1, %eax
	movl $0, %ebx
	int $0x80
