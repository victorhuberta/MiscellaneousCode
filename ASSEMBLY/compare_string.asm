section .data
	s1 db 'Hello, world!', 0
	len1 equ $ - s1
	s2 db 'Hello, there!', 0
	len2 equ $ - s2
	msgEqual db 'Strings are equal!', 0xA
	lenEqual equ $ - msgEqual
	msgNotEqual db 'Strings are not equal!', 0xA
	lenNotEqual equ $ - msgNotEqual

section .bss
	remainder resb 2

section .text
	global _start

	_start:
		mov ecx, len2
		mov esi, s1
		mov edi, s2
		cld
		repe cmpsb
		add ecx, '0'
		mov [remainder], ecx
		jecxz equal

		mov eax, 4
		mov ebx, 1
		mov ecx, msgNotEqual
		mov edx, lenNotEqual
		int 80h
		jmp exit

	equal:
		mov eax, 4
		mov ebx, 1
		mov ecx, msgEqual
		mov edx, lenEqual
		int 80h

	exit:
		mov eax, 4
		mov ebx, 1
		mov ecx, remainder
		mov edx, 2
		int 80h

		mov eax, 1
		int 80h