section .data
	file_name db '*.asm'
	msg db '*.asm found : '
	lenMsg equ $ - msg

section .bss
	fd_in resb 1
	count resb 1

section .text
	global _start

	_start:
		mov ecx, '0'
		sub ecx, 30h
	search:
		push ecx
		mov eax, 5
		mov ebx, file_name
		mov ecx, 0
		mov edx, 0777
		int 80h
		;what is the error code in eax??
		;I assumed that it is a negative number.
		cmp eax, 0
		jl exit

		mov [fd_in], eax

		mov eax, 6
		mov ebx, [fd_in]
		int 80h

		pop ecx
		inc ecx
		jmp search

	exit:
		pop ecx
		add ecx, 30h
		mov [count], ecx

		mov eax, 4
		mov ebx, 1
		mov ecx, msg
		mov edx, lenMsg
		int 80h

		mov eax, 4
		mov ebx, 1
		mov ecx, count
		mov edx, 1
		int 80h

		mov eax, 1
		int 80h