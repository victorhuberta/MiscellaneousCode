.data
    HelloWorld:
	.asciz "Hello world!\n"
    HelloFunction:
	.asciz "Hello Function!\n"

.text
.globl _start
.type Println, @function

Println:
    movl $4, %eax
    movl $1, %ebx
    int $0x80
    ret

_start:
    leal HelloWorld, %ecx
    movl $13, %edx
    call Println

    leal HelloFunction, %ecx
    movl $16, %edx
    call Println

    Exit:
	movl $1, %eax
	movl $0, %ebx
	int $0x80    
