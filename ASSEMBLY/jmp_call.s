.data
    HelloWorldString:
        .asciz "Hello world"

    CallString:
        .asciz "Call succeed."

.text

.globl _start

_start:
    nop
    call CallFunc
    
    DisplayString:
        movl $4, %eax
        movl $1, %ebx
        leal HelloWorldString, %ecx
        movl $11, %edx
        int $0x80

    ExitApp:
        movl $1, %eax
        movl $10, %ebx
        int $0x80

    CallFunc:
        movl $4, %eax
        movl $1, %ebx
        leal CallString, %ecx
        movl $13, %edx
        int $0x80
        ret
