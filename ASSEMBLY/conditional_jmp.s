.data

    HelloWorldString:
        .asciz "Hello world\n"

    ZeroFlagSet:
        .asciz "Zero Flag Set\n"
    ZeroFlagNotSet:
        .asciz "Zero Flag Not Set\n"

.text
.globl _start

_start:
    movl $10, %eax
    xorl %eax, %eax    
    jz DisplayString

    NoZeroFlag:
        movl $4, %eax
        movl $1, %ebx
        movl $ZeroFlagNotSet, %ecx
        movl $18, %edx
        int $0x80
	jmp ExitApp

    ZeroFlag:
        movl $4, %eax
        movl $1, %ebx
        movl $ZeroFlagSet, %ecx
        movl $14, %edx
        int $0x80

    ExitApp:
        movl $1, %eax
        movl $0, %ebx
        int $0x80

    DisplayString:
        movl $10, %ecx
        PrintTenTimes:
        	push %rcx
		movl $4, %eax
		movl $1, %ebx
		movl $HelloWorldString, %ecx
		movl $12, %edx
		int $0x80
		pop %rcx
	loop PrintTenTimes
	jmp ExitApp
