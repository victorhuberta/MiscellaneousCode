use hr;
# question 1
select e.last_name, d.department_id, d.department_name
from employees e
join departments d
on e.department_id = d.department_id;

# question 2
select distinct j.job_title as "Job Title",
 concat(l.street_address, ",", l.city) as "Location"
from jobs j
join employees e
on j.job_id = e.job_id
join departments d
on e.department_id = d.department_id
join locations l
on d.location_id = l.location_id
where d.department_id = 80;

# question 3
Select e.last_name, d.department_name, l.location_id, l.city
from employees e
join departments d
on e.department_id = d.department_id
join locations l
on d.location_id = l.location_id
where e.commission_pct is not NULL;

# question 4
select e.last_name, d.department_name
from employees e
join departments d
on e.department_id = d.department_id
where e.last_name LIKE "%a%"
order by e.last_name ASC ;

# question 5
select e.last_name, j.job_title, d.department_id, d.department_name
from employees e
join jobs j
on e.job_id = j.job_id
join departments d
on e.department_id = d.department_id
join locations l
on d.location_id = l.location_id
where upper(l.city) = "TORONTO";

# question 6 (8?)
select e.last_name as "Employee Name", 
d.department_id as "Department Number", 
(select group_concat(last_name separator ',') 
from employees 
where department_id = e.department_id
and employee_id != e.employee_id) as "Other Employees"
from employees e
join departments d
on e.department_id = d.department_id
group by e.last_name;

# question 7 (10?)
select last_name, hire_date
from employees
where hire_date > 
(select hire_date from employees where last_name LIKE "%Davies%")
group by last_name;