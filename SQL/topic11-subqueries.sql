use hr;

select job_id, job_title, min_salary, max_salary from jobs;

select employee_id, first_name, last_name, job_id, salary
from employees
where (job_id,salary) in 
	(select job_id, salary
	from employees
	where (salary >= 2500)
	and job_id in
		(select job_id 
		from jobs 
		where job_title like "%stock%clerk"));