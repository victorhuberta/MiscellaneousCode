create database if not exists parent_child;

use parent_child;

create table parent (
    id INT NOT NULL,
    name VARCHAR(30),
    PRIMARY KEY (id)
);

create table child (
    id INT NOT NULL,
    parent_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (parent_id)
        REFERENCES parent(id)
        ON DELETE CASCADE
);