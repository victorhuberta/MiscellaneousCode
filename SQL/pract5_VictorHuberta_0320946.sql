use hr;
desc employees;

# number 1
select last_name, salary from employees where salary > 12000;

# number 2
select last_name, department_id from employees where employee_id = 176;

# number 3
select e.last_name, e.job_id, jh.start_date 
from employees e inner join job_history jh on (e.employee_id = jh.employee_id)
where e.last_name = 'Matos' or e.last_name = 'Taylor'
order by jh.start_date ASC;

# number 4
select last_name, department_id from employees
where department_id in (20, 50)
order by last_name ASC;

# number 5
select last_name as 'Employee', salary as 'Monthly Salary' from employees
where (salary between 5000 and 12000) and (department_id in (20, 50));

# number 6
select last_name, hire_date from employees
where hire_date like '1994%';

# number 7 (needs fixing)
select e.last_name, j.job_title
from employees e inner join jobs j on (e.job_id=j.job_id)
where e.manager_id = NULL;

# number 8
select last_name, salary, commission_pct from employees
where commission_pct is not NULL
order by salary DESC, commission_pct DESC;

# number 9
select last_name from employees
where last_name like '__a%';

# number 10
select last_name from employees
where last_name like '%a%' and last_name like '%e%';

# number 11
select e.last_name, j.job_title, e.salary
from employees e inner join jobs j on (e.job_id = j.job_id)
where (j.job_title like '%sales%representative%'
or j.job_title like '%stock%clerk%')
and e.salary not in (2500, 3500, 7000);

# number 12