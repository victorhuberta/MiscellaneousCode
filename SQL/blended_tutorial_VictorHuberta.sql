use hr;

# question 1
select e.last_name, j.job_title, e.salary 
from employees e 
join jobs j 
on e.job_id = j.job_id
where e.salary not in (2500, 3500, 7000);

# question 2
select last_name, salary, commission_pct 
from employees 
where commission_pct = 0.2;

# question 3
select employee_id, last_name, 
salary, round((salary + (salary * 0.15))) as "New Salary" 
from employees;

# question 5
select employee_id, last_name, salary, 
@new_salary:=round((salary + salary * 0.15)) as "New Salary",
(FORMAT(@new_salary - salary, 0)) as Increase
from employees;

# question 6
select first_name, last_name, 
char_length(concat(first_name, last_name)) as Length
from employees
where first_name regexp '^J|^M|^A' 
order by last_name ASC;