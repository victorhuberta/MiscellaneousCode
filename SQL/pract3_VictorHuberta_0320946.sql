USE test;
desc marks;

select * from marks 
where studentID 
between 2 and 3
and first_name = 'Bill' and mark < 80;

select * from marks
where first_name like "B%" 
and last_name not like "D__can";

select * from marks
where first_name not like "%b%"
limit 2, 4;

# Drill 1
select * from marks 
where mark > 78;

# Drill 2
select * from marks
where first_name like 'F%';

# Drill 3
select * from marks
where mark between 78 and 80;

# Drill 4
select * from marks
where mark between 78 and 80 
and first_name like 'S%';