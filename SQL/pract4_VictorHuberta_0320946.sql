select (17 + 23) / SQRT(64);
select user(), version(), database();
select 'VIcToR' = 'victor';

set @price = 18.00;
set @items = 40;
set @discount = 0.5;
set @total = (@price - (@price * @discount)) * @items;
select @total;

use test;
select * from marks;
select count(*) from marks;
select first_name, max(mark) from marks;
select first_name, max(mark) from marks
group by first_name;

use hr;
select department_id, first_name, salary
from employees
where salary = (select min(salary) from employees);

select concat(first_name, job_title) from employees, jobs
where employees.employee_id = 102 and jobs.job_title like "%programmer%";

use test;
select max(mark) from marks;
select avg(mark) from marks;
