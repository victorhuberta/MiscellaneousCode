use database_system;
create table Product (
	PName VARCHAR(40),
	Price FLOAT,
	Category VARCHAR(40),
	Manufacturer VARCHAR(40)
);
insert into Product values (
	'Gizmo', 19.99, 'Gadgets', 'GizmoWorks'
);