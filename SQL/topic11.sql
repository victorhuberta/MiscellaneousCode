create database if not exists topic11;

use topic11;

create table 'office'(
	office_num INT NOT NULL,
	office_street VARCHAR(20),
	office_city VARCHAR(50),
	office_state CHAR(2),
	office_zipcode VARCHAR(10),
	office_name VARCHAR(50),
	office_phone VARCHAR(15),
	office_fax VARCHAR(15),
	PRIMARY KEY (office_num)
);

create table employee (
	emp_num INT NOT NULL,
	emp_fName VARCHAR(50),
	emp_lName VARCHAR(50),
	emp_SSN CHAR(11),
	emp_street VARCHAR(20),
	emp_city VARCHAR(50),
	emp_state CHAR(2),
	emp_zipcode VARCHAR(10),
	emp_phone VARCHAR(15),
	emp_fax VARCHAR(15),
	office_num INT,
	PRIMARY KEY (emp_num),
	FOREIGN KEY (office_num)
		REFERENCES office(office_num)
		ON DELETE CASCADE
);

create table property (
	prop_id INT NOT NULL,
	prop_street VARCHAR(20),
	prop_city VARCHAR(50),
	prop_state CHAR(2),
	prop_type VARCHAR(25),
	prop_baths INT,
	prop_rooms INT,
	prop_monthlyRent DECIMAL(9,2),
	office_num INT,
	PRIMARY KEY (prop_id),
	FOREIGN KEY (office_num)
		REFERENCES office(office_num)
		ON DELETE CASCADE
);

create table inspection (
	insp_date INT NOT NULL,
	prop_id INT,
	insp_comment VARCHAR(255),
	emp_num INT,
	PRIMARY KEY (insp_date),
	FOREIGN KEY (prop_id)
		REFERENCES property(prop_id)
		ON DELETE CASCADE,
	FOREIGN KEY (emp_num)
		REFERENCES employee(emp_num)
		ON DELETE CASCADE
);

create table manager (
	emp_num INT,
	man_salary DECIMAL(9,2),
	man_car_allowance DECIMAL(9,2),
	FOREIGN KEY (emp_num)
		REFERENCES employee(emp_num)
		ON DELETE CASCADE
);

create table associate (
	emp_num INT,
	assoc_hourly_rate DECIMAL(9,2),
	FOREIGN KEY (emp_num)
		REFERENCES employee(emp_num)
		ON DELETE CASCADE
);

