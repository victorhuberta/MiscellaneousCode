alter table office modify column office_state VARCHAR(30);

alter table office modify column office_num INT AUTO_INCREMENT;

insert into office 
(office_street, office_city, office_state, office_zipcode, office_name, office_phone, office_fax) 
values ('12' 'Subang Jaya', 'Selangor', '40400', 'TTI', '123456', '1234567'),
('13', 'Sunway', 'Selangor', '45000', 'RDF', '098766', '0987653');

update office
set office_street = '44',
	office_city = 'Not Sunway'
where office_num = 2;