use hr;
#Q4
desc employees;
#Q5
select employee_id, last_name,
job_id, hire_date  
from employees;
#Q6
select employee_id as "Emp#",
 last_name as "Employee",
 job_id as "Job",
 hire_date as "Hire Date" 
from employees;
#Q7
select distinct(job_id) from employees;
#Q8
select concat(last_name, ", ", job_id)
 as "Employee and Title"
from employees;
#Q9
select concat_ws(",", employee_id, first_name,
last_name, email, phone_number, 
hire_date, job_id, salary, commission_pct,
 manager_id, department_id) as "THE_OUTPUT"
from employees;