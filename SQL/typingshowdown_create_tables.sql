use TypingShowdown;

create table words (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    word VARCHAR(25),
    difficulty VARCHAR(20),
    PRIMARY KEY (id)
);