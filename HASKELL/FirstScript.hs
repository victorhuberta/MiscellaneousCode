module FirstScript where

size :: Integer
size = 12 + 13

square :: Integer -> Integer
square n = n * n

double :: Integer -> Integer
double n = 2 * n

doubleSquare :: Integer -> Integer
doubleSquare n = double (square n)

squareDouble :: Integer -> Integer
squareDouble n = square (double n)

example :: Integer
example = double (size - square (2+2))
