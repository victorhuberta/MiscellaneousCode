revWords :: [a] -> [a]
revWords (x:xs) = revWords (xs:x)