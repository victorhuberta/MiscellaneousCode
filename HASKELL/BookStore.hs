-- file: BookStore.hs
type SerialNumber = Int
type Title = String
type Authors = [String]

data Book = Book SerialNumber Title Authors
			deriving (Show)

bookSerialNumber :: Book -> Int
bookSerialNumber (Book serialNumber _ _) = serialNumber

bookTitle :: Book -> String
bookTitle (Book _ title _) = title

bookAuthors :: Book -> [String]
bookAuthors (Book _ _ authors) = authors

data Magazine = Magazine SerialNumber Title Authors
				deriving (Show)

magazineSerialNumber :: Magazine -> Int
magazineSerialNumber (Magazine serialNumber _ _) = serialNumber

magazineTitle :: Magazine -> String
magazineTitle (Magazine _ title _) = title

magazineAuthors :: Magazine -> authors
magazineAuthors (Magazine _ _ authors) = authors				

type CustomerID = Int
type AuthorName = String
type Content = String

data BookReview = BookReview CustomerID AuthorName Content
				  deriving (Show)

myBook = Book 12132134 "The Man In The Middle" ["Victor", "Jackie", "Jet Lee"]
myMagazine = Magazine 21312930 "Thunder in My Butt" ["Ferorika", "Hellsing"]

type CardNumber = Int
type CardHolder = String
type Addresses = [String]

data BillingInfo = CreditCard CardNumber CardHolder Addresses
				 | CashOnDelivery
				 | Invoice CustomerID
				   deriving (Show)





