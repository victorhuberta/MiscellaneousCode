lendMoney :: Double -> Double -> Maybe Double
lendMoney amount balance = 
    let 
        reserve = 100
        newBalance = balance - amount
	in 
        if newBalance < reserve
		then Nothing
		else Just newBalance

lendMoney2 :: Double -> Double -> Maybe Double
lendMoney2 amount balance = 
    if newBalance < reserve
    then Nothing
    else Just newBalance
    where 
        reserve = 100
        newBalance = balance - amount

lendMoney3 :: Double -> Double -> Maybe Double
lendMoney3 amount balance
    | (amount < 0 || balance <= 0) = Nothing
    | newBalance < reserve         = Nothing
    | otherwise                    = Just newBalance
    where
        reserve = 100
        newBalance = balance - amount