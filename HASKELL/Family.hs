data Family = Family {
	membersCountIn :: Int,
	membersOf :: [String],
	averageAgeOf :: Int
} deriving (Show)

ages :: [Int]
ages = [19, 22, 25]

countAverageAge :: Int
countAverageAge = (sum ages) `div` (length ages)

hubertaMembers :: [String]
hubertaMembers = ["Victor", "Vincent", "Vivian"]

hubertaFamily :: Family
hubertaFamily = Family {
	membersCountIn = (length hubertaMembers),
	membersOf = hubertaMembers,
	averageAgeOf = countAverageAge
}