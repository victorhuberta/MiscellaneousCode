module FirstWord where

import System.Environment (getArgs)
import SafeFunctions (safeHead)

printFirstWords inputFile = do
	input <- readFile inputFile
	printFirstWord (lines input)
	where
		printFirstWord (s:[]) = putStrLn s
		printFirstWord (s:ss) = do
			putStrLn (takeStr (safeHead (words s)))
			printFirstWord ss
			where
				takeStr a =
					case a of
						Just value -> value
						Nothing -> ""

main = do
	args <- getArgs
	case args of
		[input] -> printFirstWords input 
		_ -> error "Error: exactly one argument is needed"