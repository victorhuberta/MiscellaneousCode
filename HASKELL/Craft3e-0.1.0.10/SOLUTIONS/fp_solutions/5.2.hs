--5.2

orderTriple :: (Int,Int,Int) -> (Int,Int,Int)
orderTriple (x,y,z) = ( minThree x y z, middle x y z, maxThree x y z )


--helper functions
middle :: Int->Int->Int->Int
middle x y z
   | between y x z = x
   | between x y z = y
   | between x z y = z

between :: Int->Int->Int->Bool
between x y z
   | x < y && y < z = True
   | x > y && y > z = True
   | otherwise = False

max1 :: Int->Int->Int
max1 x y
   | x >= y = x
   | otherwise = y

maxThree :: Int->Int->Int->Int
maxThree x y z
   | max1 x y > z = max x y
   | otherwise =  z

min1 :: Int->Int ->Int
min1 x y
	| x <= y = x
	| otherwise = y
 
minThree x y z = min1 x (min1 y z)
