--6.2

superimposeLine :: [Char] -> [Char] -> [Char]
superimposeLine [] [] = []
superimposeLine (x:xs) (y:ys) = ( superimposeChar x y : superimposeLine xs ys )

--helper
superimposeChar :: Char -> Char -> Char
superimposeChar x y
   | x == '.' && y == '.' = y
   | otherwise = '#'
