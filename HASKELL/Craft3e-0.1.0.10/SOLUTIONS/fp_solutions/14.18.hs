--14.18

data Expr = Lit Int |
				Op Ops Expr Expr

data Ops = Add | Sub | Mul | Div | Mod


getOp Add = (+)
getOp Sub = (-)
getOp Mul = (*)
getOp Div = div
getOp Mod = mod

eval :: Expr -> Int
eval (Lit x) = x
eval (Op operator e1 e2) = (getOp operator) (eval e1) (eval e2)

size :: Expr -> Int
size (Lit n) = 0
size (Op operator e1 e2) = 1 + size e1 + size e2

showOp Add = "+"
showOp Sub = "-"
showOp Mul = "*"
showOp Div = "div"
showOp Mod = "mod"

showExpr :: Expr -> String
showExpr (Lit x) = show x
showExpr (Op op e1 e2) = "("  ++ showExpr e1 ++ showOp op ++ showExpr e2 ++ ")"

-- changes for "Mod" were two lines (one each in getOp and showOp)
