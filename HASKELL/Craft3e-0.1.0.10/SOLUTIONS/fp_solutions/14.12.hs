--14.12

-- address


data House = Num Int | Name String
type Name = String
type Zip = Int
type City = String
type Country = String

showHouse :: House -> String
showHouse (Num n) = show n
showHouse (Name s) = s

data Address = Address Name House Zip City Country
