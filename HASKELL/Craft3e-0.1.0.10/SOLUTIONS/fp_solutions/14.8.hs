--14.8

-- make a new instance of class Eq for type Shape
instance Eq Shape where
	(==) = eqShape

-- no need to derive (Eq) in Shape because it's explicitly defined
data Shape = Circle Float | Rectangle Float Float | Triangle Float Float Float

-- eqShape is the equality function to compare Shapes with
eqShape :: Shape -> Shape -> Bool
eqShape (Circle r1) (Circle r2) = r1 == (-r2)
eqShape _ _ = False

-- rectangles with negative sides are not equated, because they would
-- have a different "position in space" (mirrored at a side or corner)
-- (which is not the case with circles)
