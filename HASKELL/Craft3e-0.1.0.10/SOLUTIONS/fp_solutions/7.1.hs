--7.1
firstDigitPlusOne :: [Int] -> Int
firstDigitPlusOne x
	= case ( x ) of
		[] -> 0
		xs -> (head xs + 1)
