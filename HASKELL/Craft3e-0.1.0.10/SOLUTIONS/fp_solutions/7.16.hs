--7.16

import Prelude hiding(zip3)

--recursively:
zip3_1 :: [a]->[b]->[c] -> [(a,b,c)]

zip3_1 (x:[]) (y:[]) (z:[]) = [(x,y,z)]
zip3_1 (x:xs) (y:ys) (z:zs) = (x,y,z):(zip3_1 xs ys zs)

--using zip:
--zip3_2 :: [a]->[b]->[c] -> [(a,b,c)]
--zip3_2 (x:xs) (y:ys) (z:zs) = zip 
--mmmh... don't know how to do that
