--4.7
prod :: Int->Int->Int
prod x y
	| x == 0 || y == 0 = 0
	| otherwise =  x + (prod x (y-1))
