--14.5
data Shape = Circle Float | Rectangle Float Float | Triangle Float Float Float

perimeter :: Shape -> Float
perimeter (Triangle x y z) = x+y+z

isRound :: Shape -> Bool
isRound (Triangle _ _ _) = False

area :: Shape -> Float
area (Triangle x y z) = sqrt(s*(s-x)*(s-y)*(s-z))
	where
	s = perimeter (Triangle x y z) / 2

