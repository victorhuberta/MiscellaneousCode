--14.30
applyLeft :: (a -> c) -> Either a b -> c
applyLeft f x = either f (error "applyLeft applied to Right") x
