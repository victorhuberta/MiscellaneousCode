--helper functions
middle :: Int->Int->Int->Int
middle x y z
	| between y x z = x
	| between x y z = y
	| otherwise = z

between :: Int->Int->Int->Bool
between x y z
	| x <= y <= z = True
	| otherwise = False
