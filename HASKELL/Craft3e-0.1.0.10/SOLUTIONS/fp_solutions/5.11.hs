--5.11

import Prelude hiding(elem)

matches :: Int -> [Int] -> [Int]
matches x xs = [ n | n<-xs, n == x]

elem :: Int -> [Int] -> Bool
elem x xs = [] /= matches x xs
