--5.22
onSeparateLines :: [String] -> String
onSeparateLines [] = ""
onSeparateLines ( x:[] ) = x
onSeparateLines ( x:xs ) = x ++ "\n" ++ onSeparateLines xs
