--7.5

import Prelude hiding(and,or)

and, or :: [Bool] -> Bool

-- and [] = True: x and y and z and True == x and y and z
-- allows simple recursion
and [] = True
and (x:xs)
	| x == False = False
	| otherwise = and xs

-- or [] = False: x or y or z or False = x or y or z
-- allows simple recursion
or [] = False
or (x:xs)
	| x == True = True
	| otherwise = or xs
