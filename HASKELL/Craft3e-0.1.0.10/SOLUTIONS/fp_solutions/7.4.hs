--7.4

import Prelude hiding(product)

product :: [Int] -> Int
--product [] = 1 is needed to give simple recursive definition
product [] = 1
product (x:xs) = x * product xs
