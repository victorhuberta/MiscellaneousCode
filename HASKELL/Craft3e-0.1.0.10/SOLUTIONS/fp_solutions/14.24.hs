--14.24

data NTree =   NilT |
               Node Int NTree NTree

reflect :: NTree -> NTree
reflect NilT = NilT
reflect (Node x left right) = (Node x (reflect right) (reflect left))
