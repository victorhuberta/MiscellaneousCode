max :: Int->Int->Int
max x y
	| x >= y = x
	| otherwise y

maxThree :: Int->Int->Int->Int
maxThree x y z
	| max x y > z = max x y
	| otherwise z
