--4.5
rangeProduct :: Int->Int->Int
rangeProduct m n
	| m > n = 0
	| m==n = m
	| otherwise = m * rangeProduct (m + 1) n

--4.6
factorial 0 = 1
factorial n = rangeProduct 1 n
