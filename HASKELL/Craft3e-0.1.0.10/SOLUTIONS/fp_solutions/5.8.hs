--5.8
doubleAll :: [Int] -> [Int]
doubleAll x = [ 2*n | n<-x]
