--5.10
divisors :: Int -> [Int]
divisors x = [ n | n<-[1..x], (mod x n) == 0 ]

isPrime :: Int -> Bool
isPrime x
	| divisors x == [1,x] = True
	| otherwise = False	
