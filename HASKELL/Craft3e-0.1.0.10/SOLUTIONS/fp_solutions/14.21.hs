--14.21

data NTree = 	NilT |
					Node Int NTree NTree

leftTree :: NTree -> NTree
rightTree :: NTree -> NTree

leftTree NilT = NilT
leftTree (Node _ left right) = left

rightTree NilT = NilT
rightTree (Node _ left right) = right
