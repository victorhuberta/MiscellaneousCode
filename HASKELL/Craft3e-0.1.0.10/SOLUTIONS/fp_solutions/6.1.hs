--6.1
superimposeChar :: Char -> Char -> Char
superimposeChar x y
	| x == '.' && y == '.' = y
	| otherwise = '#'
