--14.23
import List

data NTree =   NilT |
               Node Int NTree NTree

collapse :: NTree -> [Int]
collapse NilT = []
collapse (Node x left right) = [x] ++ collapse left ++ collapse right

maxElem :: NTree -> Int
maxElem = 	last.
				sort.
				collapse

minElem :: NTree -> Int
minElem = 	head.
				sort.
				collapse
