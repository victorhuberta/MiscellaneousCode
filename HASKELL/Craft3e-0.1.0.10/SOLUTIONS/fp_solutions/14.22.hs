--14.22

data NTree =   NilT |
               Node Int NTree NTree

isElem :: Int -> NTree -> Bool
isElem _ NilT = False
isElem x (Node y left right)
	| x == y = True
	| otherwise = isElem x left || isElem x right
