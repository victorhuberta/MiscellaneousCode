--14.3

data Temp = Cold | Hot
   deriving (Show)
data Season = Spring | Summer | Autumn | Winter
   deriving (Eq)

weather :: Season -> Temp

-- weather function for New Zealand: The same, because their winter is 
-- hot as well

-- weather for Brazil (crossed by the Equator):
-- guess it's cold in the winter, and otherwise hot
-- don't know if that's ok (never been to Brazil ;-)
weather season
   | season == Winter = Cold
   | otherwise = Hot
