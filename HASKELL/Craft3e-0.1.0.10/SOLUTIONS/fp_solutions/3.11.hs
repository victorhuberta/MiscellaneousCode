min1 :: Int -> Int -> Int
min1 x y	| x <= y = x
			| otherwise = y

minThree x y z	= min1 x (min1 y z)
