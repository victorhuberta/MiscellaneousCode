--14.17

--import Prelude hiding( show )

-- expressions
data Expr = Lit Int |
            Add Expr Expr |
            Sub Expr Expr |
				Mult Expr Expr |
				Div Expr Expr

eval :: Expr -> Int

eval (Lit x) = x
eval (Add e1 e2) = (eval e1) + (eval e2)
eval (Sub e1 e2) = (eval e1) - (eval e2)
eval (Mult e1 e2) = (eval e1) * (eval e2)
eval (Div e1 e2) = (eval e1) `div` (eval e2)

size :: Expr -> Int
size (Lit n) = 0
size (Add e1 e2) = 1 + size e1 + size e2
size (Sub e1 e2) = 1 + size e1 + size e2
size (Mult e1 e2) = 1 + size e1 + size e2
size (Div e1 e2) = 1 + size e1 + size e2

showExpr :: Expr -> String
showExpr ( Lit x ) = show x
showExpr ( Add e1 e2 ) = "(" ++ showExpr e1 ++ "+" ++ showExpr e2 ++ ")"
showExpr ( Sub e1 e2 ) = "(" ++ showExpr e1 ++ "-" ++ showExpr e2 ++ ")"
showExpr ( Mult e1 e2 ) = "(" ++ showExpr e1 ++ "*" ++ showExpr e2 ++ ")"
showExpr ( Div e1 e2 ) = "(" ++ showExpr e1 ++ "/" ++ showExpr e2 ++ ")"
	
				
