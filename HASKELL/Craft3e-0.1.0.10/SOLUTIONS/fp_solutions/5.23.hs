--5.23
duplicate :: String -> Int -> String
duplicate x n
	| n < 1 = ""
	| otherwise = x ++ duplicate x (n -  1)
