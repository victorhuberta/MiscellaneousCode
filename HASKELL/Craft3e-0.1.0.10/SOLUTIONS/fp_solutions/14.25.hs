--14.25
import List

data NTree =   NilT |
               Node Int NTree NTree

collapse :: NTree -> [Int]
collapse NilT = []
collapse (Node x left right) = collapse left ++ [x] ++ collapse right

sort :: NTree -> [Int]
sort = (List.sort).collapse

instance Show NTree where
--	show :: NTree -> String
	show NilT = "[]"
	show (Node value left right)
		= "(" ++ show value ++ "(" ++ show left ++ show right ++ ")" ++ ")"
