--14.4
data Shape = Circle Float | Rectangle Float Float

perimeter :: Shape -> Float

perimeter (Circle r) = pi*2*r
perimeter (Rectangle x y) = 2*(x+y)
