--7.11
import List

ins :: Int -> [Int] -> [Int]
ins x [] = [x]
ins x (y:ys)
	| x >= y = x:(y:ys)
	| otherwise = y:( ins x ys )

ins2 :: Int -> [Int] -> [Int]
ins2 x [] = [x]
ins2 x (y:ys)
	| x < y = nub (x:(y:ys))
	| x == y = nub (y:ys)
	| otherwise = nub (y : ins2 x ys)
