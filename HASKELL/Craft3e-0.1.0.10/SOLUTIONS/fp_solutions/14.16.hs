--14.16

size :: Expr -> Int
size (Lit n) = 0
size (Add e1 e2) = 1 + size e1 + size e2
size (Sub e1 e2) = 1 + size e1 + size e2


-- expressions
data Expr = Lit Int |
            Add Expr Expr |
            Sub Expr Expr
