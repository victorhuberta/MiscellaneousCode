--5.1
maxOccurs :: Int->Int->(Int,Int)
maxOccurs x y
	| x > y = (x,1)
	| x < y = (y,1)
	| otherwise = (x,2)

maxThreeOccurs :: Int->Int->Int->(Int,Int)
maxThreeOccurs x y z
	| first (maxOccurs x y) > z = maxOccurs x y
	| first (maxOccurs x y)  == z = (z, (scnd (maxOccurs x y)) + 1)
	| first (maxOccurs x y) < z = (z,1)

--helper functions to get tuple elements
first :: (Int,Int)->Int
first (x,y) = x

scnd :: (Int,Int)->Int
scnd (x,y) = y
