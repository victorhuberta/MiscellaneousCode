--7.17
qSort :: [Int] -> [Int]

qSort [] = []
qSort (x:xs)
	= qSort [ y | y<-xs, y > x ] ++ [x] ++ qSort [ y | y<-xs, y < x ]

--remark: only the last comparison operator has to be changed.
