--14.6
data Shape = Circle Float | Rectangle Float Float | Triangle Float Float Float

regular :: Shape -> Bool

regular (Circle _ ) = True
regular (Rectangle x y) 
	| x == y = True
	| otherwise = False
regular (Triangle x y z)
	| x == y && x == z  = True
	| otherwise = False
