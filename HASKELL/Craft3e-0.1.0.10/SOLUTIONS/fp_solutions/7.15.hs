--7.15

--The result would be [].

import Prelude hiding(take)

take 0 _ = []
take n (x:xs)
	| n>0 = x : take (n-1) xs
take x y 
	| x<0 = error "PreludeListe.take: negative argument"
take _ [] = []
