--1.2
data Season = Spring | Summer | Autumn | Winter
   deriving (Eq,Show)

data Month = January | February | March | April | May | June | July | August | September | October | November | December
	deriving (Eq,Ord)

season :: Month -> Season
season month
	| month > September = Autumn
	| month > June = Summer
	| month > March = Spring
	| otherwise = Winter
