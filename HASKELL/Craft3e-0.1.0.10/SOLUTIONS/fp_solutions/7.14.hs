--7.14

import Prelude hiding(drop,splitAt)

drop :: Int -> [a] -> [a]
splitAt :: Int -> [a] -> ([a],[a])

drop 0 x = x
drop n [] = []
drop n (x:xs) = drop (n-1) xs

splitAt 0 x = ([],x)
splitAt n [] = ([],[])
splitAt n xs = (take n xs, drop n xs)
