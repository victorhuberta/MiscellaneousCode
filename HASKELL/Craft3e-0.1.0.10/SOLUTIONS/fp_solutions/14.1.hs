--14.1

data Temp = Cold | Hot
	deriving (Show)
data Season = Spring | Summer | Autumn | Winter
	deriving (Eq)

weather :: Season -> Temp

weather season
	| season == Summer = Hot
	| otherwise = Cold

-- Preferable: The pattern matching version. Only two thirds of the
-- number of lines ;-). Plus, the "guards" version requires season 
-- to be of type "Eq"

