--7.7
unique :: [Int] -> [Int]
unique [] = []
unique (x:xs)
	| (notElem x xs) = (x: (unique xs))
	| otherwise = unique ( deleteAll x xs)
	
--deleteAll x xs: returns list xs with all elements that equaled x deleted
deleteAll :: Eq a => a -> [a] -> [a]
deleteAll x [] = []
deleteAll x (y:ys)
	| x == y = (deleteAll x ys)
	| otherwise = (y : ( deleteAll x ys ) )
