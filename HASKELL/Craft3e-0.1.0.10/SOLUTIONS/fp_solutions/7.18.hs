--7.18
sublist :: Eq a => [a] -> [a] -> Bool

sublist [] _ = True
sublist _ [] = False
sublist (x:xs) (y:ys)
	| x == y && sublist xs ys = True
	| sublist (x:xs) ys = True
	| otherwise = False
	
subs :: Eq a => [a] -> [a] -> Bool

subs [] _ = True
subs _ [] = False
subs (x:xs) (y:ys)
	| x == y && xs == [] = True
	| ys == [] && xs /= [] = False
	| x == y && x1 == y1 && subs x1s y1s = True
	| subs (x:xs) ys = True
	| otherwise = False
		where
		(x1:x1s) = xs
		(y1:y1s) = ys
