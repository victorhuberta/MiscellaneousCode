module UsePictures where
import Pictures

whiteHorse :: Picture
whiteHorse = invertColour horse

rotateHorse :: Picture -> Picture
rotateHorse horse = flipV (flipH horse)

twoByTwo :: Picture -> Picture -> Picture -> Picture -> Picture
twoByTwo pic1 pic2 pic3 pic4 = (pic1 `beside` pic2) `above` (pic3 `beside` pic4) 

chessRow :: Picture -> Picture
chessRow twoByTwo = twoByTwo `beside` twoByTwo `beside` twoByTwo `beside` twoByTwo

chessBoard :: Picture -> Picture
chessBoard chessRow = chessRow `above` chessRow `above` chessRow `above` chessRow

chessBoardVariant1 :: Picture
chessBoardVariant1 = chessBoard (chessRow (twoByTwo horse whiteHorse whiteHorse horse))

chessBoardVariant2 :: Picture
chessBoardVariant2 = chessBoard (chessRow (twoByTwo horse whiteHorse (flipV whiteHorse) (flipV horse)))

chessBoardVariant3 :: Picture
chessBoardVariant3 = chessBoard (chessRow (twoByTwo horse whiteHorse (rotateHorse whiteHorse) (rotateHorse horse)))

chessBoardVariant4 :: Picture
chessBoardVariant4 = chessBoard (chessRow (twoByTwo horse whiteHorse (flipV (rotateHorse whiteHorse)) (flipV (rotateHorse horse))))


