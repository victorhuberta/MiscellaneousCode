noString :: [Char]
noString = "no "

oneString :: [Char]
oneString = "one "

pluralize :: [Char]-> [Int] -> [[Char]]
pluralize word counts = 
	map plural counts
	where 
		plural 0 = noString ++ word ++ "s"
		plural 1 = oneString ++ word
		plural n = show n ++ " " ++ word ++ "s"