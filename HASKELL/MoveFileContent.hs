import System.Environment (getArgs)
import Palindrome

{-|Applying a function to a file's content and move it to another -} 
moveFileContent function inputFile outputFile = do
	input <- readFile inputFile
	writeFile outputFile (function input)

main = mainWith myFunction
	where
		mainWith function = do
			args <- getArgs
			case args of
				[input, output] -> moveFileContent function input output
				_ -> error "Error: exactly two arguments are needed."
		myFunction = palindromize
