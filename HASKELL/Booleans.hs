module Booleans where
import Test.QuickCheck

exOr :: Bool -> Bool -> Bool
exOr x y = (x || y) && (not (x && y))

myNot :: Bool -> Bool
myNot True = False
myNot False = True

prop_myNot :: Bool -> Bool
prop_myNot x =
	not x == myNot x

prop_exOr :: Bool -> Bool -> Bool
prop_exOr x y =
	exOr x y == ((x || y) && (not (x && y)))

prop_exOr1 :: Bool -> Bool -> Bool
prop_exOr1 x y =
	exOr x y == (x /= y)
