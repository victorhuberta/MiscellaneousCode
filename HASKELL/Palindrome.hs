module Palindrome where
	
backwards :: [a] -> [a]
backwards xs = _backwards xs []
	where 
		_backwards :: [a] -> [a] -> [a]
		_backwards [] nl = nl
		_backwards (x:xs) nl = _backwards xs (x:nl)

palindromize :: [a] -> [a]
palindromize [] = []
palindromize xs = xs ++ (backwards xs)
			
isPalindrome :: Eq a => [a] -> Bool
isPalindrome xs 
	| (length xs) < 2 = True
	| otherwise = xs == backwards xs