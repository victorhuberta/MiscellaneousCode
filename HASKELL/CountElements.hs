countElements :: [a] -> Int
countElements [] = 0
countElements xs =
	inc c xs
	where
		c = 0
		inc n [] = n
		inc n (x:xs) = inc (n + 1) xs