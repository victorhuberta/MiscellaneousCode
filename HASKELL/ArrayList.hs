data ArrayList e = ArrayList [e]
				   deriving (Show)

data List e = Cons e (List e)
			| Nil
			deriving (Eq, Show)

data Tree a = Node a (Tree a) (Tree a)
			| Empty
			deriving (Eq, Show)

addToArrayList :: ArrayList e -> [e] -> ArrayList e
addToArrayList (ArrayList x) new = ArrayList (x ++ new) 

addToList :: List e -> e -> List e
addToList list x = Cons x (list)

addToTree :: Tree a -> a -> Bool -> Tree a
addToTree Empty new _ = Node new Empty Empty
addToTree (Node x left right) new isRight = 
	if isRight == True
	then Node x left (addToTree right new isRight)
	else Node x (addToTree left new isRight) right