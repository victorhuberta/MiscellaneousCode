meanOfList :: [Int] -> Double
meanOfList [] = 0.0
meanOfList xs =
	(fromIntegral (total s xs)) / (fromIntegral (length xs))
	where
		s = 0
		total n [] = n
		total n (x:xs) = total (n + x) xs