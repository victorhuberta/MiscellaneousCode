{-# OPTIONS_GHC -Wall #-}
module HW01 where

-- Exercise 1 -----------------------------------------

-- Get the last digit from a number
lastDigit :: Integer -> Integer
lastDigit i = i `mod` 10

-- Drop the last digit from a number
dropLastDigit :: Integer -> Integer
dropLastDigit i = (i - (i `mod` 10)) `div` 10

-- Exercise 2 -----------------------------------------

toRevDigits :: Integer -> [Integer]
toRevDigits i 
	| i <= 0 = []
	| otherwise = 
		[lastDigit i] ++ toRevDigits (dropLastDigit i)

-- Exercise 3 -----------------------------------------

-- Double every second number in a list starting on the left.
doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther [] = []
doubleEveryOther (n:[]) = [n]
doubleEveryOther (n:ns) = n:((head ns) * 2):(doubleEveryOther (tail ns))

-- Exercise 4 -----------------------------------------

-- Calculate the sum of all the digits in every Integer.
sumDigits :: [Integer] -> Integer
sumDigits ns = 
	foldl sumAcc 0 ns
	where
		sumAcc :: Integer -> Integer -> Integer
		sumAcc acc n = acc + (n `mod` 10) + (n `div` 10)

-- Exercise 5 -----------------------------------------

-- Validate a credit card number using the above functions.
luhn :: Integer -> Bool
luhn i = 
	(sumDigits (doubleEveryOther (toRevDigits i))) `mod` 10 == 0

-- Exercise 6 -----------------------------------------

-- Towers of Hanoi for three pegs
type Peg = String
type Move = (Peg, Peg)

hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi n a b c 
	| n <= 0 = []
	| otherwise = hanoi (n - 1) a c b ++ [(a,b)] ++ hanoi (n - 1) c b a 
