{-# OPTIONS_GHC -Wall #-}
module HW02 where

-- Mastermind -----------------------------------------

-- A peg can be one of six colors
data Peg = Red | Green | Blue | Yellow | Orange | Purple | Colorless
         deriving (Show, Eq, Ord)

-- A code is defined to simply be a list of Pegs
type Code = [Peg]

-- A move is constructed using a Code and two integers; the number of
-- exact matches and the number of regular matches
data Move = Move Code Int Int
          deriving (Show, Eq)

-- List containing all of the different Pegs
colors :: [Peg]
colors = [Red, Green, Blue, Yellow, Orange, Purple]

-- Defined total functions
safeHead :: [Peg] -> Peg
safeHead [] = Colorless
safeHead (c:_) = c 

-- Exercise 1 -----------------------------------------

-- Get the number of exact matches between the actual code and the guess
exactMatches :: Code -> Code -> Int
exactMatches code guess = length . filter (not . not) . zipWith (==) code $ guess

-- Exercise 2 -----------------------------------------

-- Count colors by filtering and tailing the original list of colors
countColors' :: Code -> [Peg] -> [Int]
countColors' _ [] = []
countColors' code clrs = 
	[length . filter ((==) . safeHead $ clrs) $ code] ++ (countColors' code . tail $ clrs)

-- For each peg in xs, count how many times is occurs in ys
countColors :: Code -> [Int]
countColors code = countColors' code colors

-- Count number of matches between the actual code and the guess
matches :: Code -> Code -> Int
matches code guess = sum . zipWith min (countColors code) . countColors $ guess

-- Count number of non-exact matches between the actual code and the guess
nonExactMatches :: Code -> Code -> Int
nonExactMatches code guess = (matches code guess) - (exactMatches code guess)

-- Exercise 3 -----------------------------------------

-- Construct a Move from a guess given the actual code
getMove :: Code -> Code -> Move
getMove code guess = 
	Move guess (exactMatches code guess) (nonExactMatches code guess)

-- Exercise 4 -----------------------------------------

isConsistent :: Move -> Code -> Bool
isConsistent (Move guess e ne) code = 
	(exactMatches guess code) == e && (nonExactMatches guess code) == ne 

-- Exercise 5 -----------------------------------------

filterCodes :: Move -> [Code] -> [Code]
filterCodes move codes = filter (isConsistent move) codes 

-- Exercise 6 -----------------------------------------

-- enCode :: Int -> [Peg] -> [[Peg]] -> [[Peg]]


--allCodes :: Int -> [Code]
--allCodes ln = genCode ln (take ln colors) []

-- Exercise 7 -----------------------------------------

solve :: Code -> [Move]
solve = undefined

-- Bonus ----------------------------------------------

fiveGuess :: Code -> [Move]
fiveGuess = undefined
