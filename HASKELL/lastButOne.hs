-- file: lastButOne.hs
-- lastButOne xs = take ((length xs) - 1) xs
lastButOne xs = extLastButOne [] xs	
extLastButOne ns xs = if (length xs) <= 1
				   	  then ns
				  	  else extLastButOne (ns ++ [head xs]) (tail xs)			   