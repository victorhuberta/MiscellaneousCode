sortLength :: [a] -> [a] -> Ordering
sortLength as bs 
	| (length as) > (length bs) = GT
	| (length as) < (length bs) = LT
	| otherwise = EQ