data BinaryTree a = 
	Node a (BinaryTree a) (BinaryTree a)
	| Empty
	deriving (Eq, Show)

myTree = 
	Node "x" (Node "g" (Empty) (Node "f" (Node "v" (Empty) (Empty)) (Empty))) (Node "z" (Node "a" (Empty) (Empty)) (Empty))

heightOfTree :: BinaryTree a -> Int
heightOfTree tree = 
	_heightOfTree 0 tree
	where
		_heightOfTree :: Int -> BinaryTree a -> Int
		_heightOfTree n Empty = n
		_heightOfTree n (Node a ltree rtree) 
			| lheight < rheight = rheight
			| otherwise = lheight
				where
					lheight = _heightOfTree (n + 1) ltree
					rheight = _heightOfTree (n + 1) rtree
