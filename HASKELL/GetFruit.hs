data Fruit = 
	Apple 
	| Orange
	deriving (Show)

getFruit :: String -> Fruit
getFruit f =
	case f of
		"apple" -> Apple
		"orange" -> Orange
		_ -> error "404 - Fruit not found."