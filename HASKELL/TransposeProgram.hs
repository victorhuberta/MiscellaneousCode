module TransposeProgram where 

import System.Environment (getArgs)
import TransposeStrings (transposeStrings)

repeatTranspose "" = ""
repeatTranspose ss = 
	(transposeStrings ss) ++ (repeatTranspose (unlines (tail (tail (lines ss)))))

transposeFile inputFile = do
	input <- readFile inputFile
	putStrLn (repeatTranspose input)	

main = do
	args <- getArgs
	case args of
		[input] -> transposeFile input
		_ -> error "Error: exactly one file argument is needed"
