module TransposeStrings where

composePair :: [String] -> String
composePair ss =
	[(head (head ss))] ++ [(head (head (tail ss)))] ++ "\n"
	++ transposeStrings (unlines (getTails ss))

getTails :: [String] -> [String]
getTails ss = [(tail (head ss))] ++ [(tail (head (tail ss)))]

transposeStrings :: String -> String
transposeStrings cs 
	| null cs = "" 
	| cs == "\n\n" = ""
	| otherwise = composePair (lines cs)