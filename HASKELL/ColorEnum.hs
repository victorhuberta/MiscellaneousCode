-- Color enumeration
data Color = Red
		   | Orange
		   | Yellow
		   | Green
		   | Blue
		   | Indigo
		   | Violet
		     deriving (Eq, Show)