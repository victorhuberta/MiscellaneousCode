data Direction =
	LeftTurn
	| RightTurn
	| Straight
	deriving (Eq, Show)

getDirection :: (Num a, Ord a) => (a,a) -> (a,a) -> (a,a) -> Direction
getDirection (x0, y0) (x1, y1) (x2, y2) 
	| cross < 0 = LeftTurn
 	| cross == 0 = Straight
 	| cross > 0 = RightTurn
 	where
 		v0 = (x1 - x0, y1 - y0)
 		v1 = (x2 - x1, y2 - y1)
 		cross = (fst v0) * (snd v1) - (snd v0) * (fst v1)