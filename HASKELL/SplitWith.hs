splitWith :: (a -> Bool) -> [a] -> [[a]]
splitWith _ [] = []
splitWith f xs = 
	_splitWith f [] xs
	where
		_splitWith :: (a -> Bool) -> [[a]] -> [a] -> [[a]]
		_splitWith f [[]] ys = [ys] 
		_splitWith f xs [] = xs
		_splitWith f xs ys =
			xs ++ 	let 
						(pre, suf) = (span f ys)
					in
						if not (null suf) 
						then _splitWith f [pre] (tail suf)
						else [pre]