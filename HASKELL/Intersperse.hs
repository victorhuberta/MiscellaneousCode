intersperse :: a -> [[a]] -> [a]
intersperse _ [] = []
intersperse s (x:xs) = 
	_intersperse s x xs
	where
		_intersperse :: a -> [a] -> [[a]] -> [a]
		_intersperse _ new [] = new
		_intersperse s new (x:xs) = _intersperse s (new ++ [s] ++ x) xs