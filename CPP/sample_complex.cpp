// Copyright 2016 Victor Huberta

#include <iostream>
#include "CPP/complex.h"

int main() {
    complex a{1, 2};
    complex b{2, 4};

    if (a == b)
        std::cout << "a and b is the same." << std::endl;
    else
        std::cout << "a and b is different." << std::endl;

    std::cout << "current 'a' values: real->" << a.real() << " imag->"
        << a.imag() << std::endl;
    a*=b, a+=b, a/=b;
    std::cout << "after modification: real->" << a.real() << " imag->"
        << a.imag() << std::endl;
}
