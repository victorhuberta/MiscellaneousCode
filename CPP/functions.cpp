// Copyright 2016 Victor Huberta
#include <iostream>
using std::cout;
using std::cin;
using std::endl;

namespace math_stuff {
    int64_t factorial(int n);

    template <class T, int n>
    T sum_and_add(T a, T b);

    template <class T>
    T* join_array(T a[], T b[]);
}

int main() {
    cout << "Factorial of: ";
    int n = 0;
    cin >> n;
    int64_t n_fact = math_stuff::factorial(n);
    cout << "It is " << n_fact << endl;
    cout << "SUM: " << math_stuff::sum_and_add<int, 10>(1, 2) << endl;
    cout << "SUM-2: " << math_stuff::sum_and_add<double, 50>(3, 4) << endl;

    int a[] = {1, 2, 3, 4, 5};
    int b[] = {6, 7, 8, 9, 10};
    int* c = math_stuff::join_array<int>(a, b);
    for (int i = 0; i < 10; ++i) {
        cout << c[i] << endl;
    }
    return 0;
}

int64_t math_stuff::factorial(int n) {
    return (n > 1) ? (n * factorial(n - 1)) : 1;
}

template <class T, int n>
T math_stuff::sum_and_add(T a, T b) { return a + b + n; }

template <class T>
T* math_stuff::join_array(T a[], T b[]) {
    static T c[100];
    for (int i = 0; i < 5; ++i) {
        c[i] = a[i];
        c[i + 5] = b[i];
    }
    return c;
}
