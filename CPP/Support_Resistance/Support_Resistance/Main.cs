using System;
using System.Threading;

namespace Support_Resistance
{
	class MainClass
	{
		public static void Main (string[] args)
		{

			Start:
			Console.BackgroundColor = ConsoleColor.Black;
			Console.ResetColor();
			int price, support, resistance, temp;
			string input;
			Console.Write ("Enter the price of stock : ");
			input = Console.ReadLine ();
			if (int.TryParse (input, out price)) {
				temp = price;
				Console.Write ("Enter the level of support : ");
				input = Console.ReadLine ();
				if (int.TryParse (input, out support)) {
					if (support > price) {
						Console.WriteLine ("Support cannot be more than price!");
						goto Start;
					} //if
					Console.Write ("Enter the level of resistance : ");
					input = Console.ReadLine ();
					if (int.TryParse (input, out resistance)) {
						if (resistance < price) {
							Console.WriteLine ("Resistance cannot be less than price!");
							goto Start;
						} //if
						Console.WriteLine ("Price : " + price);
						Console.WriteLine ("Support : " + support);
						Console.WriteLine ("Resistance : " + resistance);
					} //if
					else {
						Console.WriteLine ("Resistance not an integer!");
						goto Start;
					} //else
				} //if
				else {
					Console.WriteLine ("Support not an integer!");
					goto Start;
				} //else
			} //if
			else {
				Console.WriteLine ("Price not an integer!");
				goto Start;
			} //else
			int x, y, y_init_price;
			x = 5;
			y = (Console.WindowHeight / 2);
			y_init_price = y;
			while (price > support && price < resistance) {
				int i = 0;
				int total_change = 0;
				while (i <= 24) {
					Random rand = new Random ();				
					int change = rand.Next (-price/25, price/25);
					total_change += change;
					if (total_change > temp/150 || total_change < -(temp/150)) {
						if (y < 6 || y > Console.WindowHeight - 4) {
							Console.SetCursorPosition(10, Console.WindowHeight -2 );
							Console.BackgroundColor = ConsoleColor.Black;
							Console.ForegroundColor = ConsoleColor.Red;
							Console.Write ("Stock Graph Out of Bound. This bug will be fixed soon.");
							goto End;
						} //if
						draw (x, y, y_init_price, total_change);
						if (total_change > 0) 
							y -= 1;
						if (total_change < 0)
							y += 1;
						total_change = 0;
					} // if
					Console.SetCursorPosition(45, 5);
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.White;
					Console.WriteLine ("The Stock price has changed " + change + " from " + temp + " to " + (temp
					+ change) + ".");
					temp = temp + change;
					if (temp < support) {
						Console.SetCursorPosition(10, Console.WindowHeight - 2);
						Console.WriteLine ("The Stock price is below the support. Time to sell.");
						goto End;
					} //if
					else if(temp > resistance) {
						Console.SetCursorPosition(10, Console.WindowHeight - 2);
						Console.WriteLine ("The Stock price is above the resistance. Time to sell.");
						goto End;
					} //else if
					Thread.Sleep (400);
					i++;
				} //while
				if (total_change > 0) 
					y += 1;
				if (total_change < 0)
					y -= 1;
				price = temp;
				y_init_price = y;
				x += 2;
			} //while
			End:
			Console.SetCursorPosition(10, Console.WindowHeight - 1);
			Console.BackgroundColor = ConsoleColor.Black;
			Console.ForegroundColor = ConsoleColor.Red;
			Console.Write ("Exiting ...");
		} //main method

		public static void draw (int x, int y, int y_init_price, int total_change)
		{
			Console.SetCursorPosition (x, y);
			if (total_change < 0) {
				if (y < y_init_price) {
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.Red;
					Console.Write ("|");
				} //if
				else if (y > y_init_price) {
					Console.BackgroundColor = ConsoleColor.Red;
					Console.Write (" ");
				} //else if
			} // if
			else if (total_change > 0) {
				if (y < y_init_price) {
					Console.BackgroundColor = ConsoleColor.White;
					Console.Write (" ");
				} //if
				else if (y > y_init_price) {
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.White;
					Console.Write ("|");
				} //else if
			} //else if
			if (y == y_init_price) {
				if (total_change < 0)
					Console.BackgroundColor = ConsoleColor.Red;
				else if (total_change > 0)
					Console.BackgroundColor = ConsoleColor.White;
				Console.Write (" ");
			} //if
		} // method
	} //class
} //namespace
