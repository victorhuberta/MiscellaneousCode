// Copyright 2016 Victor Huberta

#include <iostream>
#include "Vector.h"

int main() {
    Vector v = {0, 0, 10, 0, 99, 0};
    v[0] = 9;

    std::cout << "Size of vector: " << v.size() << std::endl;
    for (int i = 0; i < v.size(); i++) {
        std::cout << "v[" << i << "] = " << v[i] << std::endl;
    }

    return 0;
}
