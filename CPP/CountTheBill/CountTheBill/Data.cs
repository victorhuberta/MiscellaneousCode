using System;
using System.IO;

namespace CountTheBill
{
	public class Data
	{
		StreamWriter writer = null;
		StreamReader reader = null;
		String[] _members;

		public Data (String[] members)
		{
			_members = members;
		} // constructor
		public void Update ()
		{

			try {
				writer = new StreamWriter ("members.txt", true);
				int i;
				for (i = 0; i < _members.Length; i++) {
					writer.WriteLine(_members[i]);
				} //for
				writer.Close ();
			} catch (FileErrorException fee) {
				fee.ToString ();
			} finally {
				writer.Close ();
			} // exception handling

		} // method
		public void getList ()
		{
			try {
				reader = new StreamReader ("members.txt");
				int i;
				for (i = 0; i < _members.Length; i++) {
					reader.ReadLine();
				} //for
				reader.Close ();
			} catch (FileErrorException fee) {
				fee.ToString ();
			} finally {
				reader.Close ();
			} // exception handling
		} //method
	}
	class FileErrorException : Exception {
		public override string ToString ()
		{
			return string.Format ("There has been an error encountered. Please try again.");
		}
	} //class
}

