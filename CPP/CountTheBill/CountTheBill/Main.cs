using System;

namespace CountTheBill
{
	class MainClass
	{
		public static void Main (string[] args)
		{

//			float temp, deduct;
			float total_bill;
			int total_housemates;
			int x = 0;
//			int[] deducted = new int[6];
//			int count = 6;
			float days = 0;
			total_bill = 0;
//			temp = 0;
//			deduct = 0;
			Console.WriteLine ("Input total bill : ");
			if (!(float.TryParse (Console.ReadLine (), out total_bill))) {
				Console.WriteLine ("ERROR.");
				return;
			} //if
			Console.WriteLine ("Input total housemates : ");
			if (!(int.TryParse (Console.ReadLine (), out total_housemates))) {
				Console.WriteLine ("ERROR.");
				return;
			}
			String[] input_names = new String[total_housemates];
			float[] input_days = new float[total_housemates];
			float[] bill = new float[total_housemates];
			while (x < total_housemates) {
				Console.WriteLine ("#{0} Name : ", x+1);
				input_names[x] = Console.ReadLine ();
				Console.WriteLine ("Input how many days {0} stays : ", input_names[x]);
				if (!(float.TryParse (Console.ReadLine (), out input_days [x]))) {
					Console.WriteLine ("ERROR.");
					return;
				} //if
				days += input_days [x];
				x++;
			} //while
/*
			for (x = 1; x < input.Length; x++) {
				deduct = ((input[x] - input[x-1])/(31 - input[x-1]) * total_bill);
				total_bill -= deduct;

				for (i = x; i < bill.Length; i ++) {
					bill[i] += (deduct / (bill.Length - x));
				} //for

			} //for
*/
			for (x = 0; x < input_days.Length; x++) {
/*				if ((31 - input[x]) >= 7) {
 * 					deduct = (total_bill / days) * (31-input[x]);
					temp += deduct;
					deducted[x] = 1;
					count -= 1;
					deduct = 0;
				} //if
*/
				bill[x] = (input_days[x] / days * total_bill);
			}
/*			for (x = 0; x < bill.Length; x++) {
				Console.WriteLine (bill[x]);
			} //for
*/
//			Console.WriteLine ("The Deduction is {0}. Spread it evenly...", temp);
//			temp /= count;
/*			for (x = 0; x < deducted.Length; x++) {
				if (deducted[x] == 0) {
					bill[x] += temp;
				} //if
			}
*/
			Console.WriteLine ("The Result : ");
			for (x = 0; x < bill.Length; x++) {
				Console.WriteLine ("{0}'s bill = {1}", input_names[x], bill[x]);
			} //for
			Console.ReadKey ();
		}
	}
}
