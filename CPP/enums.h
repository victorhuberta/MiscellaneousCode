#include <string>

enum class Basic_color { red, green, blue };
enum class Elaborate_color { cyan, magenta, yellow, black };
enum class Traffic_light { red, yellow, green };
Traffic_light& operator++(Traffic_light& light);
std::string toString(Basic_color& basic);
std::string toString(Elaborate_color& elaborate);
std::string toString(Traffic_light& light);
