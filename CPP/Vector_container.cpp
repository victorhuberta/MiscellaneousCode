// Copyright 2016 Victor Huberta
#include "Vector.h"

class Vector_container : public Container {
 public:
    Vector_container(std::initializer_list<double> lst) :v(lst) {}
    ~Vector_container() {}

    double& operator[](int i) { return v[i]; }
    int size() { return v.size(); }
 private:
    Vector v;
};
