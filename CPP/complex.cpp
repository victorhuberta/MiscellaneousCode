// Copyright 2016 Victor Huberta

#include "CPP/complex.h"

complex::complex(double r, double i) :re{r}, im{i} {}
complex::complex(double r) :re{r}, im{0} {}
complex::complex() :re{0}, im{0} {}

double complex::real() const { return re; }
void complex::real(double r) { re = r; }
double complex::imag() const { return im; }
void complex::imag(double i) { im = i; }

complex& complex::operator+=(complex c) {
    re += c.real(), im += c.imag(); return *this;
}
complex& complex::operator-=(complex c) {
    re -= c.real(), im -= c.imag(); return *this;
}
complex& complex::operator*=(complex c) {
    re*=c.real(), im*=c.imag(); return *this;
}
complex& complex::operator/=(complex c) {
    re/=c.real(), im/=c.imag(); return *this;
}

bool complex::operator==(complex c) {
    return re == c.real() && im == c.imag();
}
bool complex::operator!=(complex c) {
    return !(*this == c);
}
