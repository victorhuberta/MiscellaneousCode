#include <iostream>
using namespace std;

// define structure of a single linked list
struct Node {
	int data;
	Node *next;
};

// initialize the first node created in the linked list
void initNode(Node *tmpHead, int n) {
	tmpHead->data = n;
	tmpHead->next=NULL;
}

// display all nodes in the linked list
void displayNodes(Node *tmpHead) {
	Node *cur = tmpHead;
	while (cur) {
		cout << cur->data << " ";
		cur = cur->next;
	}
	cout << endl;
} 

// add a new node at the end of the linked list
void addNode(Node *tmpHead, int n){

	Node *newNode = new Node;
	newNode->data = n;
	newNode->next = NULL;
	
	Node *cur = tmpHead;
	while (cur) {
		if (cur->next == NULL){ 
			cur->next = newNode;
			break;
		}
		cur = cur->next;
	}
}

// add a new node to front of the linked list
void addFront(Node **tmpHead, int n) {

	Node *newNode = new Node;
	newNode->data = n;
	newNode->next = *tmpHead;
	*tmpHead = newNode; 
}

// remove a node at front of the linked list
void removeFront(Node **tmpHead) {

	Node *oldHead = *tmpHead;
	if (oldHead->next != NULL) {
		*tmpHead = oldHead->next;
      delete oldHead;
   }
	else
		cout << "RemoveFront() aborted!\n";   
} 

// return total nodes counted in the linked list 
int countTotalNodes(Node *tmpHead) {

	Node *cur = tmpHead;
	int count = 0;
	while (cur) {
		cout << cur->data << " ";
		cur = cur->next;
		count++;
	}
	return count;
}

// return a position of a node which content matches 
// the searched value. Position 1 for the first node.
int searchNodePos(Node *tmpHead, int n) {

	Node *cur = tmpHead;
	int pos = 1;
	while (cur) {
		if (cur->data == n)
			break;		
		pos++;
		cur = cur->next;
	}
	if (cur == NULL)
		pos = -1;
	return pos;
}

// add a new node at a given position. Position 1 is first node.
void addNodeAt(int pos, Node **tmpHead, int n) {
	
	if (( pos > (countTotalNodes(*tmpHead) + 1)) | (pos < 1))
		cout << "Invalid add position number " << pos << "!\n";
	else if (pos == 1) 	
		addFront(tmpHead, n);
	}
	else {
		Node *newNode = new Node;
		newNode->data = n;
		Node *cur = *tmpHead;
		int k = 1;
		while ((cur != NULL) & (k < (pos-1))) {
			cur = cur->next;
			k++;
		}
		newNode->next = cur->next;
		cur->next = newNode;
	}
}

// remove a node at a given position. Position 1 is first node.
void removeNodeAt(int pos, Node **tmpHead){

	if (( pos > countTotalNodes(*tmpHead)) | (pos < 1))
		cout << "Invalid remove position number " << pos << "!\n";
	else if (pos == 1) 	
		removeFront(tmpHead);
	}
	else {
		Node *cur = *tmpHead;
		int k = 1;
		while ((cur !=NULL) & (k < (pos-1))) {
			cur = cur->next;
			k++;
		}
		Node *oldNode = cur->next;
		cur->next = oldNode->next;
		delete oldNode;
		}
	}
}


void main() {
	
	Node *head = new Node;
	initNode(head, 22);
	displayNodes(head);
	
	addNode(head, 33);
	addNode(head, 44);
	displayNodes(head);
	
	addFront(&head, 11);
	displayNodes(head);
	
	removeFront(&head);
	displayNodes(head);
	
	cout << "TOTAL NODES is " << countTotalNodes(head) << endl;
	cout << "POS NODE 33 is " << searchNodePos(head, 33) << endl;

	addNodeAt(2, &head, 99 );
	displayNodes(head);

	removeNodeAt(2, &head);
	displayNodes(head);
}