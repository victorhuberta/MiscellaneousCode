#include <iostream>
#include <stdexcept>
#include "Vector.h"
using namespace std;

int main() {
    const int vector_size = -20;
    
    try {
        Vector v(vector_size);
        cout << "Assigning 99 to v[" << vector_size << "]...";
        v[vector_size] = 99;
        cout << "Assigning 101 to v[-1]...";
        v[-1] = 101;
    } catch (const out_of_range& oor) {
        cout << "Index out of range in " << oor.what() << "\n";
    } catch (const length_error& le) {
        cout << "Length error in " << le.what() << "\n";
    } catch (const bad_alloc& ba) {
        cout << "Bad memory allocation in " << ba.what() << "\n";
    }
}
