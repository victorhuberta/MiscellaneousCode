#include <iostream>

void func();

int main() {
  static int count = 10;
  for (;count > 0; --count) {
    std::cout << "Inside main: count is " << count << std::endl;
    func();
  }
}

void func() {
  static int count;
  std::cout << "Inside func: count is " << count << std::endl;
}
