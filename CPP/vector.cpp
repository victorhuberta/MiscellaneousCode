#include <iostream>
using namespace std;

struct Vector {
    int sz;
    double* elems;
};

void vector_init(Vector& v, int size);
void fill_vector(Vector v, int elems[]);
void print_vector_contents(Vector* v);

int main() {
    Vector v;
    vector_init(v, 10);
    
    int elems[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    fill_vector(v, elems);
    print_vector_contents(&v);   
}

void vector_init(Vector& v, int size) {
    v.sz = size;
    v.elems = new double[size];
}

void fill_vector(Vector v, int elems[]) {
    for (int i = 0; i < v.sz; ++i) {
        v.elems[i] = elems[i];
    }
}

void print_vector_contents(Vector* v) {
    for (int i = 0; i < v->sz; ++i) {
        cout << v->elems[i] << "\n";
    }
}
