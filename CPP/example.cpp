#include <iostream>
using namespace std;

const string owner_name {"Victor"};

int main() {
    cout << "What is your name, dear visitor?\n";
    
    string name {""};
    cin >> name;

    if (name == owner_name) {
        cout << "You are my owner, aren't you?\n";
    } else {
        cout << "Do you by any chance, know my owner?\n";
    }

    return 0;
}
