#include <iostream>
#include <fstream>

using namespace std;

int main() {
	int base_num = 0;
	int limit_num = 0;
	cout << "Input base number : ";
	cin >> base_num;
	cout << "Input limit number : ";
	cin >> limit_num;
	ofstream file;
	file.open("numbers.txt");
	while (base_num <= limit_num) {
		file << base_num << "\n";
		base_num += 1;
	}
	file.close();	
	return 0;
}
