#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

ofstream file;
string base_string;

string checkMatchedString(string word) {
	file.close();
	string line;
	ifstream read_file("strings.txt");
	while (getline(read_file, line)) {
		if (line.compare(word) == 0) {
			word = "invalid";
		}
	}
	read_file.close();
	file.open("strings.txt", ofstream::app);
	return word;
}

void generate(string word) {
	int word_size = word.length();
	string prev_word = "";
	for (int h = 0; h < word_size; h++) {
		for (int x = 0; x < word_size; x++) {
			if (x != h)
				word[x] = tolower(word[x]);
		}
		if (prev_word != word) {
			word = checkMatchedString(word);
			if (word.compare("invalid") == 0)
				return;
			file << word + "\n";
			prev_word = word;
		}
		for (int i = h; i < word_size; i++) {
			word[i] = toupper(word[i]);
			if (prev_word != word) {
				word = checkMatchedString(word);
				if (word.compare("invalid") == 0)
					return;
				file << word + "\n";
				prev_word = word;
			}
			for (int j = i + 2; j < word_size; j++) {
				word[j] = toupper(word[j]);
				word = checkMatchedString(word);
				if (word.compare("invalid") == 0)
					return;
				file << word + "\n";
				word[j] = tolower(word[j]);		
			}
		}
	}
}

string convert_o(string word) {
	for (int i = 0; i < word.length(); i++) {
		if (tolower(word[i]) == 'o')
			word[i] = '0';
	}
	return word;
}

string convert_a(string word) {
	for (int i = 0; i < word.length(); i++) {
		if (tolower(word[i]) == 'a')
			word[i] = '4';
	}
	return word;
}

string convert_e(string word) {
	for (int i = 0; i < word.length(); i++) {
		if (tolower(word[i]) == 'e')
			word[i] = '3';
	}
	return word;
}

string convert_i(string word) {
	for (int i = 0; i < word.length(); i++) {
		if (tolower(word[i]) == 'i')
			word[i] = '1';
	}
	return word;
}

void convertLetterToNum(string word) {
	int o_count = 0, a_count = 0, e_count = 0, i_count = 0;
	for (int i = 0; i < word.length(); i++) {
		if (tolower(word[i]) == 'o')
			o_count += 1;
		else if (tolower(word[i]) == 'a')
			a_count += 1;
		else if (tolower(word[i]) == 'e')
			e_count += 1;
		else if (tolower(word[i]) == 'i')
			i_count += 1;
	}
	if (o_count == 0 && a_count == 0 && e_count == 0 && i_count == 0)
		return;
	if (o_count > 0) {
		word = convert_o(word);
		generate(word);
	}
	word = base_string;
	if (a_count > 0) {
		word = convert_a(word);
		generate(word);
	}
	word = base_string;
	if (e_count > 0) {
		word = convert_e(word);
		generate(word);
	}
	word = base_string;
	if (i_count > 0) {
		word = convert_i(word);
		generate(word);
	}
	word = base_string;
	if (o_count > 0 && a_count > 0) {
		word = convert_o(word);
		word = convert_a(word);
		generate(word);
	}
	word = base_string;
	if (o_count > 0 && e_count > 0) {
		word = convert_o(word);
		word = convert_e(word);
		generate(word);
	}
	word = base_string;
	if (o_count > 0 && i_count > 0) {
		word = convert_o(word);
		word = convert_i(word);
		generate(word);
	}
	word = base_string;
	if (a_count > 0 && e_count > 0) {
		word = convert_a(word);
		word = convert_e(word);
		generate(word);
	}
	word = base_string;
	if (a_count > 0 && i_count > 0) {
		word = convert_a(word);
		word = convert_i(word);
		generate(word);
	}
	word = base_string;
	if (e_count > 0 && i_count > 0) {
		word = convert_e(word);
		word = convert_i(word);
		generate(word);
	}
	word = base_string;
	if (o_count > 0 && a_count > 0 && e_count > 0) {
		word = convert_o(word);
		word = convert_a(word);
		word = convert_e(word);
		generate(word);
	}
	word = base_string;
	if (o_count > 0 && a_count > 0 && i_count > 0) {
		word = convert_o(word);
		word = convert_a(word);
		word = convert_i(word);
		generate(word);
	}
	word = base_string;
	if (o_count > 0 && e_count > 0 && i_count > 0) {
		word = convert_o(word);
		word = convert_e(word);
		word = convert_i(word);
		generate(word);
	}
	word = base_string;
	if (a_count > 0 && e_count > 0 && i_count > 0) {
		word = convert_a(word);
		word = convert_e(word);
		word = convert_i(word);
		generate(word);
	}
	word = base_string;
	if (o_count > 0 && a_count > 0 && e_count > 0 && i_count > 0) {
		word = convert_i(word);
		word = convert_o(word);
		word = convert_a(word);
		word = convert_e(word);
		generate(word);
	}
}

int main() {
	string word = "";
	cout << "Input the base string : ";
	cin >> base_string;
	word = base_string;
	file.open("strings.txt");
	generate(word);
	convertLetterToNum(word);
	for (int i = 0; i < 100; i++) {
		stringstream stream;
		if (i < 10) {
			stream << i;
			word += "0" + stream.str();
		} else {
			stream << i;
			word += stream.str();
		}
		generate(word);
		convertLetterToNum(word);
		word = base_string;
	}
	file.close();
	return 0;
}
