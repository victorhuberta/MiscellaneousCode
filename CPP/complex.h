class complex {
    double re, im;
public:
    complex(double r, double i);
    complex(double r);
    complex();
    
    double real() const;
    void real(double r);
    double imag() const;
    void imag(double i);

    complex& operator+=(complex c);
    complex& operator-=(complex c);
    complex& operator*=(complex c);
    complex& operator/=(complex c);

    bool operator==(complex c);
    bool operator!=(complex c);
};
