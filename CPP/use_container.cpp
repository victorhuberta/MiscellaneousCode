// Copyright 2016 Victor Huberta
#include <iostream>

void use(Container* c);

int main() {
    Vector_container vc = {1, 2, 3, 4, 5};
    use(&vc);
}

void use(Container* c) {
    std::cout << "Size of container: " << c->size() << std::endl;
    for (int i = 0; i < c->size(); ++i) {
        std::cout << c[i] << std::endl;
    }
}
