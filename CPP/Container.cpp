// Copyright 2016 Victor Huberta

class Container {
    virtual double& operator[](int i) = 0;
    virtual int size() const = 0;
    virtual ~Container() {}
};
