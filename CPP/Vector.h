// Copyright 2016 Victor Huberta

#ifndef _VECTOR_H_
#define _VECTOR_H_

class Vector {
 public:
    Vector(std::initializer_list<double> lst);
    ~Vector();
    double& operator[](int i);
    int size();
 private:
    double* elems;
    int sz;
};

#endif  // _VECTOR_H_
