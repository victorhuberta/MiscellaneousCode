// Copyright 2016 Victor Huberta

#include <stdexcept>
#include <algorithm>
#include "Vector.h"

Vector::Vector(std::initializer_list<double> lst)
    :elems{new double[lst.size()]}, sz{lst.size()}
{
    std::copy(lst.begin(), lst.end(), elems);
}

Vector::~Vector() { delete[] elems; }

double& Vector::operator[](int i) {
    // this is called an invariant
    if (i < 0 || i >= size()) throw std::out_of_range("Vector::operator[]");
    return elems[i];
}

int Vector::size() {
    return sz;
}
