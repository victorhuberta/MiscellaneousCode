#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <time.h>

using namespace std;

int MAX_SIZE = 50000;

// Swap two elements
void exchange(int array[], int index1, int index2) {
	int temp = array[index2];
	array[index2] = array[index1];
	array[index1] = temp;
}

// Create a partition of smaller elements on the left side and
// bigger value elements on the right side (to use in quickSort)
int createPartition(int array[], int left, int right) {
	int pivot = array[right];
	int s_index = left - 1; //index of the smaller element
	
	for (int i = left; i <= right - 1; i++) {
		if (array[i] <= pivot) { 
		// if it is smaller or equal to pivot, move it (by swapping) to the left side
			s_index++;
			exchange(array, s_index, i);
		}
	}
	exchange(array, s_index + 1, right); // finally, move the pivot to the center of array
	return (s_index + 1); // return the index of the center element
}

void quickSort(int array[], int left, int right) {
	if (left < right) {
		// The recursion will continue as long as "left" (lowest) index
		// is smaller than "right" (highest) index
		int p = createPartition(array, left, right);
		quickSort(array, left, p - 1); // Sort the left side of the partition
		quickSort(array, p + 1, right); // Sort the right side of the partition
	}
}

void selectionSort(int list[]) {
	int min = 0; // index of the assumed smallest element
	
	for (int j = 0; j < (MAX_SIZE - 1); j++) {
		min = j; // Set the index of smallest element to the index of current element
		for (int i = j + 1; i < MAX_SIZE; i++) { // Iterate through the rest of the list
			if (list[i] < list[min]) {
			// If element is smaller than the current smallest,
			// set 'min' to the index of element
				min = i;
			}
		}
		exchange(list, min, j); // Swap the current smallest element with the current element
	}
}


/*
void insertionSort(int list[], int sorted_index) {
	if (sorted_index < (MAX_SIZE - 1)) {
		if (list[sorted_index] > list[sorted_index + 1]) {
			exchange(list, sorted_index, sorted_index + 1);
			for (int i = sorted_index; i > 0; i--) {
				if (list[i] < list[i - 1])
					exchange(list, i, i - 1);
			}
		}
		insertionSort(list, sorted_index + 1);
	}
}
*/

int main() {
	clock_t time_before, time_after;
	
	// Set up two files to store result
	ofstream qfile, sfile;
	qfile.open("quickSort_records.txt", ios::app);
	sfile.open("selectionSort_records.txt", ios::app);
	
	double sort_time = 0.0;
	
	int list[MAX_SIZE];
	for (int i = 0; i < 10; i++) {
		list[i] = rand() % MAX_SIZE;
	}
	for (int i = 0; i < MAX_SIZE; i++)
		cout << list[i] << " ";
		
	insertionSort(list, 0);
	
	cout << endl;
	for (int i = 0; i < MAX_SIZE; i++)
		cout << list[i] << " ";
	
	// Result is obtained from 10 samples of sorting,
	// with increasing max size of list and max size of random numbers	
	while (MAX_SIZE <= 450000) {
		int array[MAX_SIZE];
		int list[MAX_SIZE];
		
		srand(time(NULL));
		
		for (int i = 0; i < MAX_SIZE; i++) {
			array[i] = rand() % MAX_SIZE;
			list[i] = array[i];
		}
		
		time_before = clock();
		quickSort(array, 0, MAX_SIZE - 1);
		time_after = clock();
		sort_time = (time_after - time_before) / 1000;
		cout << "The time quickSort needs to sort an array of size " << MAX_SIZE << " is : " << sort_time << "ms." << endl;
		qfile << sort_time << endl;
		
		time_before = clock();
		selectionSort(list);
		time_after = clock();
		sort_time = (time_after - time_before) / 1000;
		cout << "The time selectionSort needs to sort the array of size " << MAX_SIZE << " is : " << sort_time << "ms." << endl;
		sfile << sort_time << endl;
		
		MAX_SIZE  =  MAX_SIZE + 10000;
	}
	
	qfile.close();
	sfile.close();
	
	return 0;
}
