// Copyright 2016 Victor Huberta
#include <iostream>
#include <sstream>
#include <string>

int main() {
    std::cout << "Please put in the price: ";

    std::string price_str;
    getline(std::cin, price_str);

    double price;
    std::stringstream(price_str) >> price;

    std::cout << "Your price is " << price << std::endl;
    std::clog << "This is a log message." << std::endl;
    std::cerr << "This is an error message!" << std::endl;

    return 0;
}
