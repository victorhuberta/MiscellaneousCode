/*
-----------------------------------------------------------
ITS60304
Assignment #1 
C Programming
Student user.name: Victor Huberta
Student ID: 0320946
-----------------------------------------------------------
*/

/* THIS PROGRAM IS SPECIFICALLY WRITTEN FOR UNIX-LIKE OPERATING SYSTEMS AND OTHER OPERATING SYSTEMS
THAT SUPPORT ANSI ESCAPE SEQUENCES */
// IT MIGHT NOT WORK AS EXPECTED IN WINDOWS OPERATING SYSTEMS.

#include <stdio.h>
#include <string.h>
#define NORMAL "\E[0m"
#define RED "\E[31m"
#define GREEN "\E[32m"
#define CYAN "\E[36m"
#define WHITE "\E[37m"
#define YELLOW "\E[33m"
#define CLEAR "\E[2J"
#define INVISIBLE "\E[8m"

void clearBuff(); //Function to clear buffer
int checkInput(); //Function to check length & format of input
const char * checkState(); //Function to check user's state according to postcode
int regProfile(); //Function to register user's profile
int checkProfile(); //Function to verify the user's profile
int updateProfile(); //Function to update user's profile
int logIn(); //Function to process the admin login
int readRecords(); //Function to read records from user.txt

struct User {
	char name[51], bday[11], number[17];
	int pcode;
}; // User structure

struct Admin {
	char username[26], password[31];
}; // Admin structure

struct User user;
struct Admin admin;

int main ()
{
//A loop to control the main application's flow
	while (1) {
		printf(CLEAR); // CLEAR the screen
		int choice = 0, response = 0; /* choice to store option chosen by user,
		 and response to store the return value of some functions */
		char ans; // stores user's answer (Y/N)
// Print the main menu	
		printf(YELLOW); // change foreground color to YELLOW
		printf("██████████████████████████████\n");
		printf(RED);
		printf("      Delivery Checker        \n");
		printf(YELLOW);
		printf("██████████████████████████████\n");
		printf(CYAN);
		printf("1. Check Malaysia State       \n");
		printf("2. Register                   \n");
		printf("3. Update Profile             \n");
		printf("4. Admin Login                \n");
		printf("5. Exit                       \n");
		printf(YELLOW);
		printf("██████████████████████████████\n");
		printf(NORMAL);
		printf(GREEN);
		printf("Please input your choice (1-5) : ");
		printf(WHITE);
		scanf("%d", &choice);
		clearBuff();
		printf("\n");
		printf(GREEN);
// Check user's choice and call the specified functions
		switch (choice) {
		case 1:
			printf("State Postcode (5 digits) : ");
			scanf("%d", &user.pcode);
			clearBuff();
			printf("\nState : %s", checkState());
			// If postcode does not exist, function checkState() returns "not found"
			if (strcmp(checkState(), "not found") == 0) 
				printf("\nPlease try again ...");
			break;
		case 2:
			printf("Name : ");
			scanf("%[^\n]", user.name);
			clearBuff();
			printf("Birthday (dd/mm/yyyy) : ");
			scanf("%s", user.bday);
			clearBuff();
			printf("Contact number : ");
			scanf("%s", user.number);
			clearBuff();
			printf("State Postcode (5 digits) : ");
			scanf("%d", &user.pcode);
			clearBuff();
// If format of inputs is not as desired, break out of the switch
			char temp_bday[11]; // temporary variable to store user.bday value
			sprintf(temp_bday, "%s", user.bday); /* store user.bday value in temp_bday 
			(to avoid strtok modifying original string) */
			
			// If input's format or length is incorrect, function checkInput() returns 1
			if (checkInput() == 1) {
				break;
			}
			sprintf(user.bday, "%s", temp_bday); // store the value back to user.bday
			printf("\nState : %s", checkState());
			// If postcode does not exist, function checkState() returns "not found"
			if (strcmp(checkState(), "not found") == 0) {
				printf("\nPlease try again ...");
				break;
			}
			response = regProfile();
			// If file user.txt cannot be opened, function regProfile() returns 1
			if (response == 1) {
				printf("\nFile user.txt cannot be opened.\n");
				break;
			}
			// If the profile has been successfully registered, function regProfile() returns 0
			else if (response == 0) {
				printf("\nProfile registered.\n");
			}
			// If a user with the same name exists in the file, function regProfile() returns -1
			else if (response == -1) {
				printf("\nProfile can't be registered. A user with the same name exists.\n");
			}
			break;
		case 3:
			printf("This option allows user to change their profile.\nContinue? (y/n) : ");
			scanf("%c", &ans);
			clearBuff();
			// If user's answer is 'y' OR 'Y', continue operation. Else, stop operation
			if (toupper(ans) != 'Y') {
				break;
			}
			printf("\n(Case Sensitive)\n");
			printf("Name : ");
			scanf("%[^\n]", user.name);
			clearBuff();
			printf("Birthday (dd/mm/yyyy) : ");
			scanf("%s", user.bday);
			clearBuff();
			/* Check if the profile with the specified user.name and birthday exists
			and stores the return value to response */
			response = checkProfile();
			/* 1 means file can't be opened. 0 means profile found.
			-1 means profile can't be found */
			if (response == 1) {
				printf("\nFile user.txt cannot be opened.\n");
			}
			else if (response == 0) {
				printf("\nProfile found. Update your profile below..\n");
				printf("Contact number : ");
				scanf("%s", user.number);
				clearBuff();
				printf("State Postcode (5 digits) : ");
				scanf("%d", &user.pcode);
				clearBuff();
				char temp_bday[11]; // temporary variable to store the user.bday value
				sprintf(temp_bday, "%s", user.bday);/* store user.bday value in temp_bday 
				(to avoid strtok modifying original string) */
				
				// If input's format or length is incorrect, function checkInput() returns 1
				if (checkInput() == 1) {
					break;
				}
				sprintf(user.bday, "%s", temp_bday); // store the value back to user.bday
				printf("\nState : %s", checkState());
				// If postcode does not exist, function checkState() returns "not found"
				if (strcmp(checkState(), "not found") == 0) {
					printf("\nPlease try again ...");
					break;
				}
				// If file can't be opened, function updateProfile() returns 1
				if (updateProfile() == 1) {
					printf("\nFile user.txt cannot be opened.\n");
					break;
				}
				else {
					printf("\nProfile updated.\n");
				}
			}
			else if (response == -1) {
				printf("\nProfile not found. Please check again.");
			}
			break;
		case 4:
			printf("Username : ");
			scanf("%s", admin.username);
			clearBuff();
			printf("Password : ");
			printf(INVISIBLE); // change foreground color to INVISIBLE (avoid security issue)
			scanf("%s", admin.password);
			clearBuff();
			printf(NORMAL); // change foreground color to NORMAL (default)
			printf(GREEN);
			// Check if username and password are correct, and stores return value to response
			response = logIn();
			/* 1 means file can't be opened. 0 means successfully logged in as admin.
			-1 means invalid username or password */
			if (response == 1) {
				printf("\nFile admin.txt cannot be opened.\n");
				break;
			}
			else if (response == -1) {
				printf("\nInvalid username or password.\n");
				break;
			} 	
			else if (response == 0) {
				printf("\nLogged in as an administrator.\n");
				printf("user.txt :\n");
				printf(NORMAL);
				printf("████████████████████████████████████████████████████████████████████\n");
				if (readRecords() == 1) { // If user.txt can't be opened, function readRecords() returns 1
					printf("\nFile user.txt cannot be opened.\n");
				}
				printf(NORMAL);
				printf("████████████████████████████████████████████████████████████████████\n");
			}
			break;
		case 5:
			printf("Are you sure you want to close the program ? (y/n) : ");
			scanf("%c", &ans);
			if (toupper(ans) != 'Y')
				break;
			else
				return 0;
		default:
			printf(RED);
			printf("%d is not in the range of options.", choice);
			break;
		}
		clearBuff();
	}
} // end main

void clearBuff() {
	char c;
	do {
		c = getchar();
	} while (c != '\n');

} // end clearBuff()

int checkInput() {
	
	int response = 0, length = 0; 
	char *token; // stores the tokenized elements of user.bday
	
	printf(RED);
	
	int i = 0; // for looping purposes
	
	/* for each char in user.name string value, check if it is a digit or symbol.
	if it is, output error message */
	for (i = 0; i <= strlen(user.name); i++) {
		if (isdigit(user.name[i]) != 0 || ispunct(user.name[i]) != 0) {
			printf("\nPlease provide us with your real name.");
			response = 1;
			break;
		}
	}
	if (strlen(user.name) > 50 || strlen(user.name) == 0) { // Check length of user.name
		printf("\nName cannot be empty or more than 50 characters.");
		response = 1;
	}
	
	if (strlen(user.bday) != 10) { // check the length of user.bday string value
		printf("\nBirthday input's length is incorrect.");
		response = 1;
	}
	
	/* for each char in user.bday string value, check if it is a letter or symbol.
	if it is, output error message */
	for (i = 0; i <= strlen(user.bday); i++) {
		if (isalpha(user.bday[i]) != 0 || (ispunct(user.bday[i]) != 0 && user.bday[i] != '/')) {
			printf("\nNo letters and symbols except '/' are allowed in the birthday input.");
			response = 1;
			break;
		}
	}
		
	token = strtok(user.bday, "/"); // tokenize the elements of user.bday with the delimiter '/'
	
	// check every element of token
	for (i = 0; token != NULL; i++) {
		switch (i) {
		case 0:
			length = 2; // the first element of token is supposed to have two characters
			if (atoi(token) > 31 || atoi(token) <= 0) { // check if the date input is out of range
				printf("\n(Date) input is wrong.");
				response = 1;
			}
			break;
		case 1:
			length = 2; // the second element of token is supposed to have two characters
			if (atoi(token) > 12 || atoi(token) <= 0) {	// check if the date input is out of range
				printf("\n(Month) input is wrong.");
				response = 1;
			}
			break;
		case 2:
			length = 4; // the third element of token is supposed to have four characters
			if (atoi(token) < 1000) { // check if the date input is out of range
				printf("\n(Year) input is wrong.");
				response = 1;
			}
			break;
		default:
			break;
		}
		// If length of the specified element of token does not equal to the supposed length, output error message
		if (strlen(token) != length) {
  			printf("\nBirthday input is in the incorrect format. Please follow the format dd/mm/yyyy.");
			response = 1;
			break;
		}
  		token = strtok(NULL, "/"); // moves to the next element of token
	}
	
	if (strlen(user.number) > 16 || strlen(user.number) < 9) { // Check length of phone user.number
		printf("\nIncorrect length of phone number.");
		response = 1;
	}
	
	/* for each char in user.number string value, check if it is a letter or symbol.
	if it is, output error message */
	for (i = 0; i <= strlen(user.number); i++) {
		if (isalpha(user.number[i]) != 0 || ispunct(user.number[i]) != 0) {
			printf("\nNo letters and symbols are allowed in the phone number input.");
			response = 1;
			break;
		}
	}

	if (user.pcode < 1000 || user.pcode > 99999) { // Check length of postcode
		printf("\nPostcode cannot be more or less than 5 digits.\n");
		response = 1;
	} 
	if (response != 1) // If response is not 1 (no error), output messages in GREEN color
		printf(GREEN);
	return response;
} // end checkInput()

const char * checkState() {
// Check postcode of the users and return a suitable state's user.name
	if ((user.pcode >= 40000 && user.pcode <= 48000) || 
		(user.pcode >= 62000 && user.pcode <= 64000))  {
		return "Selangor (Shah Alam)";
	}
	else if (user.pcode >= 20000 && user.pcode <= 24000) {
		return "Terengganu (Kuala Terengganu)";
	}
	else if (user.pcode >= 93000 && user.pcode <= 98000) {
		return "Sarawak (Kuching)";
	}
	else if (user.pcode >= 88000 && user.pcode <= 91000) {
		return "Sabah (Kota Kinabalu)";
	}
	else if ((user.pcode >= 2000 && user.pcode <= 2999) || 
			(user.pcode >= 5000 && user.pcode <= 9000)) {
		return "Kedah (Alor Setar)";
	}
	else if (user.pcode >= 15000 && user.pcode <= 18000) {
		return "Kelantan (Kota Bharu)";
	}
	else if (user.pcode >= 70000 && user.pcode <= 73000) {
		return "Negeri Sembilan (Seremban)";
	}
	else if (user.pcode >= 10000 && user.pcode <= 14000) {
		return "Pulau Pinang (George Town)";
	}
	else if (user.pcode >= 79000 && user.pcode <= 86000) {
		return "Johor (Johor Bahru)";
	}
	else if (user.pcode >= 75000 && user.pcode <= 78000) {
		return "Melaka (Melaka)";
	}
	else if (user.pcode >= 1000 && user.pcode <= 1999) {
		return "Perlis (Kangar)";
	}
	else if ((user.pcode >= 30000 && user.pcode <= 36000) ||
			(user.pcode >= 39000 && user.pcode <= 39999)) {
		return "Perak (Ipoh)";
	}
	else if (user.pcode >= 25000 && user.pcode <= 28000) {
		return "Pahang (Kuantan)";
	}
	else {
		printf(RED);
		return "not found";
	}
} // end checkState()

int regProfile() {
	FILE *ufPtr;
	char temp_number[17];
	if (( ufPtr = fopen("user.txt", "r")) == NULL) { // If user.txt can't be opened, returns 1
		printf(RED);
		return 1;
	}
	else {
		while (!feof( ufPtr)) {
			fscanf(ufPtr, "\n%*[^;];%*[^;];%[^;];&*[\n]", temp_number); // fetches data and stores it in variables 
			if (strcmp(temp_number, user.number) == 0) { // if the data fetched does not equal to the data input, returns 0
				fclose(ufPtr);
				// if the data fetched equals to the data input, returns -1
				printf(RED);
				return -1;
			}
		}
		fclose(ufPtr);
		if (( ufPtr = fopen("user.txt", "a")) == NULL) { // If user.txt can't be opened, returns 1
			printf(RED);
			return 1;
		}
		fprintf(ufPtr, "%s;%s;%s;%d\n\n\n", user.name, user.bday, user.number, user.pcode); // Write the user's data into user.txt
		fclose(ufPtr); // Close the file
		return 0;
	}
} // end regProfile()

int checkProfile() {
	FILE *ufPtr;
	char temp_name[30], temp_bday[10]; /* temporary variables to store the values of name and birthday
	fetched from user.txt */

	if (( ufPtr = fopen("user.txt", "r")) == NULL) { // If user.txt can't be opened, returns 1
		printf(RED);
		return 1;
	}
	else {
		while (!feof( ufPtr )) { // loop until the end of file is met
			fscanf(ufPtr, "\n%[^;];%[^;];%*[^\n]", temp_name, temp_bday); // fetches data and stores it in variables 
			if (strcmp( temp_name, user.name) == 0 && 
				strcmp( temp_bday, user.bday) == 0) { // if the data fetched equals to the data input, returns 0
				fclose(ufPtr);
				return 0;
			}
		}
		printf(RED);
		return -1; // if the data fetched does not equal to the data input, returns -1
	}
} // end checkProfile()

int updateProfile() {
	FILE *ufPtr;
	char temp_name[30], temp_bday[10], dump[50]; // dump[] variable is to store the rest of string that is not needed
	if (( ufPtr = fopen("user.txt", "r+w")) == NULL) { // If user.txt can't be opened, returns 1
		printf(RED);
		return 1;
	}
	else {
		while (!feof( ufPtr )) { // loop until the end of file is met
			fscanf(ufPtr, "\n%[^;];%[^;];",temp_name, temp_bday); // fetches data and stores it in variables
			 
			// if the data fetched equals to the data input, write updated data into user.txt and returns 0
			if ((strcmp( temp_name, user.name) == 0) && 
				(strcmp( temp_bday, user.bday) == 0)) { 
				fprintf(ufPtr, "%s;%d", user.number, user.pcode);
				fclose(ufPtr);
				return 0;
			} // end if
			fscanf(ufPtr, "%[^\n]", dump); // stores the rest of string to dump[]
		} // end while
	} // end if
	fclose(ufPtr);
} // end updateProfile()

int logIn() {
	FILE *afPtr;
	char temp_username[26], temp_password[31]; /* temporary variables to store the values of username and password
	fetched from admin.txt */

	if (( afPtr = fopen("admin.txt", "r")) == NULL) { // If admin.txt can't be opened, returns 1
		printf(RED);
		return 1;
	}
	else {
		while (!feof( afPtr )) { // loop until the end of file is met
			fscanf(afPtr, "\n%25[^;];%30[^\n]", temp_username, temp_password); // fetches data and stores it in variables 
			if (strcmp( temp_username, admin.username) == 0 && 
				strcmp( temp_password, admin.password) == 0) { // if the data fetched equals to the data input, returns 0
				fclose(afPtr);
				return 0;
			}
		}
		printf(RED);
		return -1; // if the data fetched does not equal to the data input, returns -1
	}
} // end logIn()

int readRecords() {
	FILE *ufPtr;
	char record[120];
	if (( ufPtr = fopen("user.txt", "r")) == NULL) { // If user.txt can't be opened, returns 1
		printf(RED);
		return 1;
	}
	else {
		while (1) {
			fscanf(ufPtr, "\n%[^\n]", record); // fetches the data and stores it in record
			if (feof( ufPtr )) // if the end of file is met, breaks from the loop
				break;
			printf("%s\n", record); // prints the data fetched
		}
	}
	fclose(ufPtr);
	return 0; // If data fetching was successful, returns 0
} // end readRecords()
