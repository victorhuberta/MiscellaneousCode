#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main() {
	ifstream file("strings.txt");
	string line;
	while (getline(file, line)) {
		cout << line << endl;
	}
	file.close();
	return 0;
}
