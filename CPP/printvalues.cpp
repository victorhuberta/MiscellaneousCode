#include <iostream>
using namespace std;

void print_numbers(int base, int limit);

int main() {
    cout << "Insert base number: ";
    
    int base = 0;
    cin >> base;

    cout << "Insert limit number: ";

    int limit = 0;
    cin >> limit;

    print_numbers(base, limit);   
}

void print_numbers(int base, int limit) {
    for (int i = base; i <= limit; i++) {
        cout << i;
        if (i < limit) cout << ", ";
        else cout << ".\n";
    }
}
