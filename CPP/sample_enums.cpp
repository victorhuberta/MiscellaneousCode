#include <iostream>
#include "enums.h"
using namespace std;

int main() {
    Basic_color basic_red = Basic_color::red;
    Elaborate_color elaborate_black = Elaborate_color::black;
    Traffic_light traffic_green = Traffic_light::green;
    Traffic_light traffic_next = ++traffic_green;
    
    cout << toString(basic_red) << "\n";
    cout << toString(elaborate_black) << "\n";
    cout << toString(traffic_green) << "\n";
    cout << toString(traffic_next) << "\n";

    return 0;
}
