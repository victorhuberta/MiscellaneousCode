// Copyright 2016 Victor Huberta
#include <iostream>
#include <string>
using std::string;

string spell_it_out(const string& name);

int main() {
    std::cout << "Give me your name: ";

    string name;
    getline(std::cin, name);

    std::cout << "Let me spell it out for you." << std::endl;
    string spelled_name = spell_it_out(name);
    std::cout << spelled_name << std::endl;

    return 0;
}

string spell_it_out(const string& name) {
    string spelled_name = "";
    for (char c : name) {
        spelled_name += c;
        if (c != name[name.length() - 1]) spelled_name += ',';
    }
    spelled_name += '.';
    return spelled_name;
}
