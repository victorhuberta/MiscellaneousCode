#include "enums.h"

using namespace std;

Traffic_light& operator++(Traffic_light& light) {
    switch (light) {
    case Traffic_light::red: return light = Traffic_light::yellow;
    case Traffic_light::yellow: return light = Traffic_light::green;
    case Traffic_light::green: return light = Traffic_light::red;
    }
}

string toString(Basic_color& basic) {
    switch (basic) {
    case Basic_color::red: return "red";
    case Basic_color::green: return "green";
    case Basic_color::blue: return "blue";
    default: return "";
    }
}

string toString(Elaborate_color& elaborate) {
    switch (elaborate) {
    case Elaborate_color::cyan: return "cyan";
    case Elaborate_color::magenta: return "magenta";
    case Elaborate_color::yellow: return "yellow";
    case Elaborate_color::black: return "black";
    default: return "";
    }
}

string toString(Traffic_light& light) {
    switch (light) {
    case Traffic_light::red: return "red light";
    case Traffic_light::yellow: return "yellow light";
    case Traffic_light::green: return "green light";
    default: return "";
    }
}
