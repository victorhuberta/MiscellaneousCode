#include <stdio.h>

struct s {
    int a;
    int b;
    int c;
};

struct s get_some_values (int a) {
    struct s rt;
    rt.a = a + 1;
    rt.b = a + 2;
    rt.c = a + 3;
    return rt;
};

void main() {
    struct s rt = get_some_values (10);
    printf ("rt.a=%d, rt.b=%d, rt.c=%d\n", rt.a, rt.b, rt.c);
}
