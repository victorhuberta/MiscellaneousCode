#include <stdio.h>

double bigger (double a, double b) {
    return ((a > b) ? a : b);
};

int main () {
    printf ("bigger=%f\n", bigger (36.76, 45.46));
};
