#include <stdio.h>

float sqrt_approx(float z)
{
    int val_int = *(int *)&z;

    val_int -= 1 << 23; /* Substract 2^m. */
    val_int >>= 1; /* Divide by 2. */
    val_int += 1 << 29; /* Add ((b + 1) / 2) * 2^m. */

    return *(float *)&val_int;
}

int main(int argc, char **argv)
{
    float res = sqrt_approx(6.25);

    printf("%f\n", res);

    return 0;
}
