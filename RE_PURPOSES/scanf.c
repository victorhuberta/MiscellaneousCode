#include <stdio.h>

int main() {
    int x;
    printf ("Enter X: ");
    if (scanf ("%d", &x) == 1) {
        printf ("You entered %d...\n", x);
    } else {
        printf ("What did you enter? Huh?\n");
    }
    return 0;
};
