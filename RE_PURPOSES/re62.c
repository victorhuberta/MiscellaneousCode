#include <stdio.h>

void function (double a[200][100], double b[200][100], double c[200][100]) {
    int e;
    int i;
    for (e = 0; e < 200; e++) {
        for (i = 0; i < 100; i++) {
            c[e][i] = a[e][i] + b[e][i];
        }
    }
}

int main() {
    double a[200][100];
    double b[200][100];
    double c[200][100];

    function (a, b, c);
    int e;
    int i;
    for (e = 0; e < 15; e++) {
        for (i = 0; i < 15; i++) {
            printf("%d\n", c[e][i]);
        }
    }
}
