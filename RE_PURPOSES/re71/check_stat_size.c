#include <sys/stat.h>
#include <stdio.h>

int main(int argc, char **argv)
{
    struct stat sb = {0};
    printf("sb.st_dev = %d\n", sizeof(sb.st_dev));
    printf("sb.st_ino = %d\n", sizeof(sb.st_ino));
    printf("sb.st_mode = %d\n", sizeof(sb.st_mode));
    printf("sb.st_nlink = %d\n", sizeof(sb.st_nlink));
    printf("sb.st_uid = %d\n", sizeof(sb.st_uid));

    return 0;
}
