#include <stdio.h>

int my_strlen (const char * str) {
    const char *eos = str;
    while ( *eos++ );
    return (eos - str - 1);
};

int main() {
    int count =  my_strlen ("hello!");
    printf ("count=%d\n", count);
    return 0;
};
