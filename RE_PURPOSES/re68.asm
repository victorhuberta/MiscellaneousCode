section .text
    global _start

    _start:
	push 123456
	call func

print:
	mov edx, 4
	mov ecx, eax
	mov ebx, 1
	mov eax, 4
	int 80h
exit:
        xor ebx, ebx
	mov eax, 1
	int 80h

func:
        push esi
        mov esi, [esp+8]
        xor ecx, ecx
        push edi
        lea edx, [ecx + 1]
	xor eax, eax
next:
	mov edi, esi
	shr edi, cl
	add ecx, 4
	and edi, 15
	imul edi, edx
	lea edx, [edx+edx*4]
	add eax, edi
	add edx, edx
	cmp ecx, 28
	jle next
	pop edi
	pop esi
	ret