#include <stdint.h>
#include <stdio.h>

uint64_t f_mul(uint64_t a, uint64_t b)
{
    return a * b;
}

int main(int argc, char **argv)
{
    printf("%lld\n", f_mul(0x1234567890, 0xABCDEFABCDEF));
    return 0;
}
