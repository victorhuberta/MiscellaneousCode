section .text
    global _start

_start:
    mov rdx, 0x6
    mov rcx, hello
    mov rbx, 1
    mov rax, 4
    int 0x80
    jmp _third

_second:
    mov rdx, 0x6
    mov rcx, hello
    mov rbx, 1
    mov rax, 4
    int 0x80

_third:
    mov rdx, 0x6
    mov rcx, hello
    mov rbx, 1
    mov rax, 4
    int 0x80

_exit:
    mov rbx, 0
    mov rax, 1
    int 0x80
section .data
hello db "Hello", 0xa
