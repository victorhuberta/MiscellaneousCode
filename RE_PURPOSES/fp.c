#include <stdio.h>

float f (float a, float b) {
    return a / 3.14 + b * 4.1;
};

int main() {
    printf ("%.2f\n", f(1.2, 3.4));
};
