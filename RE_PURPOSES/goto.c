#include <stdio.h>

int main() {
    printf ("begin\n");
    goto end;
skipme:
    printf ("skip me\n");
    goto exit;
end:
    printf ("end\n");
    goto skipme;
exit:
    return 0;
};
