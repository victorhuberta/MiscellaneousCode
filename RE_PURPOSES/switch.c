#include <stdio.h>

void f (int a) {
    switch (a) {
    case 1:
    case 2:
    case 3:
        printf ("it is a one, two, or three!\n");
        break;
    case 4:
    case 5:
    case 6:
    case 7:
        printf ("it is a four, five, six, or seven!\n");
        break;
    case 8:
    case 9:
        printf ("it is an eight or nine!\n");
        break;
    default:
        printf ("it is not in the list!\n");
        break;
    }
};

int main() {
    int a = -2;
    f (a);
    printf ("f() called.\n");
    return 0;
};
