#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <memory.h>

typedef struct Float {
    unsigned int fraction: 23;
    unsigned int exponent: 8;
    unsigned int sign: 1;
} Float;

static float f(float in)
{
    float f = in;
    Float t;

    assert(sizeof(Float) == sizeof(float));

    memcpy(&t, &f, sizeof(float));

    t.sign = 1;
    t.exponent = t.exponent + 2;

    memcpy(&f, &t, sizeof(float));

    return f;
}

int main(int argc, char **argv)
{
    printf("%f\n", f(1.234));

    return 0;
}
