const
  fs = require('fs'),
  filename = process.argv[2];

function read() {
  try {
    if (!filename) {
      throw new InvalidFilename('No filename specified');
    }

    fs.readFile(filename, function (err, data) {
      try {
        if (err) throw err;
        console.log(data.toString());
      } catch (err) {
        console.log(err.message);
      }
    });
  } catch (err) {
    console.error(err.message);
  }
}

function SimpleRead() {
  this.InvalidFilename = function (message) {
    this.code = 404;
    this.name = 'InvalidFilename';
    this.message = message || 'The specified filename is not valid';
    this.toString = function () {
      return this.code + ' - ' + this.name + ': ' + this.message;
    };
  };

  this.read = read;
}

module.exports = new SimpleRead();
