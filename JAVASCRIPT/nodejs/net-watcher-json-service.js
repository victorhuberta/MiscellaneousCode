'use strict';

const
  fs = require('fs'),
  net = require('net'),
  filename = process.argv[2];

var
  watcher = null,
  connections = [];

if (!filename) {
  throw new Error('No filename was specified');
}

var server = net.createServer(function (connection) {
  console.log('A subscriber connected.');

  // Set a timestamp to identify connection object
  connection.timestamp = Date.now();
  connections.push(connection);

  var watchingJson = {
    type: 'watching',
    filename: filename
  };

  connection.write(JSON.stringify(watchingJson) + "\n");

  // Create watcher only if it does not exist
  if (!watcher) {
    watcher = fs.watch(filename, function () {
      var changedJson = {
        type: 'changed',
        filename: filename,
        timestamp: Date.now()
      };
      for (var connection of connections) {
        connection.write(JSON.stringify(changedJson) + "\n");
      }
    });
  }

  connection.on('close', function () {
    console.log('A subscriber disconnected.');

    // Get the removeIndex by comparing timestamps
    var removeIndex = connections.map( (con) => {
      return con.timestamp;
    }).indexOf(connection.timestamp);

    connections.splice(removeIndex, 1);

    // Close and remove watcher if no more connections
    if (!connections) {
      watcher.close();
      watcher = null;
    }
  });

});

server.listen(5432, function () {
  console.log('Listening for subscribers at port 5432...');
});
