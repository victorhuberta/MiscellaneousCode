"use strict";

const
  fs = require('fs'),
  filename = process.argv[2],
  net = require('net');

var server = net.createServer(function (connection) {
  console.log('A subscriber connected.');
  connection.write("Now watching file '" + filename + "' for changes...");
  var watcher = fs.watch(filename, function () {
    connection.write("File '" + filename + "' changed: " + (new Date()).toString() + "\n");
  });

  connection.on('close', function () {
    console.log('The subscriber disconnected.');
    watcher.close();
  });
});

if (!filename) {
  throw new Error('No filename was specified');
}

// server.listen(5432, function () {
//   console.log('Listening for subscribers...');
// });

// Using Unix socket
server.listen('/tmp/watcher.sock', function () {
  console.log('Listening for subscribers...');
});
