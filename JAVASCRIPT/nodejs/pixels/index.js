'use strict';

const
  fs = require('fs'),
  getPixels = require('get-pixels');

function drawInAscii(pixels) {
  var perPixel = pixels.shape[2];
  var width = pixels.shape[0];
  var height = pixels.shape[1];
  var totalWidth = width * perPixel / 2;
  var totalHeight = height * perPixel / 2;
  var totalSize = width * height * perPixel;
  var imageString = "";
  var i = 0;
  while (i < totalSize) {
    var j = 0;
    while (j < totalWidth) {
      i += j;
      j += 1;
      var c = '';
      if (pixels.data[i] <= 15) {
        c = '#';
      } else if (pixels.data[i] <= 30) {
        c = '@';
      } else if (pixels.data[i] <= 45) {
        c = '%';
      } else if (pixels.data[i] <= 63) {
        c = '&';
      } else if (pixels.data[i] <= 126) {
        c = 'M';
      } else if (pixels.data[i] <= 189) {
        c = 'i';
      } else if (pixels.data[i] <= 255) {
        c = ' ';
      }
      imageString += c;
    }
    imageString += "\n";
  }

  fs.writeFile('result.txt', imageString, (err) => {
    if (err) throw err;
  });

}

getPixels('batman.png', (err, pixels) => {
  if (err) throw err;
  drawInAscii(pixels);
});
