const
  fs = require('fs'),
  filename = process.argv[2],
  simpleRead = require('./simple-read'),
  InvalidFilename = simpleRead.InvalidFilename,
  readFile = simpleRead.read;

try {
  if (!filename) {  
    throw new InvalidFilename('No filename specified');
  }

  fs.writeFile(filename, 'This is the replacement text\n', function (err) {
    if (err) throw err;
    console.log('replacement text has been written');
    readFile();
  });
} catch (err) {
  console.error(err.message);
}
