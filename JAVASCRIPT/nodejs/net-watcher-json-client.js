const net = require('net');

var client = net.connect({ port: 5432 });

client.on('data', function (data) {
  var obj = JSON.parse(data);
  switch (obj.type) {
    case 'watching':
      console.log("File '" + obj.filename + "' is being watched for changes...");
      break;
    case 'changed':
      console.log("File '" + obj.filename + "' changed: " + new Date(obj.timestamp));
      break;
    default:
      throw new Error('Unrecognized message type: ' + obj.type);
  }
});
