var express = require('express');
var router = express.Router();
var categories = require('../categories.json');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
  	title: 'Sample App',
  	categories: categories
  });
});

router.get('/about', function(req, res, next) {
	res.render('about', {
		title: 'About',
		name: 'Victor',
		petNum: 5 
	});
});

module.exports = router;
