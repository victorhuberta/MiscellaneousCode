#!/usr/bin/node
"use strict";
const
  fs = require('fs'),
  simpleRead = require('./simple-read'),
  filename = process.argv[2];

if (!filename) {
  throw new simpleRead.InvalidFilename();
}

var stream = fs.createReadStream(filename);
stream.on('data', function (chunk) {
  process.stdout.write(chunk);
});
stream.on('error', function (err) {
  process.stdout.write(err.message + '\n');
});
