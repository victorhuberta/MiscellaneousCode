const zmq = require('zmq');

function ZmqWatcherSubscriber() {
  this.main = () => {
    const subscriber = zmq.socket('sub');
    subscriber.subscribe('');
    subscriber.on('message', (data) => {
      var message = JSON.parse(data);
      console.log(`File '${message.filename}' changed at ${new Date(message.timestamp)}`);
    });
    subscriber.connect('tcp://localhost:5432');
    console.log('Connected to localhost:5432...');
  };
}

const zmqWatcherSubscriber = new ZmqWatcherSubscriber();
module.exports = zmqWatcherSubscriber;
if (require.main === module) zmqWatcherSubscriber.main();
