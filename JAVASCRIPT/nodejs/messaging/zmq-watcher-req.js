const
  fs = require('fs'),
  zmq = require('zmq'),
  filename = process.argv[2];

function ZmqWatcherReq() {
  this.main = () => {
    if (!filename) {
      console.error('No filename was specified.');
      process.exit(1);
    }

    var requester = zmq.socket('req');

    requester.on('message', (data) => {
      var response = JSON.parse(data);
      console.log('Received response: ', response);
    });

    requester.connect('tcp://127.0.0.1:5433');
    console.log('Connected to localhost:5433...');

    for (var i=0; i < 3; i++) {
      console.log('Sending request for ' + filename);
      requester.send(JSON.stringify({
        path: filename
      }));
    }
  };
}

const zmqWatcherReq = new ZmqWatcherReq();
module.exports = zmqWatcherReq;
if (require.main === module) zmqWatcherReq.main();
