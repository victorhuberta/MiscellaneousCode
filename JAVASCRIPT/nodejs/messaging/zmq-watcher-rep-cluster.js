'use strict';

const
  fs = require('fs'),
  zmq = require('zmq'),
  cluster = require('cluster');

function ZmqWatcherRepCluster() {
  this.sendError = (responder, err) => {
    console.log(err.message);
    console.log('Sending error message to client...');
    responder.send(JSON.stringify({
      type: 'error',
      message: err.message,
      timestamp: Date.now(),
      pid: process.pid
    }));
  };

  this.setupClusterAndFork = (cluster) => {
    // Setup cluster events and fork workers
    cluster.on('online', (worker) => {
      console.log('Worker with pid:' + worker.process.pid + ' is online.');
    });

    cluster.on('exit', (worker, code, signal) => {
      console.log('Worker with pid:' + worker.process.pid + ' is offline.');
      console.log('CODE = ' + code + ' & SIG = ' + signal);
      console.log('Raising a new worker...');
      cluster.fork();
    });

    for (let i = 0; i < 3; i++) {
      cluster.fork();
    }
  };

  this.setupRouterAndDealer = (router, dealer) => {
    // Setup the router and dealer cycle
    router.bind('tcp://127.0.0.1:5433');
    dealer.bind('ipc://filer-dealer.ipc');

    router.on('message', function () {
      let frames = Array.prototype.slice.call(arguments);
      console.log('Router reports: frames received = ' + frames);
      dealer.send(frames);
    });

    dealer.on('message', function () {
      let frames = Array.prototype.slice.call(arguments);
      console.log('Dealer reports: frames received = ' + frames);
      router.send(frames);
    });
  };

  this.work = () => {
    // Create a responder, read a file, and send it to dealer
    var responder = zmq.socket('rep');

    responder.connect('ipc://filer-dealer.ipc');

    responder.on('message', (frames) => {
      let message = JSON.parse(frames);
      console.log('Received request to get: ' + message.path);

      fs.readFile(message.path, (err, content) => {
        if (err) {
          this.sendError(responder, err);
        } else {
          console.log('Sending content of ' + message.path + '...');
          responder.send(JSON.stringify({
            type: 'success',
            content: content.toString(),
            timestamp: Date.now(),
            pid: process.pid
          }));
        }
      });
    });

    process.on('SIGTERM', () => {
      console.log('Closing worker ' + process.pid + ' connections...');
      responder.close();
      process.exit(0);
    });
  };

  this.cleanUpAndExit = (router, dealer, cluster) => {
    console.log('Closing connections on master process...');
    router.close();
    dealer.close();
    console.log('Disconnecting itself and workers...');
    cluster.disconnect();
    process.exit(0);
  };

  this.main = () => {
    /**
     * If this process is the cluster master, setup router and dealer
     * + fork new workers. Else, do the reply work with 'rep' socket.
     *
     */
    if (cluster.isMaster) {
      var router = zmq.socket('router');
      var dealer = zmq.socket('dealer');

      this.setupRouterAndDealer(router, dealer);
      this.setupClusterAndFork(cluster);

      process.on('SIGINT', () => {
        this.cleanUpAndExit(router, dealer, cluster);
      });

      process.on('SIGTERM', () => {
        this.cleanUpAndExit(router, dealer, cluster);
      });

      process.on('uncaughtException', (err) => {
        console.error(err.message);
        process.exit(0);
      });
    } else {
      this.work();
    }
  };
}

const zmqWatcherRepCluster = new ZmqWatcherRepCluster();
module.exports = zmqWatcherRepCluster;
if (require.main === module) zmqWatcherRepCluster.main();
