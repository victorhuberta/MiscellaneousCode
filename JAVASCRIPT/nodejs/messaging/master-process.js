'use strict';

const
  zmq = require('zmq'),
  worker = require('./worker-process'),
  Job = require('./job');

function MasterProcess() {
  var pusher, puller;

  this.sendJobs = () => {
    const jobs = [
      (new Job('read', 'target.txt')),
      (new Job('write', 'target2.txt', 'sick awesome turtle\n'))
    ];
    for (let job of jobs) {
      console.log('Sending job to supervisor...');
      pusher.send(JSON.stringify(job));
    }
  };

  this.setup = () => {
    pusher = zmq.socket('push').bind('ipc://instructions.ipc');
    puller = zmq.socket('pull').bind('ipc://reports.ipc');

    puller.on('message', (data) => {
      let message = JSON.parse(data);
      switch (message.type) {
        case 'ready':
          this.sendJobs();
          break;
        case 'result':
          console.log('Received result: ', message);
          break;
        default:
          console.log('Unrecognized type. Master is ignoring it!');
      }
    });
  };

  this.main = () => {
    this.setup();
    worker.work();
  };
}

const masterProcess = new MasterProcess();
module.exports = masterProcess ;
if (require.main === module) masterProcess.main();
