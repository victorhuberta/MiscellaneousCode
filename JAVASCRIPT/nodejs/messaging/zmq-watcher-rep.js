const
  fs = require('fs'),
  zmq = require('zmq');

function ZmqWatcherRep() {
  this.main = () => {
    var reqNum = 0;
    var responder = zmq.socket('rep');
    responder.on('message', (data) => {
      var request = JSON.parse(data);
      if (request.path) {
        console.log('Received request to get: ' + request.path);
        fs.readFile(request.path, (err, content) => {
          if (err) {
            console.error(err.message);
            process.exit(1);
          }
          console.log('Sending content -> ' + content);
          responder.send(JSON.stringify({
            content: content.toString(),
            timestamp: Date.now(),
            pid: process.id
          }));
        });
        reqNum += 1;
      } else {
        console.log('Received request, but no file path.');
      }
      console.log('Valid requests handled: ' + reqNum);
    });

    responder.bind('tcp://127.0.0.1:5433', (err) => {
      if (err) {
        console.error(err.message);
        process.exit(1);
      }
      console.log('Listening at localhost:5432...');
    });

    process.on('SIGINT', () => {
      console.log('Closing connections...');
      responder.close();
    });
  };
}

const zmqWatcherRep = new ZmqWatcherRep();
module.exports = zmqWatcherRep;
if (!module.parent) zmqWatcherRep.main();
