const
  fs = require('fs'),
  zmq = require('zmq');

function ZmqWatcherPublisher() {
  this.main = () => {
    const
      filename = process.argv[2],
      publisher = zmq.socket('pub');
    try {
      if (!filename) throw new Error('No filename was specified');
      fs.watch(filename, () => {
        publisher.send(JSON.stringify({
          type: 'changed',
          filename: filename,
          timestamp: Date.now()
        }));
      });
    } catch (err) {
      console.error(err.message);
      process.exit(1);
    }

    publisher.bind('tcp://*:5432', (err) => {
      if (err) {
        console.error(err.message);
        process.exit(1);
      }
      console.log('Listening for subscribers at TCP port 5432...');
    });
  };
}

const zmqWatcherPublisher = new ZmqWatcherPublisher();
module.exports = zmqWatcherPublisher;
if (!module.parent) zmqWatcherPublisher.main();
