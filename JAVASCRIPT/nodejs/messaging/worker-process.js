'use strict';

const
  fs = require('fs'),
  zmq = require('zmq'),
  cluster = require('cluster');

function WorkerProcess() {
  var pusher, puller;

  this.raiseWorkers = () => {
    cluster.on('online', (worker) => {
      console.log('Worker ' + worker.process.pid + ' is online.');
    });

    cluster.on('exit', (worker, code, signal) => {
      console.log('Worker ' + worker.process.pid + ' is offline.');
      console.log('CODE = ' + code + ' & SIGNAL = ' + signal);
    });

    for (let i = 0; i < 3; i++) {
      cluster.fork();
    }
  };

  this.ready = () => {
    pusher = zmq.socket('push').connect('ipc://reports.ipc');
    puller = zmq.socket('pull').connect('ipc://instructions.ipc');

    pusher.send(JSON.stringify({
      type: 'ready'
    }));
  };

  this.startWorking = () => {
    puller.on('message', (data) => {
      console.log('A new job is coming in!');
      let message = JSON.parse(data);
      if (message.path && message.instruction && message.type === 'job') {
        this.doJob(message);
      }
    });
  };

  this.finish = (instruction, success, output) => {
    console.log('Worker ' + process.pid +
      ' is finished with a job. SUCCESS = ' + success);
    console.log('Sending the result...');
    if (output) {
      output = output.toString();
    }
    pusher.send(JSON.stringify({
      type: 'result',
      instruction,
      success,
      output
    }));
  };

  this.doJob = (job) => {
    switch (job.instruction) {
      case 'read':
        console.log('Worker ' + process.pid + ' is working on a READ job...');
        fs.readFile(job.path, (err, content) => {
          if (err) this.finish('read', false, err);
          this.finish('read', true, content);
        });
        break;
      case 'write':
        console.log('Worker ' + process.pid + ' is working on a WRITE job...');
        fs.writeFile(job.path, job.content, (err) => {
          if (err) this.finish('write', false, err);
          this.finish('write', true);
        });
        break;
      default:
        console.log('Unrecognized instruction. Workers are ignoring it!');
    }
  };

  this.work = () => {
    if (cluster.isMaster) {
      this.raiseWorkers();
    } else {
      this.ready();
      this.startWorking();
    }
  };
}

module.exports = new WorkerProcess();
