function Job(instruction, path, content) {
  const instructions = ['read', 'write'];

  this.type = 'job';

  if (instructions.indexOf(instruction) !== -1) {
    this.instruction = instruction;
  } else {
    throw new Error('Invalid instruction.');
  }

  this.path = path;

  if (this.instruction === 'write') {
    this.content = content || '';
  }
}

module.exports = Job;
