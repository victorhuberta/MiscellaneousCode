"use strict";

const
  fs = require('fs'),
  filename = process.argv[2],
  spawn = require('child_process').spawn;

try {
  if (!filename) {
    throw new Error('A file to watch must be provided.');
  }

  fs.watch(filename, function () {
    console.log('File ' + filename + ' just changed!');
    let
      ls = spawn('ls', ['-lh', filename]),
      output = '';
    ls.stdout.on('data', function (chunk) {
      output += chunk.toString();
    });
    ls.on('close', function () {
      let splitted = output.split(/\s+/);
      console.dir([splitted[0], splitted[4], splitted[8]]);
    });
  });
  console.log('Now watching ' + filename + ' for changes...');

} catch (err) {
  console.error(err.message);
}
