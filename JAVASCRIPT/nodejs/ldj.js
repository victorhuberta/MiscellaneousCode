const
  events = require('events'),
  util = require('util'),
  LdjClient = function (stream) {
    events.EventEmitter.call(this);
    var self = this;
    var buffer = '';
    self.checkMessage = function (message, callback) {
      var err = new Error('Invalid message');

      if (message.type === 'changed') {
        if (!(message.filename && message.timestamp)) callback(err, null);
      } else {
        if (!(message.type && message.filename)) callback(err, null);
      }

      callback(null, message);
    };

    stream.on('data', function (data) {
      buffer += data;
      var delimiter = buffer.indexOf('\n');
      if (delimiter !== -1) {
        // Split between input and buffer
        var input = buffer.substr(0, delimiter);
        buffer = buffer.substr(delimiter + 1);
        try {
          var message = JSON.parse(input);

          // Check for invalid message
          self.checkMessage(message, function (err, message) {
            if (err) throw err;
            self.emit('message', message);
          });
        } catch (err) {
          self.emit('error', err);
        }
      }
    });
  };

util.inherits(LdjClient, events.EventEmitter);

module.exports = {
  LdjClient: LdjClient,
  connect: function connect(stream) {
    return new LdjClient(stream);
  }
};
