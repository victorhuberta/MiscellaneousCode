const
  net = require('net'),
  ldj = require('./ldj');

var client = net.connect({ port: 5432 });
var ldjClient = ldj.connect(client);

ldjClient.on('message', function (obj) {
  switch (obj.type) {
    case 'watching':
      console.log("File '" + obj.filename + "' is being watched for changes...");
      break;
    case 'changed':
      console.log("File '" + obj.filename + "' changed: " + new Date(obj.timestamp));
      break;
    default:
      throw new Error('Unrecognized message type: ' + obj.type);
  }
});

ldjClient.on('error', function (err) {
  console.error(err.message);
  console.log('Moving on...');
});
