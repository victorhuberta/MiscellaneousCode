function Players() {
	this.Player = function Player(name) {
		this.life = 100;
		this.melee = 4;
		this.magic = 2;
		this.stats = function stats() {
			return  "\nName: " + this +
					"\nLife: " + this.life +
					"\nMelee: " + this.melee +
					"\nMagic: " + this.magic + "\n";
		};
		this.toString = function toString() {
			return name;
		};
	}
}

module.exports = new Players();