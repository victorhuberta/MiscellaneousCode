function getDamage(player) {
	return player.magic + player.melee;
}

function Actions() {
	this.setActions = function setActions(player) {

		player.attack = function attack(target) {
			target.life -= getDamage(player);
			console.log(`${player} attacked ${target} furiously. Damage: ${getDamage(player)}`);
		};
		player.heal = function heal(target) {
			var amount = this.magic * 0.6;
			target.life += amount;
			console.log(`${player} healed ${target} by ${amount} life.`);
		};

	}
}

module.exports = new Actions();