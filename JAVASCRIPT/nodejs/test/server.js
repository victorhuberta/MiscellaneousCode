var connect = require('connect');
var http = require('http');
var fs = require('fs');

var app = connect();

function give(response) {
	return {
		html: function html(name) {
			fs.createReadStream('./public/' + name + '.html').pipe(response);
		}
	};
}

function logger(req, res, next) {
	console.log(req.method, req.url);
	next();
}

function index(req, res) {
	give(res).html('index');
}

function about(req, res) {
	give(res).html('about');
}

function validateAccount(req, res, next) {
	console.log(req.url);
	if (req.url == '/') {
		next();
	} else {
		res.setHeader('Content-Type', 'text/plain');
		res.end('Account can\'t be validated.');
	}
}

app.use(logger);
app.use('/about', validateAccount);
app.use('/about', about);
app.use('/', index);

app.listen(8000);
console.log('Server is now running on port 8000...');