var fs = require('fs');
var path = require('path');
var players = require('./players');
var actions = require('./actions');

var victor = new players.Player('Victor');
var daniel = new players.Player('Daniel');

victor.life += 500;

actions.setActions(victor);
actions.setActions(daniel);

function writeAndReadFrom(filePath, content) {
	fs.writeFileSync(path.basename(filePath), content);
	console.log(fs.readFileSync(path.basename(filePath)).toString());
}

function victorAttacksDaniel() {
	victor.attack(daniel);
	writeAndReadFrom(danielPath, daniel.stats());
}

function danielHealsVictor() {
	daniel.heal(victor);
	writeAndReadFrom(victorPath, victor.stats());
}

var victorPath = __dirname + '/victor_stats.txt';
var danielPath = __dirname + '/daniel_stats.txt';

setInterval(victorAttacksDaniel, 3000);
setInterval(danielHealsVictor, 5000);

