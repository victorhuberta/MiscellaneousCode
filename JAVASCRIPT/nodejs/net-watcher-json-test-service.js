const net = require('net');

var server = net.createServer(function (connection) {
  console.log('A subscriber connected.');
  connection.write('{"type":"watching", "filename":"targ');
  var timer = setTimeout(function () {
    connection.write('et.txt"}\n');
    connection.end();
  }, 2000);

  connection.on('end', function () {
    clearTimeout(timer);
    console.log('A subscriber disconnected.');
  });
}).listen(5432, function () {
  console.log('Listening for subscribers at port 5432...');
});
