var a = 12;

var b = function () {
	return 4;
}

var myAwesomeFunction = function (a) {
	return a* b();
}

var c = myAwesomeFunction(a);

if (c === 48) {
	console.log("wow!");
} else {
	console.log("awww... :(");
}

var text = (c > 30) ? "wow!" : "aww... :(";

//Declaring object
var myProfile = {
	name: "John Smith",
	email: "johnsmith@gmail.com",
	'zip code': 12345,
	isInvited: true;
}

var myString = "victor";
myString.replace("i", "X");

var myArray = [123, 456];
myArray.push(789);
