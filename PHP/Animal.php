<?php
	abstract class Animal {

		const CREATOR = 'Nature';
		public static $number = 0;

		public function __construct($name) {
			$this->name = $name;
			self::$number++;
		}

		public function __destruct() {
			print 'An animal called ' . $this . 
				  ' is being destroyed.' . "\n";
		}

		public abstract function makeSound();

		public function getName() {
			return $this->name;
		}

		public function setName($name) {
			$this->name = $name;
		}

		public function createdBy() {
			print 'This animal is created by ' . self::CREATOR . "\n";
		}

		public function __toString() {
			return $this->name;
		}

		protected $name;
	}