<?php

	function autoloadClass($className) {
		require $_SERVER['DOCUMENT_ROOT'] . $className . '.php';
	}

	spl_autoload_register('autoloadClass');
	
	interface Edible {
		function isTasted();
	}

	class KillException extends Exception {
		function __construct($message) {
			parent::__construct($message);
		}
	}


	class Cat extends Animal {

		public function __construct($name, $age) {
			parent::__construct($name);
			$this->age = $age;
		}

		public function __destruct() {
			print 'A cat called ' . $this .
				  ' is being killed.' . "\n";
		}

		public function makeSound() {
			print parent::getName() . ' is making sound... MEOW!' . "\n";
		}

		public function getAge() {
			return $this->age;
		}

		public function setAge($age) {
			$this->age = $age;
		}

		public function createdBy() {
			parent::createdBy();
			print 'But the ' . parent::CREATOR . 
				  ' DEMON also contributed.' . "\n";
		}

		private $age;

	}

	class Chicken extends Animal implements Edible {

		public function __construct($name) {
			parent::__construct($name);
		}

		public function __destruct() {
			print 'A chicken called ' . $this .
				  ' is being killed.' . "\n";
		}

		function isTasted() {
			return 'Good';
		}

		public function makeSound() {
			print $this . ' is making sound... PTOK PTOK PTOKKK!' . "\n";
		}
	}

	function killWithCuriosity(Animal $animal) {
		if ($animal instanceof Cat) {
			print 'Curiosity kills ' . $animal . "\n";
		} else {
			throw new KillException('Curiosity can\'t kill ' . $animal);
		}
	}

	$cat = new Cat("Melody", 10);
	print 'Name : ' . $cat . "\n";
	$cat->makeSound();
	print $cat->getName() . ' is ' . $cat->getAge() . ' year-old.' . "\n";
	$cat->createdBy();

	$chicken = new Chicken("Penang");
	print 'Name : ' . $chicken . "\n";
	$chicken->makeSound();
	$chicken->createdBy();
	$chicken->isTasted();

	try {
		killWithCuriosity($cat);
		killWithCuriosity($chicken);
		killWithCuriosity(new KillException('I am a Cat. Trust me!'));
	} catch (KilLException $exception) {
		print $exception->getMessage() . ' in file ' . 
			  $exception->getFile() . ' on line ' . 
			  $exception->getLine() . "\n";
	}