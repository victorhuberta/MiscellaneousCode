<html>
<head>
	<title>Thank you for registering</title>
</head>
<body>
	<?php
		$params = array('email', 'first_name', 'last_name', 'password');
		$error_msg = '';
		foreach ($params as $param) {
			if (empty($_POST[$param])) {
				$error_msg .= ' ERROR: ' . $param . ' is not set.';
			}
		}

		if (!empty($error_msg)) {
			die($error_msg);
		}

		$clean_email = strip_tags($_POST['email']);
		$clean_fname = strip_tags($_POST['first_name']);
		$clean_lname = strip_tags($_POST['last_name']);
		$clean_pass = strip_tags($_POST['password']);
		echo 'E-mail Address : ' . $clean_email . '<br/>';
		echo 'First Name : ' . $clean_fname . '<br/>';
		echo 'Last Name : ' . $clean_lname . '<br/>';
		echo 'Password : ' . $clean_pass . '<br/>';
	?>
</body>
</html>