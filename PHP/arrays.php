<?php
	$people = array("Victor" => "Programmer", "Daniel" => "Designer", "Vincent" => "Entrepreneur");

	print_r($people);

	foreach($people as $name => $job) {
		print "$name is a $job\n";
	}
	
 	foreach($people as $name => & $job) {
		if ($job == "Designer") {
			$job = "Programmer";
		}
		print "$name is a $job\n";
	}

	print_r($people);

	$pokemons[] = "Blastoise";
	$pokemons[] = "Snorlax";
	$pokemons[] = "Pikachu";

	print_r($pokemons);

	reset($pokemons);

	while (list($key, $value) = each($pokemons)) {
		print "#$key = $value\n";
	}