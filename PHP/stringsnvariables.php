<?php
	require_once $_SERVER['DOCUMENT_ROOT'] . 'arrays.php';

	define("UP", 0);
	define("RIGHT", 1);
	define("DOWN", 2);
	define("LEFT", 3);
	define("MIDDLE", 4);
	define("SURPRISE", 12345);
	

	function printUP() {
		print UP . "\n";
	}

	$arrows = array(DOWN, RIGHT, RIGHT, MIDDLE, UP, LEFT, SURPRISE, DOWN);
	$string = "hello";
	$index = 0;
	$counter =& $index;

	foreach($arrows as $arrow) {
		switch($arrow) {
			case UP:
				print "It is going uphill.\n";
				$string++;
				break;
			case RIGHT:
				print "It is going righthill.\n";
				$index++;
				break;
			case DOWN:
				print "It is going downhill.\n";
				$string--;
				break;
			case LEFT:
				print "It is going lefthill.\n";
				$counter--;
				break;
			default:
				print "It is not going anywhere.\n";
				break;
		}

		print 'String: ' . $string . ' ; Index: ' . $index . ' ; Counter: ' . $counter . "\n";

		if (SURPRISE === $arrow):
			print "OMG ! WHAT A SURPRISEEEE!\n";
			print "PLEASE DO MORE.\n";
		else:
			print "meh, no surprise there...\n";
		endif;
	}

	printUP();