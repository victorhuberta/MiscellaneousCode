<?php
	class Location {
		var $x, $y, $data;

		function __construct(int $x, int $y, array $data) {
			$this->x = $x;
			$this->y = $y;
			$this->data = $data;
		}
	}