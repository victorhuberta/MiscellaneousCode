<?php
	require_once $_SERVER['DOCUMENT_ROOT'] . '/Location.php';

	class BoundingBox {
		$locations;
		$minX, $minY, $maxX, $maxY;
		var $northWest, $northEast, $southEast, $southWest;

		function __construct($minX, $minY, $maxX, $maxY) {
			$this->minX = $minX;
			$this->minY = $minY;
			$this->maxX = $maxX;
			$this->maxY = $maxY;
			$this->locations = array();
		}

		function inNorthWest($location) {
			return $location->x <= $midX && $location->y >= $midY;
		}

		function inNorthEast($location) {
			return $location->x >= $midX && $location->y >= $midY;
		}

		function inSouthEast($location) {
			return $location->x >= $midX && $location->y <= $midY;
		}

		function inSouthWest($location) {
			return $location->x <= $midX && $location->y <= $midY;
		}

		function addLocation(Location $location) {
			$minX = $this->minX;
			$minY = $this->minY;
			$maxX = $this->maxX;
			$maxY = $this->maxY;
			$locations =& $this->locations;

			$midX = ($minX + $maxX) / 2.0;
			$midY = ($minY + $maxY) / 2.0;

			if ($locations->isEmpty()) {
				array_push($locations, $location);
				return;
			} else {
				if (inNorthWest($location)) {
					var $new_minX = $minX;
					var $new_minY = $midY;
					var $new_maxX = $midX;
					var $new_maxY = $maxY;

					$this->northWest = BoundingBox($new_minX, $new_minY, 
										$new_maxX, $new_maxY);

					foreach $locations as $location {
						if (inNorthWest($location)) {
							$this->northWest->addLocation($location);
						}
					}

					$this->northWest->addLocation($location);

				} else if (inNorthEast($location)) {
					var $new_minX = $midX;
					var $new_minY = $midY;
					var $new_maxX = $maxX;
					var $new_maxY = $maxY;

					$this->northEast = BoundingBox($new_minX, $new_minY, 
										$new_maxX, $new_maxY);

					foreach $locations as $location {
						if (inNorthEast($location)) {
							$this->northEast->addLocation($location);
						}
					}

					$this->northEast->addLocation($location);

				} else if (inSouthEast($location)) {
					var $new_minX = $midX;
					var $new_minY = $minY;
					var $new_maxX = $maxX;
					var $new_maxY = $midY;

					$this->southEast = BoundingBox($new_minX, $new_minY, 
										$new_maxX, $new_maxY);

					foreach $locations as $location {
						if (inSouthEast($location)) {
							$this->southEast->addLocation($location);
						}
					}

					$this->southEast->addLocation($location);

				} else if (inSouthWest($location)){
					var $new_minX = $minX;
					var $new_minY = $minY;
					var $new_maxX = $midX;
					var $new_maxY = $midY;

					$this->southWest = BoundingBox($new_minX, $new_minY, 
										$new_maxX, $new_maxY);

					foreach $locations as $location {
						if (inSouthWest($location)) {
							$this->northWest->addLocation($location);
						}
					}

					$this->southWest->addLocation($location);

				}
			}
		}
	}