<?php
	require_once $_SERVER['DOCUMENT_ROOT'] . 'BoundingBox.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . 'Location.php';

	$box = BoundingBox(34, -90, 45, 29);
	$data = array('name' => 'victor');
	$location = Location(40, 18, $data);
	$box->addLocation($location);