<?php
	function &get_something($name) {
		$GLOBALS[$name] += 10;
		return $GLOBALS[$name];	
	}

	function increment_something(&$num, $inc = 1) {
		static $inc2 = 1;
		$num += ++$inc2 + $inc;
	}

	$num = 10;

	$value =& get_something('num');
	print $value . "\n";
	print $num . "\n";
	$value = 10;
	print $num . "\n";

	$value = get_something('num');
	print $value . "\n";
	print $num . "\n";
	$value = 10;
	print $num . "\n";

	$num = 99;
	increment_something($num);
	print $num . "\n";
	increment_something($num, 6);
	print $num . "\n";