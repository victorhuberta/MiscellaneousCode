public class PrintNames {
	public static void main(String[] args) {
		Thread main_t = Thread.currentThread();
		OtherPrintNames opn = new OtherPrintNames();
		Thread t = new Thread(opn);
		t.start();
		for (int i = 0; i < 20; i++) 
			System.out.println(main_t.getName());
	}
}

class OtherPrintNames implements Runnable {
	@Override
	public void run() {
		Thread t = Thread.currentThread();
		for (int i = 0; i < 20; i++)
			System.out.println(t.getName());
	}
}
