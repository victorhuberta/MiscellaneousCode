public class DeadLock {
	public static void main(String[] args) {
		Enemy jackie = new Enemy("jackie");
		Enemy jason = new Enemy("jason");
		Thread t1 = new Thread( 
			new Runnable() {
				public void run() {
					jackie.slap(jason);
				}
			}
		);
		Thread t2 = new Thread(
			new Runnable() {
				public void run() {
					jason.slap(jackie);
				}
			}
		);
		t1.start();
		t2.start();
	}
}

class Enemy {
	public String name;

	public Enemy(String name) {
		this.name = name;
	}

	public synchronized void slap(Enemy enemy) {
		System.out.println(this.name + " slapped " + enemy.name);
		enemy.slapBack(this);
	}

	public synchronized void slapBack(Enemy enemy) {
		System.out.println(this.name + " slapped " + enemy.name + " back.");
	}
}
