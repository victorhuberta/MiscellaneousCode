public class OutputGradeLetter
{
	public static void main(String[] args) throws java.io.IOException
	{
		System.out.print("My dear, what is your score ? : ");
		int sc = 0;
		sc = System.in.read();
		char c;
		if (sc < 41)
			c = 'E';
		else
		if (sc < 61)
			c = 'D';
		else
		if (sc < 76)
			c = 'C';
		else
		if (sc < 86)
			c = 'B';
		else
		if (sc < 101)
			c = 'A';
		else
			c = 'X';	
		System.out.print("Your grade is : " + c);
		System.out.println();
	}
}
