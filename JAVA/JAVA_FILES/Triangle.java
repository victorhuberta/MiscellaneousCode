public class Triangle
{
	public static void main(String[] args)
	{
		int i = 0;
		while (i < 21)
		{
			for (int y = 0; y < (20-i)/2; y++)
				System.out.print(" ");
			for (int x = 0; x <= i; x++)
				System.out.print('*');
			System.out.println();
			i += 2;
		}	
	}
}
