import java.io.*;

public class CountLetterTest {
	public static void main(String[] args) {
		File file = new File("Q1C.txt");
		CountLetter cl = new CountLetter('e', file);
		cl.lookForCharacter();
		System.out.println("'e' count : " + cl.getCount());
	}
}
