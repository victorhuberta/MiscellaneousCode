import java.util.*;
import java.io.*;

public class CountLetter {
	public char c;
	public File file;
	public int count;

	public CountLetter(char c, File file) {
		this.c = c;
		this.file = file;	
	}

	public void lookForCharacter() {
		Scanner inputFile = new Scanner(System.in);
		try {
			inputFile = new Scanner(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while(inputFile.hasNext()) {
			String word = inputFile.next();
			for (int i = 0; i < word.length(); i++) {
				char c = word.charAt(i);
				if (c == this.c) {
					count++;
				}
			}
		}
	}

	public int getCount() {
		return count;
	}
}
