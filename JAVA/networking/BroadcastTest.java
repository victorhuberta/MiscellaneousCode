import java.net.*;
import java.util.*;

public class BroadcastTest {
	public static void main(String[] args) {
		DatagramSocket socket;
		try {
			socket = new DatagramSocket();
			socket.setBroadcast(true);
			byte[] data = new byte[2048];
			DatagramPacket packet = new DatagramPacket(data, data.length);
			InetAddress addr = getBroadcastAddress();
			System.out.println(addr);
			
		} catch (SocketException e) {
			System.out.println("ERROR: "+ e.getMessage());
		}
		
	}

	public static InetAddress getBroadcastAddress() throws SocketException {
		Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
		while (interfaces.hasMoreElements()) {
			NetworkInterface networkInterface = interfaces.nextElement();
			if (networkInterface.isLoopback())
				continue;    // Don't want to broadcast to the loopback interface
			for (InterfaceAddress interfaceAddress: networkInterface.getInterfaceAddresses()) {
				InetAddress broadcast = interfaceAddress.getBroadcast();
				if (broadcast == null) {
					System.out.println("Broadcast address is null");
					continue;
				}
				// Use the address
			}
		}
		return null;
	}
}