import java.util.*;

public class SortedSetTest {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String [] colors ={"red","white","blue","green","gray",
		"orange","tan","white","cyan","peach","gray","orange"};
		SortedSet<String> tree = new TreeSet<String>(Arrays.asList(colors));
		System.out.println("Sorted set");
		printSet(tree);
		//subset of the tree set which every element is less than "orange"
		System.out.print("headSet(\"orange\") : " );
		printSet(tree.headSet("orange"));
		//subset which each element is greater than or equal to "orange"
		System.out.print("tailSet(\"orange\") : " );
		printSet(tree.tailSet("orange"));
		//get the smallest
		System.out.printf("first : %s\n",tree.first());
		//get the largest
		System.out.printf("last: %s\n",tree.last());
	}

	private static void printSet(SortedSet<String> set){
		for(String s : set) {
			System.out.printf("%s",s);
			System.out.println();
		}
	}
}
