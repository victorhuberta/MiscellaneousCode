public class Test implements Testable {
	static java.util.Date date = new java.util.Date(1234567);

	@Override
	public void printSth() {
		System.out.println("Something");
	}

	public static void xMethod(char[] a) {
		System.out.println("Call to xMethod(char[] a) is successful!");
	}
	
//	public static int xMethod(char[] a, int value) {
//		System.out.println("Call to xMethod(char[] a, int value) is successful!");
//		return value;
//	}

	public static double xMethod(char[] a, int value) {
		System.out.println("Call to xMethod(char[] a, int value) is successful!");
		return value;
	}
	
	public static void main(String[] args) {
		TestGrandSon testGrandSon = new TestGrandSon();
		testGrandSon.printSth();
		Test test = new TestGrandSon();
		test.printSth();
		TestChild testChild = new TestGrandSon();
		testChild.printSth();
		((TestGrandSon) test).printSth();
		double radius;
		final double PI = 3.15169;
		double area = 10 * 10 * PI;
		System.out.println("Area is " + area);
		System.out.println(value);
		System.out.println(Tryable.value);
		System.out.println(Testable.value);
		
		System.out.println(date.toString());
		
		xMethod(new char[2]);
		char[] a = {'3','4'};
		xMethod(a);
		xMethod(a , 2);
		
	}
}

abstract class TestChild extends Test {
	@Override
	public abstract void printSth();
}

class TestGrandSon extends TestChild {
	@Override
	public void printSth() {
		System.out.println("Something else");
	}
}

interface Tryable {
	final static int value = 0;
	final static String name = "foo";
	
	abstract void printSth();
}

interface Testable extends Tryable {
	
}
