public class TestInheritance {
	public static void main(String[] args) {
		Child child = new Child(10);
		if (child instanceof Edible)
			System.out.println("Child is an instance of Edible.");
	}
}

class Parent {
	Parent() {
		System.out.println("I am Parent");
	}
}

class Child extends Parent implements Edible {
	Child(int x) {
		System.out.println("I am Child");
	}
}

interface Edible {
	
}
