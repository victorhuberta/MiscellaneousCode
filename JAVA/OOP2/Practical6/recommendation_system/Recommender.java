import java.util.*;

public class Recommender {
	private static HashMap<String, LinkedList<String>> likes = new HashMap<>();
	public static void addLikes(String person, String item) {
		LinkedList<String> itemList = likes.get(person);
		if (itemList == null) {
			itemList = new LinkedList<String>();
			itemList.add(item.toLowerCase());
			likes.put(person, itemList);
			return;
		}
		itemList.add(item.toLowerCase());	
	}
	
	public static boolean likesBoth(String person, String item1, String item2) {
		LinkedList<String> itemList = likes.get(person);
		if (itemList == null)	
			throw new NoSuchElementException("No such person was found.");
		Boolean both = false;
		for (String first: itemList) {
			if (first.equals(item1.toLowerCase())) {
				for (String second: itemList) {
					if (second.equals(item2.toLowerCase())) {
						both = true;
						break;
					}
				}
				if (both == true)
					break;
			}
		}
		return both;
	}
}
