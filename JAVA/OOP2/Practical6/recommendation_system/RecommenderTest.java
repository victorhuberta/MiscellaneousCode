import java.util.*;

public class RecommenderTest {
	public static void main(String[] args) {
		String[] victor_items = {"Man In The Middle", "Hackers The Movie"};
		for (String item: victor_items) 
			Recommender.addLikes("Victor", item);
		String[] daniel_items = {"Korean Movie 2015", "Kamen Raider Ryukki"};
		for (String item: daniel_items)
			Recommender.addLikes("Daniel", item);
		try {
			System.out.println("Victor likes 'Man In The Middle' and 'Kamen Raider' ?");
			System.out.println(Recommender.likesBoth("Victor", "Man In The Middle", "Kamen Raider"));
			System.out.println("Daniel likes 'Korean Movie 2015' and 'Kamen Raider Ryukki' ?");
			System.out.println(Recommender.likesBoth("Daniel", "korean movie 2015", "kamEn RAIDer RyUkkI"));
			System.out.println("Janice likes 'Japanese Banana 1988' and 'Naruto The Movie' ?");
			System.out.println(Recommender.likesBoth("Janice", "Japanese Banana 1988", "Naruto the MOVie"));
		}
		catch (NoSuchElementException nsee) {
			System.err.println(nsee.getMessage());
		}
	}
}
