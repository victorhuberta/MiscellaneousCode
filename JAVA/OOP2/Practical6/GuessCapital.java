import java.util.*;

public class GuessCapital {
	public static void main(String[] args) {
		HashMap<String, String> stateDict = new HashMap<String, String>();
		stateDict.put("Alabama", "Montgomery");
		stateDict.put("Alaska", "Juneau");
		stateDict.put("Arizona", "Phoenix");
		Iterator<Map.Entry<String, String>> it = stateDict.entrySet().iterator();
		Scanner input = new Scanner(System.in);
		String answer = "";
		int correctCount = 0;
		while (it.hasNext()) {
			Map.Entry<String, String> entry = it.next();
			System.out.printf("What is the capital of %s ? ", entry.getKey());
			answer = input.nextLine();
			if (answer.equals(entry.getValue())) {
				System.out.println("Your answer is correct.");
				correctCount++;
			}
			else
				System.out.println("The correct answer should be " + entry.getValue());
		}
		System.out.println("The correct count is " + correctCount);		
	}
}
