public class TaskThreadDemo {
  public static void main(String[] args) {
    // Create tasks
    Runnable printA = new PrintChar('a', 100);
    Runnable printB = new PrintChar('b', 100);
    Runnable print100 = new PrintNum(100);

    // Create threads
    Thread thread1 = new Thread(printA);
    Thread thread2 = new Thread(printB);
    Thread thread3 = new Thread(print100);

    // Start threads

    try {
        thread1.start();
        thread1.join();
        thread2.start();
	thread2.join();
	thread3.start();
	thread3.join();
    } catch (InterruptedException e) {
	e.printStackTrace();
    }
  }
}
